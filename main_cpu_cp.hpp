#ifndef _MAIN_H_
#define _MAIN_H_

#include <chrono>
#include <string>
#include "stdlib.h"
#include "stdio.h"
#include <iostream>

#define IMAGE_SIZE 784
#define OUT1_SIZE 4096
#define OUT4_SIZE 10

//#define USE_CONCAT_W

struct shape {
  size_t num{0};    // number of images in mini-batch
  size_t length{0};  // width of the image
  shape(size_t num, size_t length) : num(num), length(length){
  }

  size_t batched_length() const {
    return num * length;
  }
};

template<typename T>
static void readdata(std::string fileName, shape dims, T *output){
  FILE *fin;
  fin=fopen(fileName.c_str(), "rb");
  for(int i = 0; i < dims.num; i++){
    fread(&output[i * dims.length], sizeof(T), dims.length, fin);
  }
  fclose(fin);
}

template <typename T, typename ShapeT>
static T* allocate(const ShapeT &shp) {
    T *res = new T[shp.batched_length()];
      return res;
}

template <typename T>
static T* allocate1D(const int Size) {
  T *res = new T[Size];
  return res;
}

//for debug
template<typename T>
void print_acc(T* in, int size){
  T acc1 = 0;
  for(int i = 0; i < size; i++){
    acc1 += in[i];
  }
  printf ("acc is %f\n", (float)acc1);
}

static std::chrono::time_point<std::chrono::high_resolution_clock> now() {
  return std::chrono::high_resolution_clock::now();
}

unsigned int concatenate(float* array);


#endif
