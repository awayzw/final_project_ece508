//1. The first layer is baseline, it could be replaced with cublas GEMM and/or our register tiled MM kernel

void mm_gpu(int batchSize, 
  float *x, int xlength,
  float *w1, int w1_length, //784 * 4096
  float *b1, int b1_length, //4096
  int out1length, //4096
  float *beta1, float* gamma1, float *inv_std1, float *mean1,
  ){
	//first layer, dense layer
	float *device_x;
	size_t device_x_length_byte = batchSize * xlength * sizeof(float);
	check_success(cudaMalloc((void **)&device_x, device_x_length_byte));

	float *device_w1;
	size_t device_w1_length_byte = w1_length * sizeof(float);
	check_success(cudaMalloc((void **)&device_w1, device_w1_length_byte));

	float *device_out1;
	size_t device_out1_length_byte = batchSize * out1_length * sizeof(float);

	//MLP, the none-linearity is identify, i.e., g(x) = x;
	check_success(cudaMemcpy(device_x, x, device_x_length_byte, cudaMemcpyHostToDevice);
	gemm();
	check_success(cudaDeviceSynchronize());

	//FIXME, these two lines should remove, should done in gpu.
	float *out1 = allocate<float>(batchSize * out1_length);
	size_t out1_size = batchSize * out1_length;
	check_success(cudaMemcpy(out1, device_ou1, device_out1_length_byte, cudaMemcpyDeviceToHost);
	//FIXME: batch_normalization, this probably could be implemented as a cuda kernel, hence we do not need to copy the output of the first layer from device, but do normalization within GPU.
	//merged into gemm
	
	batch_normalization(beta1, gamma1, mean1, inv_std1, out1, out1_size);
	//FIXME: put this into gpu too, merge into gemm
	SignTheano(out1, out1_size);
	
	//second layer
	//BNN: 4096
	
	check_success(cudaMalloc(device_out1, out1, device_out1_length_byte, cudaMemcpyDeviceToHost);

	float *device_w2;  
	size_t device_w2_length_byte = w2_length * sizeof(float);
	check_success(cudaMalloc((void**)&device_w2, device_w2_length_byte));

	unsigned int *device_out1_conc;
	size_t device_out1_conc_length = out1_length/32;
	check_success(cudaMalloc((void**)&device_A_conc, device_out1_conc_length * sizeof(unsigned int));
	concatenation_rows(device_out1, device_out1_conc, batchSize, out1_length);

	unsigned int *device_w2_conc;
	size_t device_w2_conc_byte = w2_length_byte / 32;
	size_t device_w2_conc_length = w2_length / 32;
	check_success(cudaMalloc((void**)&device_w2_conc, device_w2_conc_byte));

	concatenation_columns(device_w2, device_w2_conc, out1_length, w2_length/out1_length);

	float *device_out2;
	size_t device_out2_length_byte = batchSize * out2_length;
	check_success(cudaMalloc((void**)&device_out2, device_out2_length_byte));

	gemm_xnor(device_out1_conc, device_w2_conc, device_out2, batchSize, device_out1_conc_length, device_w2_conc_length);

	float *out2 = allocate<float>(batchSize * out2_length);
	check_success(out2, cudaMalloc((void**) &device_out2, device_out2_length_byte, cudaMemcpyDeviceToHost));


	//batch_normalization
	batch_normalization();
	//Nolinear
	
	//thrid layer
	//BNN: 4096
	//batch_normalization
	//nolinear
	
	//forth layer
	//BNN: 4096
	//batch_normalization
	//nolinear
	
	//output layer
	//BNN : 10
	
}

//following the definition of http://lasagne.readthedocs.io/en/latest/modules/layers/normalization.html
void batch_normalization(float beta, float gamma, float mean, float inv_std, float *x, int size){
	var = inv_std * inv_std;
	epslion = 0.0001;
	det = sqrt(var + epslion);
	for (int i = 0; i < size; i++){
		x[i]  = (x[i] - mean)/det)*gamma + beta;
	}
}

//the function binary_ops.SignTheano(x), binarize the output x  = (x>= 0) ? 1.0 : -1.0;
void SignTheano(float* x, int size){
	for(int i = 0; i < size; i++){
		x[i] = (x[i] >=0.0) ? 1.0 : 0.0;

	}
}

void concatenation_rows(float * A, unsigned int * A_conc, int A_rows, int A_cols){
  assert A_cols % 32 == 0; //make sure it can be devided by 32.
  block_size = 64; //FIXME: this should be changeable
  block = (block_size, 1, 1);
  grid = (A_rows * A_cols/(block_size * 32) +1, 1);
  concatenate_rows_kernel<<<grid, block>>>(A, A_conc, A_rows*A_cols/32);
}

void concatenation_columns(float *A, unsigned int *A_conc, int A_rows, int A_cols){
  assert A_rows%32 == 0;
  block_size = 64;
  block = (block_size, 1, 1);
  grid = (A_cols/block_size+1, 1);
  concatenate_cols_kernel<<<grid, block>>>(A, A_conc, A_rows, A_cols);
}

__global__ void concatenate_rows_kernel(float *a, unsigned int *b, int size){
  int i = blockIdx.x * blockDim.x + threadIdx.x;
  if(i<size) b[i] = concatenate(&a[i*32]);
}

__global__ void concatenate_cols_kernel(float *a, unsigned int *b, int m, int n) { 
  int j = blockIdx.x * blockDim.x + threadIdx.x;
  if(j<n){
    float * array = new float[32];
    for(int i=0; i<m; i+=32){
      for(int k=0; k<32;k++) array[k] = a[j + n*(i+k)];
      b[j+n*i/32]=concatenate(array); 
    } 
    delete[] array;
  }
}

//FIXME: size should change
void gemm_xnor(unsigned int* A, unsigned int* B, float* C, int A_rows, int A_cols, int B_cols){
  assert A_cols % 16 == 0;
  block_size = 16;
  block = (block_size, block_size, 1);
  grid = (B_cols / block_size+1, A_rows / block_size+1) ;// better too many blocks than too little
  xnor_gemm<<<grid, block>>>(A, B, C, A_rows, A_cols, B_cols);
}

__device__ unsigned int concatenate(float* array) {
  unsigned int rvalue=0;
  unsigned int sign;
  for (int i = 0; i < 32; i++)
  {
    sign = (array[i]>=0);
    rvalue = rvalue | (sign<<i);
  }
  return rvalue;
}

