//originally the import weight,  now we seperately import weight, for the first layer, use orignial, for the rest of the layers, use the concatenated ones.
//
int main(int argc, char *argv){
    //allocate the memory for input output weights
    float *x = allocate<float>(xdims);
    float *y = aloocate<float>(rdims);

    float *w1 = allocate<float>(w1dims);
    float *w2 = allocate<float>(w2dims);
    float *w3 = allocate<float>(w3dims);
    float *w4 = allocate<float>(w4dims);
    float *w5 = allocate<float>(w5dims);

    //read input and weights
    readdata(x, xdims);
    readdata(w1, w1dims);
    readdata(w2, w2dims);
    readdata(w3, w3dims);
    readdata(w4, w4dims);
    readdata(w5, w5dims);

    //generate output feature map for verification
    mm_gpu();
}
