#include "main_cpu.hpp"
#include "math.h"
#include <iostream>
#include <cassert>


template<typename T>
static void writedata(std::string fileName, shape dims, T *input){
  FILE *fout;
  fout=fopen(fileName.c_str(), "wb");
  for(int i = 0; i < dims.num; i++){
    fwrite(&input[i * dims.length], sizeof(T), dims.length, fout);
  }
  fclose(fout);
}

void gemm_cpu(float* A, float* B, float *C, int A_row, int A_col, int B_col, float *b){
  for(int i = 0; i < A_row; i++){
    for(int j =0; j < B_col; j++){
      float acc = b[j];
      for(int k =0; k < A_col; k++){
      	acc += A[i *A_col + k] * B[ k *B_col + j];
      }
      C[i * B_col + j] = acc;
    }
  }
}

//this should be merged into gemm_gpu
void reshape(float *x, int size){
  for(int i = 0; i < size ; i++){
    x[i] = 2 * x[i] -1;
  }
}


//following the definition of http://lasagne.readthedocs.io/en/latest/modules/layers/normalization.html
//the real code is https://github.com/Lasagne/Lasagne/blob/master/lasagne/layers/normalization.py#L120-L320, line 320
void batch_normalization(float* beta, float* gamma, float* mean, float* inv_std, float *x, int batch_num, int onebatchsize){
  for( int j = 0; j < batch_num; j++){
    for (int i = 0; i < onebatchsize; i++){ 
      x[j * onebatchsize + i] = (x[j * onebatchsize + i] - mean[i] ) * gamma[i] * inv_std[i] + beta[i];
    }
  }
}

//the function binary_ops.SignTheano(x), binarize the output x  = (x>= 0) ? 1.0 : -1.0;
void SignTheano(float* x, int size){
	for(int i = 0; i < size; i++){
		x[i] = (x[i] >=0.0) ? 1.0 : -1.0;

	}
}

unsigned int concatenate(float* array)
{
    unsigned int rvalue=0;
    unsigned int sign;
    
    for (int i = 0; i < 32; i++)
    {
        sign = (array[i]>=0);
        rvalue = rvalue | (sign<<i);
    }
    
    return rvalue;
}

void concatenate_rows_cpu(float *a, unsigned int *b, int size)
{
    for (int i = 0; i < size; i++)
        b[i] = concatenate(&a[i*32]);
}

void concatenate_cols_cpu(float *a, unsigned int *b, int m, int n)
{
    for (int j = 0; j < n; j++)
    {
        float * array = new float[32];
        for(int i=0; i<m; i+=32){
            for(int k=0; k<32;k++) array[k] = a[j + n*(i+k)];
            b[j+n*i/32]=concatenate(array); 
        } 
        delete[] array;
    }
}

void xnor_gemm_cpu(unsigned int* A, unsigned int* B, float* C, int A_row, int A_col, int B_col, float* b){
//void xnor_gemm_cpu(float* A, float* B, float* C, int A_row, int A_col, int B_col, float* b){
  for(int i = 0; i < A_row; i++){
    for(int j =0; j < B_col; j++){
      unsigned int acc = b[j];
      for(int k =0; k < A_col; k++){
      	unsigned int A_tmp = (unsigned int)A[i *A_col + k];
      	unsigned int B_tmp = (unsigned int)B[ k *B_col + j];
        acc += __builtin_popcount(A_tmp ^ B_tmp);
      	//acc += (A_tmp ^ B_tmp);
      }
      C[i * B_col + j] = -(2*(float)acc-(float)32*A_col);
    }
  }
}

int main(int argc, char **argv){
  //allocate the memory for input and output weights
  float *x = allocate<float>(xdims);
  float *w1 = allocate<float>(w1dims);
  float *b1 = allocate<float>(b1dims);
  float *beta1 = allocate<float>(b1dims); 
  float *gamma1 = allocate<float>(b1dims);
  float *mean1 = allocate<float>(b1dims);
  float *inv_std1 = allocate<float>(b1dims);

  float *w2 = allocate<float>(w2dims);
  float *b2 = allocate<float>(b2dims);
  float *beta2 = allocate<float>(b2dims); 
  float *gamma2 = allocate<float>(b2dims);
  float *mean2 = allocate<float>(b2dims);
  float *inv_std2 = allocate<float>(b2dims);

  float *w3 = allocate<float>(w3dims);
  float *b3 = allocate<float>(b3dims);
  float *beta3 = allocate<float>(b3dims); 
  float *gamma3 = allocate<float>(b3dims);
  float *mean3 = allocate<float>(b3dims);
  float *inv_std3 = allocate<float>(b3dims);

  float *w4 = allocate<float>(w4dims);
  float *b4 = allocate<float>(b4dims);
  float *beta4 = allocate<float>(b4dims); 
  float *gamma4 = allocate<float>(b4dims);
  float *mean4 = allocate<float>(b4dims);
  float *inv_std4 = allocate<float>(b4dims);

  readdata("params/x.bin", xdims, x);
  readdata("params/w1.bin", w1dims, w1);
  readdata("params/b1.bin", b1dims, b1);
  readdata("params/beta1.bin", b1dims, beta1);
  readdata("params/gamma1.bin", b1dims, gamma1);
  readdata("params/inv_std1.bin", b1dims, inv_std1);
  readdata("params/mean1.bin", b1dims, mean1);

  readdata("params/w2.bin", w2dims, w2);
  readdata("params/b2.bin", b2dims, b2);
  readdata("params/beta2.bin", b2dims, beta2);
  readdata("params/gamma2.bin", b2dims, gamma2);
  readdata("params/inv_std2.bin", b2dims, inv_std2);
  readdata("params/mean2.bin", b2dims, mean2);

  readdata("params/w3.bin", w3dims, w3);
  readdata("params/b3.bin", b3dims, b3);
  readdata("params/beta3.bin", b3dims, beta3);
  readdata("params/gamma3.bin", b3dims, gamma3);
  readdata("params/inv_std3.bin", b3dims, inv_std3);
  readdata("params/mean3.bin", b3dims, mean3);

  readdata("params/w4.bin", w4dims, w4);
  readdata("params/b4.bin", b4dims, b4);
  readdata("params/beta4.bin", b4dims, beta4);
  readdata("params/gamma4.bin", b4dims, gamma4);
  readdata("params/inv_std4.bin", b4dims, inv_std4);
  readdata("params/mean4.bin", b4dims, mean4);

  float *out1 = allocate<float>(out1dims); //dense layer1, output is batchsize * 4096
  float *out2 = allocate<float>(out2dims);
  float *out3 = allocate<float>(out3dims);
  float *out4 = allocate<float>(out4dims);

  //reshpe to the range of [-1, 1]
  reshape(x, xdims.batched_length());

  // The input layer
  std::chrono::time_point<std::chrono::high_resolution_clock>start, end;
  double elapsed1 = 0;

  start = now();
  gemm_cpu(x, w1, out1, BATCH_SIZE,  IMAGE_SIZE, OUT1_SIZE, b1);
  end = now();
  elapsed1 += std::chrono::duration<double, std::milli>(end - start).count(); 
  std::cout << "elapsed = " << elapsed1 << " milliseconds\n";

  // The input layer - batchNormrLayer
  batch_normalization(beta1, gamma1, mean1, inv_std1, out1, out1dims.num, out1dims.length);
  
  // The input layer - NonlinearityLayer
  SignTheano(out1, out1dims.batched_length());

  // The first hidden layer
  unsigned int *Ac = new unsigned int[BATCH_SIZE*OUT1_SIZE/8]; 
  unsigned int *Bc = new unsigned int[OUT1_SIZE*OUT1_SIZE/8]; 

  concatenate_rows_cpu(out1, Ac, BATCH_SIZE*OUT1_SIZE/32);
  concatenate_cols_cpu(w2, Bc, OUT1_SIZE, OUT1_SIZE);

  shape wc2dims = {w2dims.num/32, w2dims.length};
  writedata("params/wc2.bin", wc2dims, Bc);

  xnor_gemm_cpu(Ac, Bc, out2, BATCH_SIZE, OUT1_SIZE/32, OUT1_SIZE, b2);

  batch_normalization(beta2, gamma2, mean2, inv_std2, out2, out2dims.num, out2dims.length);

  SignTheano(out2, out2dims.batched_length());

  // The second hidden layer
  concatenate_rows_cpu(out2, Ac, BATCH_SIZE*OUT1_SIZE/32);
  concatenate_cols_cpu(w3, Bc, OUT1_SIZE, OUT1_SIZE);

  shape wc3dims = {w3dims.num/32, w3dims.length};
  writedata("params/wc3.bin", wc3dims, Bc);

  xnor_gemm_cpu(Ac, Bc, out3, BATCH_SIZE, OUT1_SIZE/32, OUT1_SIZE, b3);

  batch_normalization(beta3, gamma3, mean3, inv_std3, out3, out3dims.num, out3dims.length);

  SignTheano(out3, out3dims.batched_length());

  // The output layer
  concatenate_rows_cpu(out3, Ac, BATCH_SIZE*OUT1_SIZE/32);
  concatenate_cols_cpu(w4, Bc, OUT1_SIZE, OUT4_SIZE);

  shape wc4dims = {w4dims.num/32, w4dims.length};
  writedata("params/wc4.bin", wc4dims, Bc);

  xnor_gemm_cpu(Ac, Bc, out4, BATCH_SIZE, OUT1_SIZE/32, OUT4_SIZE, b4);

  batch_normalization(beta4, gamma4, mean4, inv_std4, out4, out4dims.num, out4dims.length);

  
  float max_idx = 0;
  for (int i = 0; i < out4dims.batched_length(); i++)
  {
    float max_value = 0;
    if (max_value < out4[i])
    {
      max_idx = i; 
      max_value = out4[i]; 
    }
  }
  std::cout << "Category: " << max_idx << std::endl;

  delete [] x;
  delete [] w1;
  delete [] b1;
  delete [] beta1;
  delete [] gamma1;
  delete [] mean1;
  delete [] inv_std1;

  delete [] w2;
  delete [] b2;
  delete [] beta2;
  delete [] gamma2;
  delete [] mean2;
  delete [] inv_std2;

  delete [] w3;
  delete [] b3;
  delete [] beta3;
  delete [] gamma3;
  delete [] mean3;
  delete [] inv_std3;

  delete [] w4;
  delete [] b4;
  delete [] beta4;
  delete [] gamma4;
  delete [] mean4;
  delete [] inv_std4;

  delete [] out1;
  delete [] out2;
  delete [] out3;
  delete [] out4;
}
