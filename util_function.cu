#include "util_function.hpp"
void transpose_reshape(float *src, float *dst, const int N, const int M) {
#pragma omp parallel for
  for(int n = 0; n<N*M; n++) {
    int i = n/N;
    int j = n%N;
    dst[n] = 2*src[M*j + i]-1;
  }
}

void transpose(float *src, float *dst, const int N, const int M) {
#pragma omp parallel for
  for(int n = 0; n<N*M; n++) {
    int i = n/N;
    int j = n%N;
    dst[n] = src[M*j + i];
  }
}

__device__ unsigned int concatenate_gpu(float* array)
{
    unsigned int rvalue=0;
    unsigned int sign;
    
    for (int i = 0; i < 32; i++)
    {
        sign = (array[i]>=0);
        rvalue = rvalue | (sign<<i);
    }
    
    return rvalue;
}

__global__ void concatenate_rows_kernel(float *a, unsigned int *b, int size)
{ 
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if(i<size) b[i] = concatenate_gpu(&a[i*32]);
}

__global__ void concatenate_cols_kernel(float *a, unsigned int *b, int m, int n)
{   

    int j = blockIdx.x * blockDim.x + threadIdx.x;
    
    if(j<n){
        float * array = new float[32];
        for(int i=0; i<m; i+=32){
            for(int k=0; k<32;k++) array[k] = a[j + n*(i+k)];
            b[j+n*i/32]=concatenate_gpu(array); 
        } 
        delete[] array;
    }
}

void concatenate_cols_gpu(float *a, unsigned int *b, int m, int n){
  int num_threads = n;
  int num_blocks = ((num_threads-1)/CUDA_MAX_NUM_THREADS)+1;
  concatenate_cols_kernel<<<num_blocks, CUDA_MAX_NUM_THREADS>>>(a, b, m, n);
}



__global__ void gemm_xnor_gpu_optimized_kernel(unsigned int *A, unsigned int *B, float *C,
                                      int numARows, int numAColumns,
                                      int numBRows, int numBColumns,
                                      int numCRows, int numCColumns,
                     float * b,float* beta, float* gamma, float* mean, float* inv_std) {

  //Shared Memory and registers
  __shared__ unsigned int ds_B[TILE_WIDTH_K][TILE_WIDTH_N];
  unsigned int  ds_A[TILE_WIDTH_K];
  float  ds_P[TILE_WIDTH_N] ={0};
  
  //tile indices
  int bx = blockIdx.x;
  int by = blockIdx.y;
  int ty = threadIdx.y;
  int dx = TILE_WIDTH_N;
  int dy = TILE_WIDTH_M;
  
  int ds_B_row = ty/TILE_WIDTH_N;
  int ds_B_column = ty%TILE_WIDTH_N;
  
  int Column  = bx*dx+ds_B_column;
  int Row     = by*dy+ty;

  for (int p = 0; p < (numBRows-1)/TILE_WIDTH_K+1; ++p){
    //Each thread loads one B elements to shared memory
    if(p*TILE_WIDTH_K+ds_B_row < numBRows && Column < numBColumns){
      ds_B[ds_B_row][ds_B_column] = B[Column + (p*TILE_WIDTH_K+ds_B_row)*numBColumns];
    } else {
      ds_B[ds_B_row][ds_B_column] = 0.0;
    }
    __syncthreads();
    //Each thread loads TILE_WIDTH_K elements from A
    for(int i = 0; i < TILE_WIDTH_K; i++){
      if(p*TILE_WIDTH_K+i < numARows && Row < numAColumns){
        ds_A[i] = A[(p*TILE_WIDTH_K+i)*numAColumns + Row];
      }  
    }
    //compute
    for(int n = 0; n < TILE_WIDTH_N; ++n){
      for(int i = 0; i < TILE_WIDTH_K; ++i)
        ds_P[n] += __popc(ds_A[i] ^ ds_B[i][n]);
    }
    __syncthreads();
  }
  //write out
  for(int n = 0; n < TILE_WIDTH_N; ++n){
    if(Row < numCRows && bx*dx+n < numCColumns){
      int offset = Row;
      //C[Row*numCColumns + bx*dx+n] = -(2*(float)ds_P[n]-(float)32*numARows) + b[Row];
      C[Row * numCColumns + bx*dx+n] = ((-(2*(float)ds_P[n] -(float)32*numARows)+ b[offset] - mean[offset]) * gamma[offset] * inv_std[offset] + beta[offset]) >= 0.0? 1.0:-1.0;
    }
  }
}


void gemm_gpu_xnor(unsigned int *A, unsigned int *B, float *C,
                                      int numARows, int numAColumns,
                                      int numBRows, int numBColumns,
                                      int numCRows, int numCColumns,
                     float * b, float* beta, float* gamma, float* mean, float* inv_std, bool l4) {
  //launch GEMM with Optimization
  dim3 DimGrid((numCColumns-1)/TILE_WIDTH_N+1, (numCRows-1)/TILE_WIDTH_M+1);
  dim3 DimBlock(1, TILE_WIDTH_M); 
  if(!l4){
	 gemm_xnor_gpu_optimized_kernel<<<DimGrid, DimBlock>>>(A, B, C, numARows,numAColumns, numBRows, 
      numBColumns, numCRows, numCColumns, b, beta, gamma, mean, inv_std);
}
  else{
	 gemm_xnor_gpu_optimized_kernel_l4<<<DimGrid, DimBlock>>>(A, B, C, numARows,numAColumns, numBRows, 
      numBColumns, numCRows, numCColumns, b, beta, gamma, mean, inv_std);
}
}


void gemm_gpu(float *A, float *B, float *C,
                                      int numARows, int numAColumns,
                                      int numBRows, int numBColumns,
                                      int numCRows, int numCColumns,
                     float * b, float* beta, float* gamma, float* mean, float* inv_std)
 {

  dim3 DimGrid((numCColumns-1)/TILE_WIDTH_N+1, (numCRows-1)/TILE_WIDTH_M+1);
  dim3 DimBlock(1, TILE_WIDTH_M); 
  matrixMultiply_kernel<<<DimGrid, DimBlock>>>(A, B, C, numARows,numAColumns, numBRows, 
      numBColumns, numCRows, numCColumns, b, beta, gamma, mean, inv_std);

}

__global__ void matrixMultiply_kernel(float *A, float *B, float *C,
                                      int numARows, int numAColumns,
                                      int numBRows, int numBColumns,
                                      int numCRows, int numCColumns,
                     float * b,float* beta, float* gamma, float* mean, float* inv_std) {
  //@@ Insert code to implement matrix multiplication here
  //@@ You have to perform register tiling for this 
  
  //Shared Memory and registers
  __shared__ float ds_B[TILE_WIDTH_K][TILE_WIDTH_N];
  float  ds_A[TILE_WIDTH_K];
  float  ds_P[TILE_WIDTH_N] ={0};
  
  //tile indices
  int bx = blockIdx.x;
  int by = blockIdx.y;
  int ty = threadIdx.y;
  int dx = TILE_WIDTH_N;
  int dy = TILE_WIDTH_M;
  
  int ds_B_row = ty/TILE_WIDTH_N;
  int ds_B_column = ty%TILE_WIDTH_N;
  
  int Column  = bx*dx+ds_B_column;
  int Row     = by*dy+ty;

  for (int p = 0; p < (numBRows-1)/TILE_WIDTH_K+1; ++p){
    //Each thread loads one B elements to shared memory
    if(p*TILE_WIDTH_K+ds_B_row < numBRows && Column < numBColumns){
      ds_B[ds_B_row][ds_B_column] = B[Column + (p*TILE_WIDTH_K+ds_B_row)*numBColumns];
    } else {
      ds_B[ds_B_row][ds_B_column] = 0.0;
    }
    __syncthreads();
    //Each thread loads TILE_WIDTH_K elements from A
    for(int i = 0; i < TILE_WIDTH_K; i++){
      if(p*TILE_WIDTH_K+i < numARows && Row < numAColumns){
        ds_A[i] = A[(p*TILE_WIDTH_K+i)*numAColumns + Row];
      }  
    }
    //compute
    for(int n = 0; n < TILE_WIDTH_N; ++n){
      for(int i = 0; i < TILE_WIDTH_K; ++i)
        ds_P[n] += ds_A[i] * ds_B[i][n];
    }
    __syncthreads();
  }
  //write out
  for(int n = 0; n < TILE_WIDTH_N; ++n){
    if(Row < numCRows && bx*dx+n < numCColumns){
      //C[Row*numCColumns + bx*dx+n] = ds_P[n] + b[Row];
      int offset = Row;
      C[Row * numCColumns + bx*dx+n] = ((ds_P[n] + b[offset] - mean[offset]) * gamma[offset] * inv_std[offset] + beta[offset]) >= 0.0? 1.0:-1.0;
    }
  }
}

__global__ void gemm_xnor_gpu_optimized_kernel_l4(unsigned int *A, unsigned int *B, float *C,
                                      int numARows, int numAColumns,
                                      int numBRows, int numBColumns,
                                      int numCRows, int numCColumns,
                     float * b,float* beta, float* gamma, float* mean, float* inv_std) {

  //Shared Memory and registers
  __shared__ unsigned int ds_B[TILE_WIDTH_K][TILE_WIDTH_N];
  unsigned int  ds_A[TILE_WIDTH_K];
  float  ds_P[TILE_WIDTH_N] ={0};
  
  //tile indices
  int bx = blockIdx.x;
  int by = blockIdx.y;
  int ty = threadIdx.y;
  int dx = TILE_WIDTH_N;
  int dy = TILE_WIDTH_M;
  
  int ds_B_row = ty/TILE_WIDTH_N;
  int ds_B_column = ty%TILE_WIDTH_N;
  
  int Column  = bx*dx+ds_B_column;
  int Row     = by*dy+ty;

  for (int p = 0; p < (numBRows-1)/TILE_WIDTH_K+1; ++p){
    //Each thread loads one B elements to shared memory
    if(p*TILE_WIDTH_K+ds_B_row < numBRows && Column < numBColumns){
      ds_B[ds_B_row][ds_B_column] = B[Column + (p*TILE_WIDTH_K+ds_B_row)*numBColumns];
    } else {
      ds_B[ds_B_row][ds_B_column] = 0.0;
    }
    __syncthreads();
    //Each thread loads TILE_WIDTH_K elements from A
    for(int i = 0; i < TILE_WIDTH_K; i++){
      if(p*TILE_WIDTH_K+i < numARows && Row < numAColumns){
        ds_A[i] = A[(p*TILE_WIDTH_K+i)*numAColumns + Row];
      }  
    }
    //compute
    for(int n = 0; n < TILE_WIDTH_N; ++n){
      for(int i = 0; i < TILE_WIDTH_K; ++i)
        ds_P[n] += __popc(ds_A[i] ^ ds_B[i][n]);
    }
    __syncthreads();
  }
  //write out
  for(int n = 0; n < TILE_WIDTH_N; ++n){
    if(Row < numCRows && bx*dx+n < numCColumns){
      int offset = Row;
      //C[Row*numCColumns + bx*dx+n] = -(2*(float)ds_P[n]-(float)32*numARows) + b[Row];
      C[Row * numCColumns + bx*dx+n] = ((-(2*(float)ds_P[n] -(float)32*numARows)+ b[offset] - mean[offset]) * gamma[offset] * inv_std[offset] + beta[offset]) ;
    }
  }
}
