import numpy as np
from optparse import OptionParser

#Parse the options
options = OptionParser();

options.add_option("--cout", action="store", type="int", dest="cout");
options.add_option("--cin", action="store", type="int", dest="cin");
options.add_option("--filter_size", action="store", type="int", dest="filter_size");
options.add_option("--w_num", action="store", type="int", dest="w_num");

(opts, args) = options.parse_args();
cout = opts.cout;
cin = opts.cin;
fs = opts.filter_size;
w_num = opts.w_num;

inFileName = "./cifar10_W"  + str(w_num)
flipFileName = "./wT"+str(w_num)+".bin"
tranFileName = "./w"+str(w_num)+"TT.bin"
w =  np.fromfile(inFileName, dtype=np.float32)
w = w.reshape(cout, cin, fs, fs)
wT = w[:, :, ::-1, ::-1]
wT.tofile(flipFileName)
wTT = wT.reshape(cout,-1)
wTT = np.transpose(wTT)
wTT.tofile(tranFileName)
