
from __future__ import print_function

import sys
import os
import time

import numpy as np
np.random.seed(1234) # for reproducibility?

# specifying the gpu to use
import theano.sandbox.cuda
theano.sandbox.cuda.use('gpu1') 
import theano
import theano.tensor as T

import lasagne

import cPickle as pickle
import gzip

import binary_net
import binary_ops

from pylearn2.datasets.zca_dataset import ZCA_Dataset   
from pylearn2.datasets.cifar10 import CIFAR10 
from pylearn2.utils import serial

from collections import OrderedDict

if __name__ == "__main__":
    
    # BN parameters
    batch_size = 1
    print("batch_size = "+str(batch_size))
    # alpha is the exponential moving average factor
    alpha = .1
    print("alpha = "+str(alpha))
    epsilon = 1e-4
    print("epsilon = "+str(epsilon))
    
    # BinaryOut
    activation = binary_net.binary_tanh_unit
    #activation = binary_net.identity
    #activation= nonlinearity=lasagne.nonlinearities.identity;
    print("activation = binary_net.binary_tanh_unit")
    # activation = binary_net.binary_sigmoid_unit
    # print("activation = binary_net.binary_sigmoid_unit")
    
    # BinaryConnect    
    binary = True
    print("binary = "+str(binary))
    stochastic = False
    print("stochastic = "+str(stochastic))
    # (-H,+H) are the two binary values
    # H = "Glorot"
    H = 1.
    print("H = "+str(H))
    # W_LR_scale = 1.    
    W_LR_scale = "Glorot" # "Glorot" means we are using the coefficients from Glorot's paper
    print("W_LR_scale = "+str(W_LR_scale))
    
    # Training parameters
    #num_epochs = 500
    #print("num_epochs = "+str(num_epochs))
    
    # Decaying LR 
    #LR_start = 0.001
    #print("LR_start = "+str(LR_start))
    #LR_fin = 0.0000003
    #print("LR_fin = "+str(LR_fin))
    #LR_decay = (LR_fin/LR_start)**(1./num_epochs)
    #print("LR_decay = "+str(LR_decay))
    # BTW, LR decay might good for the BN moving average...
    
    #train_set_size = 45000
    #print("train_set_size = "+str(train_set_size))
    #shuffle_parts = 1
    #print("shuffle_parts = "+str(shuffle_parts))
    
    print('Loading CIFAR-10 dataset...')
    
    #train_set = CIFAR10(which_set="train",start=0,stop = train_set_size)
    #valid_set = CIFAR10(which_set="train",start=train_set_size,stop = 50000)
    test_set = CIFAR10(which_set="test")
    #test_set.X = test_set.X[0];
    #test_set.y = test_set.y[0];
    #test_set.X = test_set.X[0:5000];
    #test_set.y = test_set.y[0:5000];
    test_set.y = test_set.y[0];
    test_set.X = test_set.X[0];
        
    # bc01 format
    # Inputs in the range [-1,+1]
    # print("Inputs in the range [-1,+1]")
    #train_set.X = np.reshape(np.subtract(np.multiply(2./255.,train_set.X),1.),(-1,3,32,32))
    #valid_set.X = np.reshape(np.subtract(np.multiply(2./255.,valid_set.X),1.),(-1,3,32,32))
    test_set.X = np.reshape(np.subtract(np.multiply(2./255.,test_set.X),1.),(-1,3,32,32))
    #test_set.X.tofile("cifar10_x.bin");
    print(test_set.X.sum());
    print(test_set.X.shape)
    print(test_set.y.shape)

    
    # flatten targets
    #train_set.y = np.hstack(train_set.y)
    #valid_set.y = np.hstack(valid_set.y)
    #test_set.y = np.hstack(test_set.y)
    
    # Onehot the targets
    #train_set.y = np.float32(np.eye(10)[train_set.y])    
    #valid_set.y = np.float32(np.eye(10)[valid_set.y])
    #test_set.y = np.float32(np.eye(10)[test_set.y])
    
    # for hinge loss
    #train_set.y = 2* train_set.y - 1.
    #valid_set.y = 2* valid_set.y - 1.
    #test_set.y = 2* test_set.y - 1.
    test_set.y = test_set.y.reshape(-1)

    print('Building the CNN...') 
    
    # Prepare Theano variables for inputs and targets
    input = T.tensor4('inputs')
    #target = T.matrix('targets')
    target = T.vector('targets');
    LR = T.scalar('LR', dtype=theano.config.floatX)

    cnn = lasagne.layers.InputLayer(
            shape=(None, 3, 32, 32),
            input_var=input)
    
    # 128C3-128C3-P2             
    cnn = lasagne.layers.Conv2DLayer(
            cnn, 
            num_filters=128, 
            filter_size=(3, 3),
            pad=1,
            flip_filters = True,
            nonlinearity=lasagne.nonlinearities.identity)
	#)
    
    cnn = lasagne.layers.BatchNormLayer(
            cnn,
            epsilon=epsilon, 
            alpha=alpha)
                
    cnn = lasagne.layers.NonlinearityLayer(
            cnn,
            nonlinearity=activation) 
            
    #cnn = binary_net.Conv2DLayer(
    cnn = lasagne.layers.Conv2DLayer(
            cnn, 
            #binary=binary,
            #stochastic=stochastic,
            #H=H,
            #W_LR_scale=W_LR_scale,
            num_filters=128, 
            filter_size=(3, 3),
            pad=1,
            nonlinearity=lasagne.nonlinearities.identity)
    
    cnn = lasagne.layers.MaxPool2DLayer(cnn, pool_size=(2, 2))
    
    cnn = lasagne.layers.BatchNormLayer(
            cnn,
            epsilon=epsilon, 
            alpha=alpha)
                
    cnn = lasagne.layers.NonlinearityLayer(
            cnn,
            nonlinearity=activation) 
            
    # 256C3-256C3-P2             
    cnn = binary_net.Conv2DLayer(
            cnn, 
            binary=binary,
            stochastic=stochastic,
            H=H,
            W_LR_scale=W_LR_scale,
            num_filters=256, 
            filter_size=(3, 3),
            pad=1,
            nonlinearity=lasagne.nonlinearities.identity)
    
    cnn = lasagne.layers.BatchNormLayer(
            cnn,
            epsilon=epsilon, 
            alpha=alpha)
                
    cnn = lasagne.layers.NonlinearityLayer(
            cnn,
            nonlinearity=activation) 
            
    cnn = binary_net.Conv2DLayer(
            cnn, 
            binary=binary,
            stochastic=stochastic,
            H=H,
            W_LR_scale=W_LR_scale,
            num_filters=256, 
            filter_size=(3, 3),
            pad=1,
            nonlinearity=lasagne.nonlinearities.identity)
    
    cnn = lasagne.layers.MaxPool2DLayer(cnn, pool_size=(2, 2))
    
    cnn = lasagne.layers.BatchNormLayer(
            cnn,
            epsilon=epsilon, 
            alpha=alpha)
                
    cnn = lasagne.layers.NonlinearityLayer(
            cnn,
            nonlinearity=activation) 
            
    # 512C3-512C3-P2              
    cnn = binary_net.Conv2DLayer(
            cnn, 
            binary=binary,
            stochastic=stochastic,
            H=H,
            W_LR_scale=W_LR_scale,
            num_filters=512, 
            filter_size=(3, 3),
            pad=1,
            nonlinearity=lasagne.nonlinearities.identity)
    
    cnn = lasagne.layers.BatchNormLayer(
            cnn,
            epsilon=epsilon, 
            alpha=alpha)
                
    cnn = lasagne.layers.NonlinearityLayer(
            cnn,
            nonlinearity=activation) 
                  
    cnn = binary_net.Conv2DLayer(
            cnn, 
            binary=binary,
            stochastic=stochastic,
            H=H,
            W_LR_scale=W_LR_scale,
            num_filters=512, 
            filter_size=(3, 3),
            pad=1,
            nonlinearity=lasagne.nonlinearities.identity)
    
    cnn = lasagne.layers.MaxPool2DLayer(cnn, pool_size=(2, 2))
    
    cnn = lasagne.layers.BatchNormLayer(
            cnn,
            epsilon=epsilon, 
            alpha=alpha)
                
    cnn = lasagne.layers.NonlinearityLayer(
            cnn,
            nonlinearity=activation) 
    
    # print(cnn.output_shape)
    
    # 1024FP-1024FP-10FP            
    cnn = binary_net.DenseLayer(
                cnn, 
                binary=binary,
                stochastic=stochastic,
                H=H,
                W_LR_scale=W_LR_scale,
                nonlinearity=lasagne.nonlinearities.identity,
                num_units=1024)      
                  
    cnn = lasagne.layers.BatchNormLayer(
            cnn,
            epsilon=epsilon, 
            alpha=alpha)
                
    cnn = lasagne.layers.NonlinearityLayer(
            cnn,
            nonlinearity=activation) 
            
    cnn = binary_net.DenseLayer(
                cnn, 
                binary=binary,
                stochastic=stochastic,
                H=H,
                W_LR_scale=W_LR_scale,
                nonlinearity=lasagne.nonlinearities.identity,
                num_units=1024)      
                  
    cnn = lasagne.layers.BatchNormLayer(
            cnn,
            epsilon=epsilon, 
            alpha=alpha)
                
    cnn = lasagne.layers.NonlinearityLayer(
            cnn,
            nonlinearity=activation) 
    
    cnn = binary_net.DenseLayer(
                cnn, 
                binary=binary,
                stochastic=stochastic,
                H=H,
                W_LR_scale=W_LR_scale,
                nonlinearity=lasagne.nonlinearities.identity,
                num_units=10)      
                  
    cnn = lasagne.layers.BatchNormLayer(
            cnn,
            epsilon=epsilon, 
            alpha=alpha)

    test_output = lasagne.layers.get_output(cnn, deterministic=True)
    test_err = T.mean(T.neq(T.argmax(test_output, axis=1), target),dtype=theano.config.floatX)

    #compile a function computing the validation loss and accuracy
    val_fn = theano.function([input, target], test_err)
    print("Loading the trained parameters and binarizing the weights ... ")

    with np.load("cifar10_parameters_epoch_500.npz") as f:
	param_values = [f['arr_%d' % i] for i in range(len(f.files))]
    lasagne.layers.set_all_param_values(cnn, param_values)
    
    #print lasagne.layers.get_all_params(cnn);

    #Binarize the weight
    params = lasagne.layers.get_all_params(cnn);
    print(params)
    betai = 0;
    gammai = 0;
    meani = 0;
    inv_stdi = 0;
    bi = 0;
    wi = 0;
    for param in params:
        # print param.name
        #if param.name == "beta":
        #    betai = betai+1;
	#    print (param.name+str(betai));
	#    fileName = "cifar10_"+ param.name + str(betai);
        #    param.get_value().tofile(fileName);
	#    print(param.get_value().sum());

        #if param.name == "gamma":
        #    gammai = gammai+1;
	#    print (param.name+str(gammai));
	#    fileName = "cifar10_"+ param.name + str(gammai);
        #    param.get_value().tofile(fileName);
	#    print(param.get_value().sum());

        #if param.name == "mean":
        #    meani = meani+1;
	#    print (param.name+str(meani));
	#    fileName = "cifar10_"+ param.name + str(meani);
        #    param.get_value().tofile(fileName);
	#    print(param.get_value().sum());
        #
        #if param.name == "inv_std":
        #    inv_stdi = inv_stdi+1;
	#    print (param.name+str(inv_stdi));
	#    fileName = "cifar10_"+ param.name + str(inv_stdi);
        #    param.get_value().tofile(fileName);
	#    print(param.get_value().sum());

        #if param.name == "b":
        #    bi = bi+1;
	#    print (param.name+str(bi));
	#    fileName = "cifar10_"+ param.name + str(bi);
        #    param.get_value().tofile(fileName);
	#    print(param.get_value().sum());

	#print(param.get_value().shape);
        if param.name == "W":
            param.set_value(binary_ops.SignNumpy(param.get_value()))
            wi = wi+1;
	    print ("W"+str(wi));
	    fileName = "cifar10_"+ param.name + str(wi);
            param.get_value().tofile(fileName);
	    print(param.get_value().sum());
    
    print('Running...')
    
    start_time = time.time()
    
    test_error = val_fn(test_set.X,test_set.y)*100.
    print("test_error = " + str(test_error) + "%")
    
    run_time = time.time() - start_time
    print("run_time = "+str(run_time)+"s")

    lays = lasagne.layers.get_all_layers(cnn);
    lay = lays[1];
    print (lay);
    lay_test_output_rst = lasagne.layers.get_output(lay, deterministic=True)

    #print(lay.flip_filters)
    #lay_test_output = lasagne.layers.get_all_param_values(lay)
    #print(test_set.X)
    #for p in lay_test_output:
#	print(p.shape)
        #print(p.sum())
#	print (p)
    #print(lay_test_output)
    val_fn2 = theano.function([input], lay_test_output_rst)

    test_ = val_fn2(test_set.X)
    print(test_.sum());
    print(test_.shape);
    #for i in range(128):
    #for j in range(32):
     # print(test_[0][0][j].sum());
    #print(test_[0][0][0][6:10].round())
    #print(test_[0][0][1][6:10].round())

    
   
    
