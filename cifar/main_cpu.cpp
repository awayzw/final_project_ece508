#include "stdlib.h"
#include "stdio.h"
#include "main_cpu.hpp"
#include "layer_functions.hpp"

int main(int argc, char ** argv){

  //const unsigned n_imgs = std::stoi(argv[1]);
  
  float *x = allocate<float>(xdims);
  float *x_pad = allocate<float>(xpdims);
  
  readdata("./params/cifar10_x200.bin", xdims, x);


  edge_padding(x, x_pad, 1, xdims);

  //layer1: conv1
  float *w1 = allocate<float>(w1dims);
  float *out1 = allocate1D<float>(BATCH_SIZE*1024*32*32);
  float *beta1 = allocate1D<float>(1024);
  float *gamma1 = allocate1D<float>(1024);
  float *mean1 = allocate1D<float>(1024);
  float *inv_std1 = allocate1D<float>(1024);
  float *b1 = allocate1D<float>(1024);
  float *w1f = allocate<float>(w1dims);

  readdata("./params/cifar10_W1", w1dims, w1);
  readdata("./params/cifar10_b1", out1dims.channels, b1);
  readdata("./params/cifar10_beta1", out1dims.channels, beta1);
  readdata("./params/cifar10_gamma1", out1dims.channels, gamma1);
  readdata("./params/cifar10_mean1", out1dims.channels, mean1);
  readdata("./params/cifar10_inv_std1", out1dims.channels, inv_std1);
  readdata("./params/wT1.bin", w1dims, w1f);



  // the 1st conv layer
  //this can only be real number conv2D
  conv2D(x_pad, xpdims, w1, b1, w1dims, out1, out1dims);
  //conv2D_org(x_pad, xpdims, w1f, b1, w1dims, out1, out1dims);
  //////print_acc(out1, out1dims.batched_length(), "out1");
  delete [] w1f;

  //This  is NOT the same as MLP batch norm
  batch_normalization(beta1, gamma1, mean1, inv_std1, out1, out1dims);
  tanh(out1, out1dims.batched_length());

  //layer2
  float *out1_pad = allocate<float>(out1pdims);
  edge_padding(out1, out1_pad, 1, out1dims);

  float *w2 = allocate<float>(w2dims);
  //reuse the following parameter pointers for layer1, to save space
  float *out2 = out1;
  float *beta2 = beta1;
  float *gamma2 = gamma1;
  float *mean2 = mean1;
  float *inv_std2 = inv_std1;
  float *b2 = b1;

  float *out2_pool = allocate<float>(out2pooldims);

  readdata("./params/cifar10_W2", w2dims, w2);

  readdata("./params/cifar10_b2", out2dims.channels, b2);

  readdata("./params/cifar10_beta2", out2dims.channels, beta2);

  readdata("./params/cifar10_gamma2", out2dims.channels, gamma2);

  readdata("./params/cifar10_mean2", out2dims.channels, mean2);

  readdata("./params/cifar10_inv_std2", out2dims.channels, inv_std2);
//  //print_acc(inv_std2, out1dims.channels);

  // the 2nd conv layer
  //this can only be binary number conv2D
  conv2D(out1_pad, out1pdims, w2, b2, w2dims, out2, out2dims);
  maxPool(out2, out2_pool, out2dims, out2pooldims, 2);
  batch_normalization(beta2, gamma2, mean2, inv_std2, out2_pool, out2pooldims);
  tanh(out2_pool, out2pooldims.batched_length());
  //print_acc(out2_pool, out2pooldims.batched_length(), "out2_tanh");

  //layer 3
  float *out2_pad = allocate<float>(out1pdims);
  edge_padding(out2_pool, out2_pad, 1, out2pooldims);

  float *w3 = allocate<float>(w3dims);
  //reuse the following parameter pointers for layer1, to save space
  float *out3 = out1;
  float *beta3 = beta1;
  float *gamma3 = gamma1;
  float *mean3 = mean1;
  float *inv_std3 = inv_std1;
  float *b3 = b1;

  readdata("./params/cifar10_W3", w3dims, w3);
//  //print_acc(w3, w3dims.batched_length());

  readdata("./params/cifar10_b3", out3dims.channels, b3);
//  //print_acc(b3, out1dims.channels);

  readdata("./params/cifar10_beta3", out3dims.channels, beta3);
//  //print_acc(beta3, out1dims.channels);

  readdata("./params/cifar10_gamma3", out3dims.channels, gamma3);
//  //print_acc(gamma3, out1dims.channels);

  readdata("./params/cifar10_mean3", out3dims.channels, mean3);
//  //print_acc(mean3, out1dims.channels);

  readdata("./params/cifar10_inv_std3", out3dims.channels, inv_std3);
//  //print_acc(inv_std3, out1dims.channels);

  // the 3rd conv layer
  //this can only be binary number conv2D
  conv2D(out2_pad, out2pdims, w3, b3, w3dims, out3, out3dims);
  //print_acc(out3, out3dims.batched_length(), "out3");
  batch_normalization(beta3, gamma3, mean3, inv_std3, out3, out3dims);
  tanh(out3, out3dims.batched_length());
  //print_acc(out3, out3dims.batched_length(), "out3_tanh");

  //layer 4
  float *out3_pad = allocate<float>(out3pdims);
  edge_padding(out3, out3_pad, 1, out3dims);

  float *w4 = allocate<float>(w4dims);
  //reuse the following parameter pointers for layer1, to save space
  float *out4 = out1;
  float *beta4 = beta1;
  float *gamma4 = gamma1;
  float *mean4 = mean1;
  float *inv_std4 = inv_std1;
  float *b4 = b1;

  float *out4_pool = allocate<float>(out4pooldims);

  readdata("./params/cifar10_W4", w4dims, w4);
  readdata("./params/cifar10_b4", out4dims.channels, b4);
  readdata("./params/cifar10_beta4", out4dims.channels, beta4);
  readdata("./params/cifar10_gamma4", out4dims.channels, gamma4);
  readdata("./params/cifar10_mean4", out4dims.channels, mean4);
  readdata("./params/cifar10_inv_std4", out4dims.channels, inv_std4);

  // the 4th conv layer
  //this can only be binary number conv2D
  conv2D(out3_pad, out3pdims, w4, b4, w4dims, out4, out4dims);
  //print_acc(out4, out4dims.batched_length(), "out4");
  maxPool(out4, out4_pool, out4dims, out4pooldims, 2);
  batch_normalization(beta4, gamma4, mean4, inv_std4, out4_pool, out4pooldims);
  tanh(out4_pool, out4pooldims.batched_length());
  //print_acc(out4_pool, out4pooldims.batched_length(), "out4_tanh");

  //layer 5
  float *out4_pad = allocate<float>(out4pdims);
  edge_padding(out4_pool, out4_pad, 1, out4pooldims);

  float *w5 = allocate<float>(w5dims);
  //reuse the following parameter pointers for layer1, to save space
  float *out5 = out1;
  float *beta5 = beta1;
  float *gamma5 = gamma1;
  float *mean5 = mean1;
  float *inv_std5 = inv_std1;
  float *b5 = b1;

  readdata("./params/cifar10_W5", w5dims, w5);
  readdata("./params/cifar10_b5", out5dims.channels, b5);
  readdata("./params/cifar10_beta5", out5dims.channels, beta5);
  readdata("./params/cifar10_gamma5", out5dims.channels, gamma5);
  readdata("./params/cifar10_mean5", out5dims.channels, mean5);
  readdata("./params/cifar10_inv_std5", out5dims.channels, inv_std5);

  // the 5th conv layer
  //this can only be binary number conv2D
  conv2D(out4_pad, out4pdims, w5, b5, w5dims, out5, out5dims);
  //print_acc(out5, out5dims.batched_length(), "out5");
  batch_normalization(beta5, gamma5, mean5, inv_std5, out5, out5dims);
  tanh(out5, out5dims.batched_length());
  //print_acc(out5, out5dims.batched_length(), "out5_tanh");

  //layer 6
  float *out5_pad = allocate<float>(out5pdims);
  edge_padding(out5, out5_pad, 1, out5dims);

  float *w6 = allocate<float>(w6dims);
  //reuse the following parameter pointers for layer1, to save space
  float *out6 = out1;
  float *beta6 = beta1;
  float *gamma6 = gamma1;
  float *mean6 = mean1;
  float *inv_std6 = inv_std1;
  float *b6 = b1;

  float *out6_pool = allocate<float>(out6pooldims);

  readdata("./params/cifar10_W6", w6dims, w6);
  readdata("./params/cifar10_b6", out6dims.channels, b6);
  readdata("./params/cifar10_beta6", out6dims.channels, beta6);
  readdata("./params/cifar10_gamma6", out6dims.channels, gamma6);
  readdata("./params/cifar10_mean6", out6dims.channels, mean6);
  readdata("./params/cifar10_inv_std6", out6dims.channels, inv_std6);

  // the 6th conv layer
  //this can only be binary number conv2D
  conv2D(out5_pad, out5pdims, w6, b6, w6dims, out6, out6dims);
  //print_acc(out6, out6dims.batched_length(), "out6");
  maxPool(out6, out6_pool, out6dims, out6pooldims, 2);
  batch_normalization(beta6, gamma6, mean6, inv_std6, out6_pool, out6pooldims);
  tanh(out6_pool, out6pooldims.batched_length());
  //print_acc(out6_pool, out6pooldims.batched_length(), "out6_tanh");
//
//  //layer 7
//  float *w7 = allocate<float>(w7dims);
//  //reuse the following parameter pointers for layer1, to save space
//  float *out7 = out1;
//  float *beta7 = beta1;
//  float *gamma7 = gamma1;
//  float *mean7 = mean1;
//  float *inv_std7 = inv_std1;
//  float *b7 = b1;
//
//  readdata("./params/cifar10_W7", w7dims, w7);
//  readdata("./params/cifar10_b7", out7dims.channels, b7);
//  readdata("./params/cifar10_beta7", out7dims.channels, beta7);
//  readdata("./params/cifar10_gamma7", out7dims.channels, gamma7);
//  readdata("./params/cifar10_mean7", out7dims.channels, mean7);
//  readdata("./params/cifar10_inv_std7", out7dims.channels, inv_std7);
//
//  // the 1st fc layer
//  //this can only be binary number conv2D
//  gemm_cpu(out6_pool, w7, out7, out6pooldims.num, out6pooldims.length(), w7dims.length(), b7);
//  //print_acc(out7, out7dims.batched_length(), "out7");
//  batch_normalization(beta7, gamma7, mean7, inv_std7, out7, out7dims);
//  tanh(out7, out7dims.batched_length());
//  //print_acc(out7, out7dims.batched_length(), "out7_tanh");
//
//  //layer 8
//  float *w8 = allocate<float>(w8dims);
//  //reuse the following parameter pointers for layer1, to save space
//  float *out8 = allocate<float>(out8dims);
//  float *beta8 = beta1;
//  float *gamma8 = gamma1;
//  float *mean8 = mean1;
//  float *inv_std8 = inv_std1;
//  float *b8 = b1;
//
//  readdata("./params/cifar10_W8", w8dims, w8);
//  readdata("./params/cifar10_b8", out8dims.channels, b8);
//  readdata("./params/cifar10_beta8", out8dims.channels, beta8);
//  readdata("./params/cifar10_gamma8", out8dims.channels, gamma8);
//  readdata("./params/cifar10_mean8", out8dims.channels, mean8);
//  readdata("./params/cifar10_inv_std8", out8dims.channels, inv_std8);
//
//  // the 2nd fc layer
//  //this can only be binary number conv2D
//  gemm_cpu(out7, w8, out8, out7dims.num, out7dims.length(), w8dims.length(), b8);
//  //print_acc(out8, out8dims.batched_length(), "out8");
//  batch_normalization(beta8, gamma8, mean8, inv_std8, out8, out8dims);
//  tanh(out8, out8dims.batched_length());
//  //print_acc(out8, out8dims.batched_length(), "out8_tanh");
//
//  //layer 9
//  float *w9 = allocate<float>(w9dims);
//  //reuse the following parameter pointers for layer1, to save space
//  float *out9 = out1;
//  float *beta9 = beta1;
//  float *gamma9 = gamma1;
//  float *mean9 = mean1;
//  float *inv_std9 = inv_std1;
//  float *b9 = b1;
//
//  readdata("./params/cifar10_W9", w9dims, w9);
//  readdata("./params/cifar10_b9", out9dims.channels, b9);
//  readdata("./params/cifar10_beta9", out9dims.channels, beta9);
//  readdata("./params/cifar10_gamma9", out9dims.channels, gamma9);
//  readdata("./params/cifar10_mean9", out9dims.channels, mean9);
//  readdata("./params/cifar10_inv_std9", out9dims.channels, inv_std9);
//
//  // the 3rd fc layer
//  //this can only be binary number conv2D
//  gemm_cpu(out8, w9, out9, out8dims.num, out8dims.length(), w9dims.length(), b9);
//  //print_acc(out9, out9dims.batched_length(), "out9");
//  batch_normalization(beta9, gamma9, mean9, inv_std9, out9, out9dims);
//  //print_acc(out9, out9dims.batched_length(), "out9_bn");
//
////  float max_idx = 0;
////  for (int i = 0; i < out9dims.batched_length(); i++)
////  {
////    float max_value = 0;
////    if (max_value < out9[i])
////    {
////      max_idx = i; 
////      max_value = out9[i]; 
////    }
////  }
////  //print_acc(out9, out9dims.batched_length());
////  printf("Category: %f\n", max_idx);
//  unsigned int max_idx = 0;
//
//  for(int j = 0; j < out9dims.num; j++){
// 	for (int i = 0; i < out9dims.length(); i++)
// 	 {
// 	   float max_value = 0;
// 	   if (max_value < out9[j * out9dims.length() + i])
// 	   {
// 	     max_idx = j * out9dims.length() + i; 
// 	     max_value = out9[j*out9dims.length() + i]; 
// 	   }
// 	 }
//  	////print_acc(out1, out9dims.batched_length()());
//  	std::cout << "Category: " << max_idx%(out9dims.length() )<< std::endl;
//}


  delete [] x;
  delete [] x_pad;
  delete [] w1;
  delete [] out1;
  delete [] gamma1;
  delete [] mean1;
  delete [] inv_std1;
  delete [] b1;
  delete [] beta1;

  delete [] out1_pad;
  delete [] w2;
  delete [] out2_pool;

}

