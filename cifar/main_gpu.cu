#include "layer_functions.hpp"
#include "util_function.hpp"
#include <iostream>

using namespace std;

int main(int argc, char ** argv){

  //const unsigned n_imgs = std::stoi(argv[1]);
  
  float *x = allocate<float>(xdims);
  float *x_pad = allocate<float>(xpdims);

  float *device_x_pad;
  
  readdata("./params/cifar10_x200.bin", xdims, x);
  //print_acc(x, xdims.batched_length(), "x");

  edge_padding(x, x_pad, 1, xdims);

  //layer1: conv1
  float *w1 = allocate<float>(w1dims);
  float *out1 = allocate1D<float>(BATCH_SIZE*1024*32*32);
  float *beta1 = allocate1D<float>(1024);
  float *gamma1 = allocate1D<float>(1024);
  float *mean1 = allocate1D<float>(1024);
  float *inv_std1 = allocate1D<float>(1024);
  float *b1 = allocate1D<float>(1024);

  float *device_w1;
  float *device_out1;
  float *device_beta1; 
  float *device_gamma1;
  float *device_mean1;
  float *device_inv_std1;
  float *device_b1;

  //readdata("./params/cifar10_W1", w1dims, w1);
  readdata("./params/w1TT.bin", w1dims, w1);
  readdata("./params/cifar10_b1", out1dims.channels, b1);
  readdata("./params/cifar10_beta1", out1dims.channels, beta1);
  readdata("./params/cifar10_gamma1", out1dims.channels, gamma1);
  readdata("./params/cifar10_mean1", out1dims.channels, mean1);
  readdata("./params/cifar10_inv_std1", out1dims.channels, inv_std1);

  //float *w1t = allocate<float>(w1dims);
  //transpose(w1, w1t, w1dims.num, w1dims.length());

  check_success(cudaMalloc((void **)&device_x_pad, sizeof(float) * xpdims.batched_length()));
  check_success(cudaMalloc((void **)&device_w1, sizeof(float) * w1dims.batched_length()));
  check_success(cudaMalloc((void **)&device_b1, sizeof(float) * 1024));
  check_success(cudaMalloc((void **)&device_out1, sizeof(float) * BATCH_SIZE*1024*32*32)); 
  check_success(cudaMalloc((void **)&device_beta1, sizeof(float) * 1024));
  check_success(cudaMalloc((void **)&device_gamma1, sizeof(float) * 1024));
  check_success(cudaMalloc((void **)&device_mean1, sizeof(float) * 1024));
  check_success(cudaMalloc((void **)&device_inv_std1, sizeof(float) * 1024));
  
  check_success(cudaMemcpy(device_x_pad, x_pad, sizeof(float) * xpdims.batched_length(), cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_w1, w1, sizeof(float) * w1dims.batched_length(), cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_b1, b1, sizeof(float) * 1024, cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_beta1, beta1, sizeof(float) * 1024, cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_gamma1, gamma1, sizeof(float) * 1024, cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_mean1, mean1, sizeof(float) * 1024, cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_inv_std1, inv_std1, sizeof(float) * 1024, cudaMemcpyHostToDevice));

  // the 1st conv layer
  //this can only be real number conv2D
  conv2D_gpu(device_x_pad, xpdims, device_w1, device_b1, w1dims, device_out1, out1dims, device_beta1, device_gamma1, device_mean1, device_inv_std1);
  //conv2D(x_pad, xpdims, w1, b1, w1dims, out1, out1dims);
  check_success(cudaDeviceSynchronize());
  check_success(cudaMemcpy(out1, device_out1, sizeof(float) * out1dims.batched_length(), cudaMemcpyDeviceToHost));  
  check_success(cudaDeviceSynchronize());
  //print_acc(out1, out1dims.batched_length(), "out1 tanh");

  //This  is NOT the same as MLP batch norm
  //batch_normalization(beta1, gamma1, mean1, inv_std1, out1, out1dims);
  ////print_acc(out1, out1dims.batched_length(), "out1 batch_normalization");
  //tanh(out1, out1dims.batched_length());
  ////print_acc(out1, out1dims.batched_length(), "out1 tanh");

  //layer2
  float *out1_pad = allocate<float>(out1pdims);
  float *device_out1_pad;
  float *device_out2_pool; 

  edge_padding(out1, out1_pad, 1, out1dims);
  check_success(cudaMalloc((void **)&device_out1_pad, sizeof(float) * out1pdims.batched_length()));
  check_success(cudaMemcpy(device_out1_pad, out1_pad, sizeof(float) * out1pdims.batched_length(), cudaMemcpyHostToDevice));

  float *w2 = allocate<float>(w2dims);
  
  //reuse the following parameter pointers for layer1, to save space
  float *out2 = allocate<float>(out2dims);
  //float *out2 = out1;
  float *beta2 = beta1;
  float *gamma2 = gamma1;
  float *mean2 = mean1;
  float *inv_std2 = inv_std1;
  float *b2 = b1;

  float *device_w2; 
/*  float *device_out2 = device_out1;
  float *device_beta2 = device_beta1; 
  float *device_gamma2 = device_gamma1;
  float *device_mean2 = device_mean1;
  float *device_inv_std2 = device_inv_std1;
  float *device_b2 = device_b1;*/
  float *device_out2;
  float *device_beta2; 
  float *device_gamma2;
  float *device_mean2;
  float *device_inv_std2;
  float *device_b2;

  check_success(cudaMalloc((void **)&device_b2, sizeof(float) * out2dims.channels));
  check_success(cudaMalloc((void **)&device_out2, sizeof(float) * out2dims.batched_length())); 
  check_success(cudaMalloc((void **)&device_beta2, sizeof(float) * out2dims.channels));
  check_success(cudaMalloc((void **)&device_gamma2, sizeof(float) * out2dims.channels));
  check_success(cudaMalloc((void **)&device_mean2, sizeof(float) * out2dims.channels));
  check_success(cudaMalloc((void **)&device_inv_std2, sizeof(float) * out2dims.channels));

  float *out2_pool = allocate<float>(out2pooldims);
  check_success(cudaMalloc((void **)&device_w2, sizeof(float) * w2dims.batched_length()));
  check_success(cudaMalloc((void **)&device_out2_pool, sizeof(float) * out2pooldims.batched_length()));

  //readdata("./params/cifar10_W2", w2dims, w2);
  readdata("./params/w2TT.bin", w2dims, w2);
  readdata("./params/cifar10_b2", out2dims.channels, b2);
  readdata("./params/cifar10_beta2", out2dims.channels, beta2);
  readdata("./params/cifar10_gamma2", out2dims.channels, gamma2);
  readdata("./params/cifar10_mean2", out2dims.channels, mean2);
  readdata("./params/cifar10_inv_std2", out2dims.channels, inv_std2);

  //float *w2t = allocate<float>(w2dims);
  //transpose(w2, w2t, w2dims.num, w2dims.length());

  check_success(cudaMemcpy(device_w2, w2, sizeof(float) * w2dims.batched_length(), cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_b2, b2, sizeof(float) * out2dims.channels, cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_beta2, beta2, sizeof(float) * out2dims.channels, cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_gamma2, gamma2, sizeof(float) * out2dims.channels, cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_mean2, mean2, sizeof(float) * out2dims.channels, cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_inv_std2, inv_std2, sizeof(float) * out2dims.channels, cudaMemcpyHostToDevice));
  check_success(cudaDeviceSynchronize());
  // the 2nd conv layer
  //this can only be binary number conv2D
  conv2D_gpu_pooling(device_out1_pad, out1pdims, device_w2, device_b2, w2dims, device_out2, out2dims, device_out2_pool, out2pooldims, device_beta2, device_gamma2, device_mean2, device_inv_std2, 2);
  check_success(cudaDeviceSynchronize());  
  check_success(cudaMemcpy(out2_pool, device_out2_pool, sizeof(float) * out2pooldims.batched_length(), cudaMemcpyDeviceToHost));  
  check_success(cudaDeviceSynchronize());
  //print_acc(out2_pool, out2pooldims.batched_length(), "out2 final");

  //maxPool(out2, out2_pool, out2dims, out2pooldims, 2);
  //batch_normalization(beta2, gamma2, mean2, inv_std2, out2_pool, out2pooldims);
  //tanh(out2_pool, out2pooldims.batched_length());
  ////print_acc(out2_pool, out2pooldims.batched_length(), "out2_tanh");

  //layer 3
  float *out2_pad = allocate<float>(out1pdims);
  float *device_out2_pad;
  edge_padding(out2_pool, out2_pad, 1, out2pooldims);

  check_success(cudaMalloc((void **)&device_out2_pad, sizeof(float) * out2pdims.batched_length()));
  check_success(cudaMemcpy(device_out2_pad, out2_pad, sizeof(float) * out2pdims.batched_length(), cudaMemcpyHostToDevice));

  float *w3 = allocate<float>(w3dims);
  //reuse the following parameter pointers for layer1, to save space
  float *out3 = allocate<float>(out3dims);
  float *beta3 = beta1;
  float *gamma3 = gamma1;
  float *mean3 = mean1;
  float *inv_std3 = inv_std1;
  float *b3 = b1;

  //readdata("./params/cifar10_W3", w3dims, w3);
  readdata("./params/w3TT.bin", w3dims, w3);
  readdata("./params/cifar10_b3", out3dims.channels, b3);
  readdata("./params/cifar10_beta3", out3dims.channels, beta3);
  readdata("./params/cifar10_gamma3", out3dims.channels, gamma3);
  readdata("./params/cifar10_mean3", out3dims.channels, mean3);
  readdata("./params/cifar10_inv_std3", out3dims.channels, inv_std3);

  float *device_w3;
  float *device_out3;
  float *device_beta3; 
  float *device_gamma3;
  float *device_mean3;
  float *device_inv_std3;
  float *device_b3;

  check_success(cudaMalloc((void **)&device_w3, sizeof(float) * w3dims.batched_length()));
  check_success(cudaMalloc((void **)&device_b3, sizeof(float) * out3dims.channels));
  check_success(cudaMalloc((void **)&device_out3, sizeof(float) * out3dims.batched_length())); 
  check_success(cudaMalloc((void **)&device_beta3, sizeof(float) * out3dims.channels));
  check_success(cudaMalloc((void **)&device_gamma3, sizeof(float) * out3dims.channels));
  check_success(cudaMalloc((void **)&device_mean3, sizeof(float) * out3dims.channels));
  check_success(cudaMalloc((void **)&device_inv_std3, sizeof(float) * out3dims.channels));

  //float *w3t = allocate<float>(w3dims);
  //transpose(w3, w3t, w3dims.num, w3dims.length());

  check_success(cudaMemcpy(device_w3, w3, sizeof(float) * w3dims.batched_length(), cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_b3, b3, sizeof(float) * out3dims.channels, cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_beta3, beta3, sizeof(float) * out3dims.channels, cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_gamma3, gamma3, sizeof(float) * out3dims.channels, cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_mean3, mean3, sizeof(float) * out3dims.channels, cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_inv_std3, inv_std3, sizeof(float) * out3dims.channels, cudaMemcpyHostToDevice));
  check_success(cudaDeviceSynchronize());

  // the 3rd conv layer
  //this can only be binary number conv2D
  //conv2D(out2_pad, out2pdims, w3, b3, w3dims, out3, out3dims);
  conv2D_gpu(device_out2_pad, out2pdims, device_w3, device_b3, w3dims, device_out3, out3dims, device_beta3, device_gamma3, device_mean3, device_inv_std3);
  check_success(cudaDeviceSynchronize());
  check_success(cudaMemcpy(out3, device_out3, sizeof(float) * out3dims.batched_length(), cudaMemcpyDeviceToHost));  
  check_success(cudaDeviceSynchronize());
  //print_acc(out3, out3dims.batched_length(), "out3");
  //batch_normalization(beta3, gamma3, mean3, inv_std3, out3, out3dims);
  //tanh(out3, out3dims.batched_length());
  ////print_acc(out3, out3dims.batched_length(), "out3_tanh");

  //layer 4
  float *out3_pad = allocate<float>(out3pdims);
  float *device_out3_pad;

  edge_padding(out3, out3_pad, 1, out3dims);

  check_success(cudaMalloc((void **)&device_out3_pad, sizeof(float) * out3pdims.batched_length()));
  check_success(cudaMemcpy(device_out3_pad, out3_pad, sizeof(float) * out3pdims.batched_length(), cudaMemcpyHostToDevice));


  float *w4 = allocate<float>(w4dims);
  //reuse the following parameter pointers for layer1, to save space
  float *out4 = allocate<float>(out4dims);
  float *beta4 = beta1;
  float *gamma4 = gamma1;
  float *mean4 = mean1;
  float *inv_std4 = inv_std1;
  float *b4 = b1;

  float *out4_pool = allocate<float>(out4pooldims);
  float *device_out4_pool;
  check_success(cudaMalloc((void **)&device_out4_pool, sizeof(float) * out4pooldims.batched_length()));

  //readdata("./params/cifar10_W4", w4dims, w4);
  readdata("./params/w4TT.bin", w4dims, w4);
  readdata("./params/cifar10_b4", out4dims.channels, b4);
  readdata("./params/cifar10_beta4", out4dims.channels, beta4);
  readdata("./params/cifar10_gamma4", out4dims.channels, gamma4);
  readdata("./params/cifar10_mean4", out4dims.channels, mean4);
  readdata("./params/cifar10_inv_std4", out4dims.channels, inv_std4);

  float *device_w4;
  float *device_out4;
  float *device_beta4; 
  float *device_gamma4;
  float *device_mean4;
  float *device_inv_std4;
  float *device_b4;

  check_success(cudaMalloc((void **)&device_w4, sizeof(float) * w4dims.batched_length()));
  check_success(cudaMalloc((void **)&device_b4, sizeof(float) * out4dims.channels));
  check_success(cudaMalloc((void **)&device_out4, sizeof(float) * out4dims.batched_length())); 
  check_success(cudaMalloc((void **)&device_beta4, sizeof(float) * out4dims.channels));
  check_success(cudaMalloc((void **)&device_gamma4, sizeof(float) * out4dims.channels));
  check_success(cudaMalloc((void **)&device_mean4, sizeof(float) * out4dims.channels));
  check_success(cudaMalloc((void **)&device_inv_std4, sizeof(float) * out4dims.channels));

  //float *w4t = allocate<float>(w4dims);
  //transpose(w4, w4t, w4dims.num, w4dims.length());

  check_success(cudaMemcpy(device_w4, w4, sizeof(float) * w4dims.batched_length(), cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_b4, b4, sizeof(float) * out4dims.channels, cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_beta4, beta4, sizeof(float) * out4dims.channels, cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_gamma4, gamma4, sizeof(float) * out4dims.channels, cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_mean4, mean4, sizeof(float) * out4dims.channels, cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_inv_std4, inv_std4, sizeof(float) * out4dims.channels, cudaMemcpyHostToDevice));
  check_success(cudaDeviceSynchronize());


  // the 4th conv layer
  //this can only be binary number conv2D
  //conv2D(out3_pad, out3pdims, w4, b4, w4dims, out4, out4dims);
  conv2D_gpu_pooling(device_out3_pad, out3pdims, device_w4, device_b4, w4dims, device_out4, out4dims, device_out4_pool, out4pooldims, device_beta4, device_gamma4, device_mean4, device_inv_std4, 2);
  check_success(cudaDeviceSynchronize());
  check_success(cudaMemcpy(out4_pool, device_out4_pool, sizeof(float) * out4pooldims.batched_length(), cudaMemcpyDeviceToHost));  
  check_success(cudaDeviceSynchronize());
  //print_acc(out4_pool, out4pooldims.batched_length(), "out4 final");
  //maxPool(out4, out4_pool, out4dims, out4pooldims, 2);
  //batch_normalization(beta4, gamma4, mean4, inv_std4, out4_pool, out4pooldims);
  //tanh(out4_pool, out4pooldims.batched_length());
  ////print_acc(out4_pool, out4pooldims.batched_length(), "out4_tanh");

  //layer 5
  float *out4_pad = allocate<float>(out4pdims);
  float *device_out4_pad;
  edge_padding(out4_pool, out4_pad, 1, out4pooldims);

  check_success(cudaMalloc((void **)&device_out4_pad, sizeof(float) * out4pdims.batched_length()));
  check_success(cudaMemcpy(device_out4_pad, out4_pad, sizeof(float) * out4pdims.batched_length(), cudaMemcpyHostToDevice));

  float *w5 = allocate<float>(w5dims);
  //reuse the following parameter pointers for layer1, to save space
  float *out5 = allocate<float>(out5dims);
  float *beta5 = beta1;
  float *gamma5 = gamma1;
  float *mean5 = mean1;
  float *inv_std5 = inv_std1;
  float *b5 = b1;

  //readdata("./params/cifar10_W5", w5dims, w5);
  readdata("./params/w5TT.bin", w5dims, w5);
  readdata("./params/cifar10_b5", out5dims.channels, b5);
  readdata("./params/cifar10_beta5", out5dims.channels, beta5);
  readdata("./params/cifar10_gamma5", out5dims.channels, gamma5);
  readdata("./params/cifar10_mean5", out5dims.channels, mean5);
  readdata("./params/cifar10_inv_std5", out5dims.channels, inv_std5);

  float *device_w5;
  float *device_out5;
  float *device_beta5; 
  float *device_gamma5;
  float *device_mean5;
  float *device_inv_std5;
  float *device_b5;

  check_success(cudaMalloc((void **)&device_w5, sizeof(float) * w5dims.batched_length()));
  check_success(cudaMalloc((void **)&device_b5, sizeof(float) * out5dims.channels));
  check_success(cudaMalloc((void **)&device_out5, sizeof(float) * out5dims.batched_length())); 
  check_success(cudaMalloc((void **)&device_beta5, sizeof(float) * out5dims.channels));
  check_success(cudaMalloc((void **)&device_gamma5, sizeof(float) * out5dims.channels));
  check_success(cudaMalloc((void **)&device_mean5, sizeof(float) * out5dims.channels));
  check_success(cudaMalloc((void **)&device_inv_std5, sizeof(float) * out5dims.channels));

  //float *w5t = allocate<float>(w5dims);
  //transpose(w5, w5t, w5dims.num, w5dims.length());

  check_success(cudaMemcpy(device_w5, w5, sizeof(float) * w5dims.batched_length(), cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_b5, b5, sizeof(float) * out5dims.channels, cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_beta5, beta5, sizeof(float) * out5dims.channels, cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_gamma5, gamma5, sizeof(float) * out5dims.channels, cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_mean5, mean5, sizeof(float) * out5dims.channels, cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_inv_std5, inv_std5, sizeof(float) * out5dims.channels, cudaMemcpyHostToDevice));
  check_success(cudaDeviceSynchronize());


  // the 5th conv layer
  //this can only be binary number conv4D
  //conv4D(out4_pad, out4pdims, w5, b5, w5dims, out5, out5dims);
  conv2D_gpu(device_out4_pad, out4pdims, device_w5, device_b5, w5dims, device_out5, out5dims, device_beta5, device_gamma5, device_mean5, device_inv_std5);
  check_success(cudaDeviceSynchronize());
  check_success(cudaMemcpy(out5, device_out5, sizeof(float) * out5dims.batched_length(), cudaMemcpyDeviceToHost));  
  check_success(cudaDeviceSynchronize());
  //print_acc(out5, out5dims.batched_length(), "out5 final");
  //batch_normalization(beta5, gamma5, mean5, inv_std5, out5, out5dims);
  //tanh(out5, out5dims.batched_length());
  ////print_acc(out5, out5dims.batched_length(), "out5_tanh");

  //layer 6
  float *out5_pad = allocate<float>(out5pdims);
  float *device_out5_pad;
  edge_padding(out5, out5_pad, 1, out5dims);

  check_success(cudaMalloc((void **)&device_out5_pad, sizeof(float) * out5pdims.batched_length()));
  check_success(cudaMemcpy(device_out5_pad, out5_pad, sizeof(float) * out5pdims.batched_length(), cudaMemcpyHostToDevice));

  float *w6 = allocate<float>(w6dims);
  //reuse the following parameter pointers for layer1, to save space
  float *out6 = allocate<float>(out6dims);
  float *beta6 = beta1;
  float *gamma6 = gamma1;
  float *mean6 = mean1;
  float *inv_std6 = inv_std1;
  float *b6 = b1;

  float *out6_pool = allocate<float>(out6pooldims);
  float *device_out6_pool;
  check_success(cudaMalloc((void **)&device_out6_pool, sizeof(float) * out6pooldims.batched_length()));

  //readdata("./params/cifar10_W6", w6dims, w6);
  readdata("./params/w6TT.bin", w6dims, w6);
  readdata("./params/cifar10_b6", out6dims.channels, b6);
  readdata("./params/cifar10_beta6", out6dims.channels, beta6);
  readdata("./params/cifar10_gamma6", out6dims.channels, gamma6);
  readdata("./params/cifar10_mean6", out6dims.channels, mean6);
  readdata("./params/cifar10_inv_std6", out6dims.channels, inv_std6);


  float *device_w6;
  float *device_out6;
  float *device_beta6; 
  float *device_gamma6;
  float *device_mean6;
  float *device_inv_std6;
  float *device_b6;

  check_success(cudaMalloc((void **)&device_w6, sizeof(float) * w6dims.batched_length()));
  check_success(cudaMalloc((void **)&device_b6, sizeof(float) * out6dims.channels));
  check_success(cudaMalloc((void **)&device_out6, sizeof(float) * out6dims.batched_length())); 
  check_success(cudaMalloc((void **)&device_beta6, sizeof(float) * out6dims.channels));
  check_success(cudaMalloc((void **)&device_gamma6, sizeof(float) * out6dims.channels));
  check_success(cudaMalloc((void **)&device_mean6, sizeof(float) * out6dims.channels));
  check_success(cudaMalloc((void **)&device_inv_std6, sizeof(float) * out6dims.channels));

  //float *w6t = allocate<float>(w6dims);
  //transpose(w6, w6t, w6dims.num, w6dims.length());

  check_success(cudaMemcpy(device_w6, w6, sizeof(float) * w6dims.batched_length(), cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_b6, b6, sizeof(float) * out6dims.channels, cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_beta6, beta6, sizeof(float) * out6dims.channels, cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_gamma6, gamma6, sizeof(float) * out6dims.channels, cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_mean6, mean6, sizeof(float) * out6dims.channels, cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_inv_std6, inv_std6, sizeof(float) * out6dims.channels, cudaMemcpyHostToDevice));
  check_success(cudaDeviceSynchronize());

  // the 6th conv layer
  //this can only be binary number conv2D
  //conv2D(out5_pad, out5pdims, w6, b6, w6dims, out6, out6dims);
  conv2D_gpu_pooling(device_out5_pad, out5pdims, device_w6, device_b6, w6dims, device_out6, out6dims, device_out6_pool, out6pooldims, device_beta6, device_gamma6, device_mean6, device_inv_std6, 2);
  check_success(cudaDeviceSynchronize());
  check_success(cudaMemcpy(out6_pool, device_out6_pool, sizeof(float) * out6pooldims.batched_length(), cudaMemcpyDeviceToHost));  
  check_success(cudaDeviceSynchronize());
  //print_acc(out6_pool, out6pooldims.batched_length(), "out6 final");
  //maxPool(out6, out6_pool, out6dims, out6pooldims, 2);
  //batch_normalization(beta6, gamma6, mean6, inv_std6, out6_pool, out6pooldims);
  //tanh(out6_pool, out6pooldims.batched_length());
  ////print_acc(out6_pool, out6pooldims.batched_length(), "out6_tanh");

//  //layer 7
//  float *w7 = allocate<float>(w7dims);
//  float *out6_pool_t = allocate<float>(out6pooldims);
//  //reuse the following parameter pointers for layer1, to save space
//  float *out7 = out1;
//  float *beta7 = beta1;
//  float *gamma7 = gamma1;
//  float *mean7 = mean1;
//  float *inv_std7 = inv_std1;
//  float *b7 = b1;
//
//  transpose(out6_pool, out6_pool_t, out6pooldims.num, out6pooldims.length());
//
//  readdata("./params/cifar10_W7", w7dims, w7);
//  readdata("./params/cifar10_b7", out7dims.channels, b7);
//  readdata("./params/cifar10_beta7", out7dims.channels, beta7);
//  readdata("./params/cifar10_gamma7", out7dims.channels, gamma7);
//  readdata("./params/cifar10_mean7", out7dims.channels, mean7);
//  readdata("./params/cifar10_inv_std7", out7dims.channels, inv_std7);
//
//  float *device_out6_pool_t; 
//  float *device_w7;
//  float *device_out7;
//  float *device_beta7; 
//  float *device_gamma7;
//  float *device_mean7;
//  float *device_inv_std7;
//  float *device_b7;
//
//  check_success(cudaMalloc((void **)&device_out6_pool_t, sizeof(float) * out6pooldims.batched_length()));
//  check_success(cudaMalloc((void **)&device_w7, sizeof(float) * w7dims.batched_length()));
//  check_success(cudaMalloc((void **)&device_b7, sizeof(float) * out7dims.channels));
//  check_success(cudaMalloc((void **)&device_out7, sizeof(float) * out7dims.batched_length())); 
//  check_success(cudaMalloc((void **)&device_beta7, sizeof(float) * out7dims.channels));
//  check_success(cudaMalloc((void **)&device_gamma7, sizeof(float) * out7dims.channels));
//  check_success(cudaMalloc((void **)&device_mean7, sizeof(float) * out7dims.channels));
//  check_success(cudaMalloc((void **)&device_inv_std7, sizeof(float) * out7dims.channels));
//
//  //float *w7t = allocate<float>(w7dims);
//  //transpose(w7, w7t, w7dims.num, w7dims.length());
//
//  check_success(cudaMemcpy(device_out6_pool_t, out6_pool_t, sizeof(float) * out6pooldims.batched_length(), cudaMemcpyHostToDevice));
//  check_success(cudaMemcpy(device_w7, w7, sizeof(float) * w7dims.batched_length(), cudaMemcpyHostToDevice));
//  check_success(cudaMemcpy(device_b7, b7, sizeof(float) * out7dims.channels, cudaMemcpyHostToDevice));
//  check_success(cudaMemcpy(device_beta7, beta7, sizeof(float) * out7dims.channels, cudaMemcpyHostToDevice));
//  check_success(cudaMemcpy(device_gamma7, gamma7, sizeof(float) * out7dims.channels, cudaMemcpyHostToDevice));
//  check_success(cudaMemcpy(device_mean7, mean7, sizeof(float) * out7dims.channels, cudaMemcpyHostToDevice));
//  check_success(cudaMemcpy(device_inv_std7, inv_std7, sizeof(float) * out7dims.channels, cudaMemcpyHostToDevice));
//  check_success(cudaDeviceSynchronize());
//
//  // the 1st fc layer
//  //this can only be binary number conv2D
//  gemm_gpu_org(device_w7, device_out6_pool_t, device_out7,
//     w7dims.num, w7dims.length(), 
//    out6pooldims.length(), out6pooldims.num, 
//    w7dims.length(), out6pooldims.num,
//     device_b7, device_beta7, device_gamma7, device_mean7, device_inv_std7);
//  check_success(cudaDeviceSynchronize());
//  check_success(cudaMemcpy(out7, device_out7, sizeof(float) * out7dims.batched_length(), cudaMemcpyDeviceToHost));  
//  check_success(cudaDeviceSynchronize());
//  //gemm_cpu(out6_pool, w7, out7, out6pooldims.num, out6pooldims.length(), w7dims.length(), b7);
//  //print_acc(out7, out7dims.batched_length(), "out7");
//  //batch_normalization(beta7, gamma7, mean7, inv_std7, out7, out7dims);
//  //tanh(out7, out7dims.batched_length());
//  ////print_acc(out7, out7dims.batched_length(), "out7_tanh");
//
//  //layer 8
//  float *w8 = allocate<float>(w8dims);
//  //reuse the following parameter pointers for layer1, to save space
//  float *out8 = allocate<float>(out8dims);
//  float *beta8 = beta1;
//  float *gamma8 = gamma1;
//  float *mean8 = mean1;
//  float *inv_std8 = inv_std1;
//  float *b8 = b1;
//
//  readdata("./params/cifar10_W8", w8dims, w8);
//  readdata("./params/cifar10_b8", out8dims.channels, b8);
//  readdata("./params/cifar10_beta8", out8dims.channels, beta8);
//  readdata("./params/cifar10_gamma8", out8dims.channels, gamma8);
//  readdata("./params/cifar10_mean8", out8dims.channels, mean8);
//  readdata("./params/cifar10_inv_std8", out8dims.channels, inv_std8);
//
//  float *device_w8;
//  float *device_out8;
//  float *device_beta8; 
//  float *device_gamma8;
//  float *device_mean8;
//  float *device_inv_std8;
//  float *device_b8;
//
//  check_success(cudaMalloc((void **)&device_w8, sizeof(float) * w8dims.batched_length()));
//  check_success(cudaMalloc((void **)&device_b8, sizeof(float) * out8dims.channels));
//  check_success(cudaMalloc((void **)&device_out8, sizeof(float) * out8dims.batched_length())); 
//  check_success(cudaMalloc((void **)&device_beta8, sizeof(float) * out8dims.channels));
//  check_success(cudaMalloc((void **)&device_gamma8, sizeof(float) * out8dims.channels));
//  check_success(cudaMalloc((void **)&device_mean8, sizeof(float) * out8dims.channels));
//  check_success(cudaMalloc((void **)&device_inv_std8, sizeof(float) * out8dims.channels));
//
//  //float *w8t = allocate<float>(w8dims);
//  //transpose(w8, w8t, w8dims.num, w8dims.length());
//
//  check_success(cudaMemcpy(device_w8, w8, sizeof(float) * w8dims.batched_length(), cudaMemcpyHostToDevice));
//  check_success(cudaMemcpy(device_b8, b8, sizeof(float) * out8dims.channels, cudaMemcpyHostToDevice));
//  check_success(cudaMemcpy(device_beta8, beta8, sizeof(float) * out8dims.channels, cudaMemcpyHostToDevice));
//  check_success(cudaMemcpy(device_gamma8, gamma8, sizeof(float) * out8dims.channels, cudaMemcpyHostToDevice));
//  check_success(cudaMemcpy(device_mean8, mean8, sizeof(float) * out8dims.channels, cudaMemcpyHostToDevice));
//  check_success(cudaMemcpy(device_inv_std8, inv_std8, sizeof(float) * out8dims.channels, cudaMemcpyHostToDevice));
//  check_success(cudaDeviceSynchronize());
//
//  // the 2nd fc layer
//  //this can only be binary number conv2D
//  //gemm_cpu(out7, w8, out8, out7dims.num, out7dims.length(), w8dims.length(), b8);
//  gemm_gpu_org(device_w8, device_out7, device_out8,
//     w8dims.num, w8dims.length(), 
//    out7dims.length(), out7dims.num, 
//    w8dims.length(), out7dims.num,
//     device_b8, device_beta8, device_gamma8, device_mean8, device_inv_std8);
//  check_success(cudaDeviceSynchronize());
//  check_success(cudaMemcpy(out8, device_out8, sizeof(float) * out8dims.batched_length(), cudaMemcpyDeviceToHost));  
//  check_success(cudaDeviceSynchronize());
//  //print_acc(out8, out8dims.batched_length(), "out8");
//  //batch_normalization(beta8, gamma8, mean8, inv_std8, out8, out8dims);
//  //tanh(out8, out8dims.batched_length());
//  ////print_acc(out8, out8dims.batched_length(), "out8_tanh");
//
//  //layer 9
//  float *w9 = allocate<float>(w9dims);
//  //reuse the following parameter pointers for layer1, to save space
//  float *out9 = out1;
//  float *beta9 = beta1;
//  float *gamma9 = gamma1;
//  float *mean9 = mean1;
//  float *inv_std9 = inv_std1;
//  float *b9 = b1;
//
//  readdata("./params/cifar10_W9", w9dims, w9);
//  readdata("./params/cifar10_b9", out9dims.channels, b9);
//  readdata("./params/cifar10_beta9", out9dims.channels, beta9);
//  readdata("./params/cifar10_gamma9", out9dims.channels, gamma9);
//  readdata("./params/cifar10_mean9", out9dims.channels, mean9);
//  readdata("./params/cifar10_inv_std9", out9dims.channels, inv_std9);
//
//  float *device_w9;
//  float *device_out9;
//  float *device_beta9; 
//  float *device_gamma9;
//  float *device_mean9;
//  float *device_inv_std9;
//  float *device_b9;
//
//  check_success(cudaMalloc((void **)&device_w9, sizeof(float) * w9dims.batched_length()));
//  check_success(cudaMalloc((void **)&device_b9, sizeof(float) * out9dims.channels));
//  check_success(cudaMalloc((void **)&device_out9, sizeof(float) * out9dims.batched_length())); 
//  check_success(cudaMalloc((void **)&device_beta9, sizeof(float) * out9dims.channels));
//  check_success(cudaMalloc((void **)&device_gamma9, sizeof(float) * out9dims.channels));
//  check_success(cudaMalloc((void **)&device_mean9, sizeof(float) * out9dims.channels));
//  check_success(cudaMalloc((void **)&device_inv_std9, sizeof(float) * out9dims.channels));
//
//  //float *w9t = allocate<float>(w9dims);
//  //transpose(w9, w9t, w9dims.num, w9dims.length());
//
//  check_success(cudaMemcpy(device_w9, w9, sizeof(float) * w9dims.batched_length(), cudaMemcpyHostToDevice));
//  check_success(cudaMemcpy(device_b9, b9, sizeof(float) * out9dims.channels, cudaMemcpyHostToDevice));
//  check_success(cudaMemcpy(device_beta9, beta9, sizeof(float) * out9dims.channels, cudaMemcpyHostToDevice));
//  check_success(cudaMemcpy(device_gamma9, gamma9, sizeof(float) * out9dims.channels, cudaMemcpyHostToDevice));
//  check_success(cudaMemcpy(device_mean9, mean9, sizeof(float) * out9dims.channels, cudaMemcpyHostToDevice));
//  check_success(cudaMemcpy(device_inv_std9, inv_std9, sizeof(float) * out9dims.channels, cudaMemcpyHostToDevice));
//  check_success(cudaDeviceSynchronize());
//
//  // the 3rd fc layer
//  //this can only be binary number conv2D
//  //gemm_cpu(out9, w9, out9, out9dims.num, out9dims.length(), w9dims.length(), b9);
//  gemm_gpu_org(device_w9, device_out8, device_out9,
//     w9dims.num, w9dims.length(), 
//    out8dims.length(), out8dims.num, 
//    w9dims.length(), out8dims.num,
//     device_b9, device_beta9, device_gamma9, device_mean9, device_inv_std9);
//  check_success(cudaDeviceSynchronize());
//  check_success(cudaMemcpy(out9, device_out9, sizeof(float) * out9dims.batched_length(), cudaMemcpyDeviceToHost));  
//  check_success(cudaDeviceSynchronize());
//  //print_acc(out9, out9dims.batched_length(), "out9");
//  //batch_normalization(beta9, gamma9, mean9, inv_std9, out9, out9dims);
//  ////print_acc(out9, out9dims.batched_length(), "out9_bn");
//
//  //float max_idx = 0;
//  //for (int i = 0; i < out9dims.batched_length(); i++)
//  //{
//  //  float max_value = 0;
//  //  if (max_value < out9[i])
//  //  {
//  //    max_idx = i; 
//  //    max_value = out9[i]; 
//  //  }
//  //}
//  ////print_acc(out9, out9dims.batched_length());
//  //printf("Category: %f\n", max_idx);
//  unsigned int max_idx = 0;
//
//  for(int j = 0; j < out9dims.num; j++){
// 	for (int i = 0; i < out9dims.length(); i++)
// 	 {
// 	   float max_value = 0;
// 	   if (max_value < out9[j * out9dims.length() + i])
// 	   {
// 	     max_idx = j * out9dims.length() + i; 
// 	     max_value = out9[j*out9dims.length() + i]; 
// 	   }
// 	 }
//  	////print_acc(out1, out9dims.batched_length()());
//  	std::cout << "Category: " << max_idx%(out9dims.length() )<< std::endl;
//}
//
  delete [] x;
  delete [] x_pad;
  delete [] w1;
  delete [] out1;
  delete [] gamma1;
  delete [] mean1;
  delete [] inv_std1;
  delete [] b1;
  delete [] beta1;

  delete [] out1_pad;
  delete [] w2;
  delete [] out2_pool;

}

