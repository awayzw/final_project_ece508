#include "main_cpu.hpp"
#include "util_function_wei.hpp"
int main(int argc, char ** argv){
	float *x = allocate<float>(xdims);
	float *x_pad = allocate<float>(xpdims);
	readdata("./params/cifar10_x200.bin", xdims, x);
  print_acc(x, xdims.batched_length(), "x");
	edge_padding(x, x_pad, 1, xdims);
	delete [] x;

	//reuse
	float *w1 = allocate<float>(w1dims);
	unsigned int *w1_int = allocate1D<unsigned int>(w7dims.batched_length()/32);
        float *out1 = allocate1D<float>(BATCH_SIZE*1024*32*32);
        float *beta1 = allocate1D<float>(1024);
        float *gamma1 = allocate1D<float>(1024);
        float *mean1 = allocate1D<float>(1024);
        float *inv_std1 = allocate1D<float>(1024);
	float *b1 = allocate1D<float>(1024);
	
	float *device_x_pad;

  	float *device_w1;
  	float *device_out1;
  	float *device_beta1; 
  	float *device_gamma1;
  	float *device_mean1;
  	float *device_inv_std1;
  	float *device_b1;
	float *device_x_unroll;
	unsigned int *device_x_unroll_c;
  	unsigned int *device_w1_int;
	float *device_out1_pool;

  	readdata("./params/w1TT.bin", w1dims, w1);
  	readdata("./params/cifar10_b1", out1dims.channels, b1);
  	readdata("./params/cifar10_beta1", out1dims.channels, beta1);
  	readdata("./params/cifar10_gamma1", out1dims.channels, gamma1);
  	readdata("./params/cifar10_mean1", out1dims.channels, mean1);
  	readdata("./params/cifar10_inv_std1", out1dims.channels, inv_std1);

  	check_success(cudaMalloc((void **)&device_x_pad, sizeof(float) * xpdims.batched_length()));
	check_success(cudaMalloc((void **)&device_x_unroll, sizeof(float) * out1urdims.length()));
  	check_success(cudaMalloc((void **)&device_w1, sizeof(float) * w1dims.batched_length()));
  	check_success(cudaMalloc((void **)&device_b1, sizeof(float) * 1024));
  	check_success(cudaMalloc((void **)&device_out1, sizeof(float) * BATCH_SIZE*1024*32*32)); 
  	check_success(cudaMalloc((void **)&device_beta1, sizeof(float) * 1024));
  	check_success(cudaMalloc((void **)&device_gamma1, sizeof(float) * 1024));
  	check_success(cudaMalloc((void **)&device_mean1, sizeof(float) * 1024));
  	check_success(cudaMalloc((void **)&device_inv_std1, sizeof(float) * 1024));
	check_success(cudaMalloc((void **)&device_x_unroll_c, sizeof(unsigned int) * out1urdims.length()/32));
	check_success(cudaMalloc((void **)&device_out1_pool, sizeof(float) * out2pooldims.batched_length()));
  	check_success(cudaMalloc((void **)&device_w1_int, sizeof(unsigned int) * w7dims.batched_length()/32));
 
  	check_success(cudaMemcpy(device_w1, w1, sizeof(float) * w1dims.batched_length(), cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_x_pad, x_pad, sizeof(float) * xpdims.batched_length(), cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_b1, b1, sizeof(float) * out1dims.channels, cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_beta1, beta1, sizeof(float) * out1dims.channels, cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_gamma1, gamma1, sizeof(float) * out1dims.channels, cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_mean1, mean1, sizeof(float) * out1dims.channels, cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_inv_std1, inv_std1, sizeof(float) * out1dims.channels, cudaMemcpyHostToDevice));


	//layer1
	unroll_X_gpu(BATCH_SIZE, xpdims.channels, xpdims.width, xpdims.height, w1dims.width, device_x_unroll, device_x_pad);
  	cudaDeviceSynchronize(); 


	cudaMemset(device_out1, 0, out1pdims.batched_length()*sizeof(float));

	gemm_gpu_nl(BATCH_SIZE, xpdims.channels, xpdims.width, xpdims.height, w1dims.width, device_w1, device_x_unroll, device_out1, 
		w1dims.length(), w1dims.num, x1urdims.height/BATCH_SIZE, x1urdims.width, 
		w1dims.num, x1urdims.width,
        	device_b1, device_beta1, device_gamma1, device_mean1, device_inv_std1, 1);	
  	cudaDeviceSynchronize(); 

	//layer2
  	readdata("./params/w2TT.bin", w2dims.batched_length()/32, w1_int);
  	readdata("./params/cifar10_b2", out2dims.channels, b1);
  	readdata("./params/cifar10_beta2", out2dims.channels, beta1);
  	readdata("./params/cifar10_gamma2", out2dims.channels, gamma1);
  	readdata("./params/cifar10_mean2", out2dims.channels, mean1);
  	readdata("./params/cifar10_inv_std2", out2dims.channels, inv_std1);
	
  	check_success(cudaMemcpy(device_w1_int, w1_int, sizeof(unsigned int) * w2dims.batched_length()/32, cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_b1, b1, sizeof(float) * out2dims.channels, cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_beta1, beta1, sizeof(float) * out2dims.channels, cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_gamma1, gamma1, sizeof(float) * out2dims.channels, cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_mean1, mean1, sizeof(float) * out2dims.channels, cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_inv_std1, inv_std1, sizeof(float) * out2dims.channels, cudaMemcpyHostToDevice));
	print_acc(mean1, out2dims.channels,"mean");

	unroll_X_gpu(BATCH_SIZE, out1pdims.channels, out1pdims.width, out1pdims.height, w2dims.width, device_x_unroll, device_out1);
  	cudaDeviceSynchronize(); 

	concatenate_cols_gpu(device_x_unroll, device_x_unroll_c, out1urdims.height,out1urdims.width);
  	cudaDeviceSynchronize(); 

	gemm_gpu_xnor(BATCH_SIZE, out1pdims.channels, out1pdims.width, out1pdims.height, w2dims.width, device_w1_int, device_x_unroll_c, device_out1, 
		w2dims.length()/32, w2dims.num, out1urdims.height/(BATCH_SIZE*32), x1urdims.width, 
		w2dims.num, x1urdims.width, device_b1);	
  	cudaDeviceSynchronize(); 

	cudaMemset(device_out1_pool, 0, out2pdims.batched_length()*sizeof(float));

	max_pool_gpu(device_out1, 2, device_out1_pool, BATCH_SIZE, out2pooldims.channels, out2pooldims.height, out2pooldims.width, device_beta1, device_gamma1, device_mean1,device_inv_std1, 1);
  	cudaDeviceSynchronize(); 

	//layer3
  	readdata("./params/w3TT.bin", w3dims.batched_length()/32, w1_int);
  	readdata("./params/cifar10_b3", out3dims.channels, b1);
  	readdata("./params/cifar10_beta3", out3dims.channels, beta1);
  	readdata("./params/cifar10_gamma3", out3dims.channels, gamma1);
  	readdata("./params/cifar10_mean3", out3dims.channels, mean1);
  	readdata("./params/cifar10_inv_std3", out3dims.channels, inv_std1);
	
  	check_success(cudaMemcpy(device_w1_int, w1_int, sizeof(unsigned int) * w3dims.batched_length()/32, cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_b1, b1, sizeof(float) * out3dims.channels, cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_beta1, beta1, sizeof(float) * out3dims.channels, cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_gamma1, gamma1, sizeof(float) * out3dims.channels, cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_mean1, mean1, sizeof(float) * out3dims.channels, cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_inv_std1, inv_std1, sizeof(float) * out3dims.channels, cudaMemcpyHostToDevice));

	unroll_X_gpu(BATCH_SIZE, out2pdims.channels, out2pdims.width, out2pdims.height, w3dims.width, device_x_unroll, device_out1);
  	cudaDeviceSynchronize(); 

	concatenate_cols_gpu(device_x_unroll, device_x_unroll_c, out2urdims.height,out2urdims.width);
  	cudaDeviceSynchronize(); 

	gemm_gpu_xnor_nl(BATCH_SIZE, out2pdims.channels, out2pdims.width, out2pdims.height, w3dims.width, device_w1_int, device_x_unroll_c, device_out1, 
		w3dims.length()/32, w3dims.num, out2urdims.height/(BATCH_SIZE*32), out2urdims.width, 
		w3dims.num, out2urdims.width,
        	device_b1, device_beta1, device_gamma1, device_mean1, device_inv_std1, 1);	
  	cudaDeviceSynchronize(); 


	//layer 4
  	readdata("./params/w4TT.bin", w4dims.batched_length()/32, w1_int);
  	readdata("./params/cifar10_b4", out4dims.channels, b1);
  	readdata("./params/cifar10_beta4", out4dims.channels, beta1);
  	readdata("./params/cifar10_gamma4", out4dims.channels, gamma1);
  	readdata("./params/cifar10_mean4", out4dims.channels, mean1);
  	readdata("./params/cifar10_inv_std4", out4dims.channels, inv_std1);
	
  	check_success(cudaMemcpy(device_w1_int, w1_int, sizeof(unsigned int) * w4dims.batched_length()/32, cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_b1, b1, sizeof(float) * out4dims.channels, cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_beta1, beta1, sizeof(float) * out4dims.channels, cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_gamma1, gamma1, sizeof(float) * out4dims.channels, cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_mean1, mean1, sizeof(float) * out4dims.channels, cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_inv_std1, inv_std1, sizeof(float) * out4dims.channels, cudaMemcpyHostToDevice));

	unroll_X_gpu(BATCH_SIZE, out3pdims.channels, out3pdims.width, out3pdims.height, w4dims.width, device_x_unroll, device_out1);
  	cudaDeviceSynchronize(); 

	concatenate_cols_gpu(device_x_unroll, device_x_unroll_c, out3urdims.height,out1urdims.width);
  	cudaDeviceSynchronize(); 

	gemm_gpu_xnor(BATCH_SIZE, out3pdims.channels, out3pdims.width, out3pdims.height, w4dims.width, device_w1_int, device_x_unroll_c, device_out1, 
		w4dims.length()/32, w4dims.num, out3urdims.height/(BATCH_SIZE*32), out3urdims.width, 
		w4dims.num, out2urdims.width, device_b1);	
  	cudaDeviceSynchronize(); 

	cudaMemset(device_out1_pool, 0, out4pdims.batched_length()*sizeof(float));

	max_pool_gpu(device_out1, 2, device_out1_pool, BATCH_SIZE, out4pooldims.channels, out4pooldims.height, out4pooldims.width, device_beta1, device_gamma1, device_mean1,device_inv_std1, 1);
  	cudaDeviceSynchronize(); 

	printf("bal\n");
	check_success(cudaMemcpy(out1, device_out1, sizeof(float) * out4pdims.batched_length(), cudaMemcpyDeviceToHost));
	printf("bal\n");
	print_acc(out1, out4pdims.batched_length(), "out1 tanh");

//layer 5
  	readdata("./params/w5TT.bin", w5dims.batched_length()/32, w1_int);
  	readdata("./params/cifar10_b5", out5dims.channels, b1);
  	readdata("./params/cifar10_beta5", out5dims.channels, beta1);
  	readdata("./params/cifar10_gamma5", out5dims.channels, gamma1);
  	readdata("./params/cifar10_mean5", out5dims.channels, mean1);
  	readdata("./params/cifar10_inv_std5", out5dims.channels, inv_std1);
	
  	check_success(cudaMemcpy(device_w1_int, w1_int, sizeof(unsigned int) * w5dims.batched_length()/32, cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_b1, b1, sizeof(float) * out5dims.channels, cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_beta1, beta1, sizeof(float) * out5dims.channels, cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_gamma1, gamma1, sizeof(float) * out5dims.channels, cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_mean1, mean1, sizeof(float) * out5dims.channels, cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_inv_std1, inv_std1, sizeof(float) * out5dims.channels, cudaMemcpyHostToDevice));

	unroll_X_gpu(BATCH_SIZE, out4pdims.channels, out4pdims.width, out4pdims.height, w5dims.width, device_x_unroll, device_out1);
  	cudaDeviceSynchronize(); 

	concatenate_cols_gpu(device_x_unroll, device_x_unroll_c, out4urdims.height,out4urdims.width);
  	cudaDeviceSynchronize(); 

	gemm_gpu_xnor_nl(BATCH_SIZE, out4pdims.channels, out4pdims.width, out4pdims.height, w5dims.width, device_w1_int, device_x_unroll_c, device_out1, 
		w5dims.length()/32, w5dims.num, out4urdims.height/(BATCH_SIZE*32), out4urdims.width, 
		w5dims.num, out4urdims.width,
        	device_b1, device_beta1, device_gamma1, device_mean1, device_inv_std1, 1);	

  	cudaDeviceSynchronize(); 

//layer 6
  	readdata("./params/w6TT.bin", w6dims.batched_length()/32, w1_int);
  	readdata("./params/cifar10_b6", out6dims.channels, b1);
  	readdata("./params/cifar10_beta6", out6dims.channels, beta1);
  	readdata("./params/cifar10_gamma6", out6dims.channels, gamma1);
  	readdata("./params/cifar10_mean6", out6dims.channels, mean1);
  	readdata("./params/cifar10_inv_std6", out6dims.channels, inv_std1);
	
  	check_success(cudaMemcpy(device_w1_int, w1_int, sizeof(unsigned int) * w6dims.batched_length()/32, cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_b1, b1, sizeof(float) * out6dims.channels, cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_beta1, beta1, sizeof(float) * out6dims.channels, cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_gamma1, gamma1, sizeof(float) * out6dims.channels, cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_mean1, mean1, sizeof(float) * out6dims.channels, cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_inv_std1, inv_std1, sizeof(float) * out6dims.channels, cudaMemcpyHostToDevice));

	unroll_X_gpu(BATCH_SIZE, out5pdims.channels, out5pdims.width, out5pdims.height, w6dims.width, device_x_unroll, device_out1);
  	cudaDeviceSynchronize(); 

	concatenate_cols_gpu(device_x_unroll, device_x_unroll_c, out5urdims.height,out5urdims.width);
  	cudaDeviceSynchronize(); 

	max_pool_gpu(device_out1, 2, device_out1_pool, BATCH_SIZE, out6pooldims.channels, out6pooldims.height, out6pooldims.width, device_beta1, device_gamma1, device_mean1,device_inv_std1, 1);
  	cudaDeviceSynchronize(); 

	check_success(cudaMemcpy(out1, device_out1, sizeof(float) * out6dims.batched_length(), cudaMemcpyDeviceToHost));
	print_acc(out1, out6dims.batched_length(), "out1 tanh");

	check_success(cudaMemcpy(out1, device_out1, sizeof(float) * out6dims.batched_length(), cudaMemcpyDeviceToHost));

  	delete [] w1; 
  	delete [] w1_int; 
  	delete [] out1;
  	delete [] beta1;
  	delete [] gamma1;
  	delete [] mean1;
  	delete [] inv_std1;
  	delete [] b1;

	check_success(cudaFree(device_x_unroll));
	check_success(cudaFree(device_x_pad));
	check_success(cudaFree(device_w1));
	check_success(cudaFree(device_w1_int));
	check_success(cudaFree(device_b1));
	check_success(cudaFree(device_out1));
	check_success(cudaFree(device_out1_pool));
	check_success(cudaFree(device_beta1));
	check_success(cudaFree(device_gamma1));
	check_success(cudaFree(device_mean1));
	check_success(cudaFree(device_inv_std1));
}
         
 
 
 
 

