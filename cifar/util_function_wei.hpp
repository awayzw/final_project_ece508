#ifndef _UTIL_FUNC_H1_
#define _UTIL_FUNC_H1_
#include "main_cpu.hpp"
#include "assert.h"
#define CUDA_MAX_NUM_THREADS 1024
#ifndef TILE_WIDTH_N
 #define TILE_WIDTH_N 32//16
#endif

#ifndef TILE_WIDTH_K
 #define TILE_WIDTH_K 1//4
#endif 

#ifndef TILE_WIDTH_M
  #define TILE_WIDTH_M TILE_WIDTH_N*TILE_WIDTH_K
#endif 

#include <iostream>
void gemm_gpu_nl(int N, int Cin, int W, int H, int K, float *A, float *B, float *C, int numARows,
    int numAColumns, int numBRows, int numBColumns,
    int numCRows, int numCColumns,
    float * b, float* beta, float* gamma, float* mean, float* inv_std, int padding) ;
__global__ void matrixMultiply_kernel_nl(int N, int Cin, int W, int H, int K, float *A, float *B, float *C,
    int numARows, int numAColumns,
    int numBRows, int numBColumns,
    int numCRows, int numCColumns,
    float * b, float* beta, float* gamma, float* mean, float* inv_std, int padding
    ) ;

void max_pool_gpu( float *X,  int pool_size, float *Y,  int B, int C, int H, int W, 
 float* beta, float* gamma, float* mean, float* inv_std, int padding);
__global__ void max_pool_kernel(float* X, int pool_size, float *Y,  int B, int C, int H, int W, 
 float* beta, float* gamma, float* mean, float* inv_std, int padding);

template <typename T>
static bool check_success(const T &err);

template <>
bool check_success<cudaError_t>(const cudaError_t &err) {
  const auto res = err == cudaSuccess;
  if (res == true) {
    return res;
  }
  std::cout << "Failed in CUDA. Error = " << cudaGetErrorString(err) << std::endl;
  assert(res);
  return res;
}
void edge_padding(float *in, float *out, int padSize, shape &dims);
void unroll_X_gpu(int N, int C, int W, int H, int K, float *X_unroll, float *X);
__global__ void unroll_X_kernel(int N, int C, int W, int H, int K, float *X_unroll, float *X);

__global__ void matrixMultiply_kernel_xnor(int N, int Cin, int W, int H, int K, unsigned int *A, unsigned int *B, float *C,
    int numARows, int numAColumns,
    int numBRows, int numBColumns,
    int numCRows, int numCColumns,
    float * b) ;
void gemm_gpu_xnor(int N, int Cin, int W, int H, int K, unsigned int *A, unsigned int *B, float *C, int numARows,
    int numAColumns, int numBRows, int numBColumns,
    int numCRows, int numCColumns,
    float * b) ;

__global__ void concatenate_cols_kernel(float *a, unsigned int *b, int m, int n);
__device__ unsigned int concatenate_gpu(float* array);
void concatenate_cols_gpu(float *a, unsigned int *b, int m, int n);

void gemm_gpu_xnor_nl(int N, int Cin, int W, int H, int K,unsigned int *A, unsigned int *B, float *C, int numARows,
    int numAColumns, int numBRows, int numBColumns,
    int numCRows, int numCColumns,
    float * b, float* beta, float* gamma, float* mean, float* inv_std, int padding) ;

__global__ void matrixMultiply_kernel_xnor_nl(int N, int Cin, int W, int H, int K, unsigned int *A, unsigned int *B, float *C,
    int numARows, int numAColumns,
    int numBRows, int numBColumns,
    int numCRows, int numCColumns,
    float * b, float* beta, float* gamma, float* mean, float* inv_std, int padding);
#endif
