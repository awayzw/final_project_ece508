#ifndef _MAIN_H_
#define _MAIN_H_

#include <chrono>
#include <string>
#include "stdlib.h"
#include "stdio.h"
#include <iostream>
#ifndef BATCH_SIZE
 #define BATCH_SIZE 1
#endif
#define IMG_CHANNELS 3
#define IMG_ROWS 32
#define IMG_COLS 32
#define N_LAYERS 9

struct shape2D {
	size_t height{0};
	size_t width{0};

	shape2D(size_t h_, size_t w_):height(h_), width(w_){}
	size_t length() const {
		return height * width;
	}
};

struct shape {
  size_t num{0};
  size_t channels{0};
  size_t height{0};
  size_t width{0};

  shape(size_t num_, size_t channels_, size_t h_, size_t w_):
    num(num_), channels(channels_), height(h_), width(w_){}

  size_t batched_length() const {
    return num * channels * height * width;
  }

  size_t length() const {
    return channels * height * width;
  }
};

template <typename T, typename ShapeT>          
static T* allocate(const ShapeT &shp) {    
  T *res = new T[shp.batched_length()];    
  return res;                         
}  

template <typename T>          
static T* allocate1D(const int Size) {    
  T *res = new T[Size];    
  return res;                         
}  

template<typename T>
static void readdata(std::string fileName, shape dims, T *output){
  FILE *fin;
  fin=fopen(fileName.c_str(), "rb");
  if (fin == NULL) {
        perror("Error\n");
    }
  for(int i = 0; i < dims.num; i++){
    fread(&output[i * dims.length()], sizeof(T), dims.length(), fin);
    for(int j = 0; j < dims.length(); j++){
    }
  }
  fclose(fin);
}

template<typename T>
static void readdata(std::string fileName, int Size, T *output){
  FILE *fin;
  fin=fopen(fileName.c_str(), "rb");
  fread(output, sizeof(T), Size, fin);
  fclose(fin);
}

template<typename T>
void print_acc(T* in, int size, std::string str=""){
    T acc1 = 0;
      for(int i = 0; i < size; i++){
      	    acc1 += in[i];
      	      }
        printf ("%s acc is %f\n", str.c_str(), acc1);
}

template<typename T>
void print_matrix(T *in, shape xdims){
  for(int i = 0; i < xdims.num; i++){
    for(int j  =0; j < xdims.channels; j++){
      for(int k =0; k<xdims.height; k++){
        for(int l = 0; l<xdims.width; l++){
          printf("%.1f\t", in[i*xdims.channels *xdims.height *xdims.width + j * xdims.height * xdims.width + k *xdims.width +l]);
        }
        printf("\n");
      }
    printf("\n");
    }
  }

}

static shape xdims = {BATCH_SIZE,3 , 32 ,32};

static shape xpdims = {BATCH_SIZE, IMG_CHANNELS, IMG_ROWS+2, IMG_COLS+2};

static shape w1dims = {128, 3, 3, 3};
static shape2D x1urdims = {BATCH_SIZE*3*3*3, 32*32};
static shape out1dims = {BATCH_SIZE, 128, 32, 32};

static shape out1pdims = {BATCH_SIZE, 128, 34, 34};
static shape w2dims = {128, 128, 3, 3};
static shape2D out1urdims = {BATCH_SIZE*128*3*3, 32*32}; // max

static shape out2dims = {BATCH_SIZE, 128, 32, 32};
static shape out2pooldims = {BATCH_SIZE, 128, 16, 16};

static shape out2pdims = {BATCH_SIZE, 128, 18, 18};
static shape2D out2urdims = {BATCH_SIZE*128*3*3, 32*32};
static shape w3dims = {256, 128, 3, 3};
static shape out3dims = {BATCH_SIZE, 256, 16, 16};

static shape out3pdims = {BATCH_SIZE, 256, 18, 18};
static shape2D out3urdims = {BATCH_SIZE*256*3*3, 16*16};
static shape w4dims = {256, 256, 3, 3};
static shape out4dims = {BATCH_SIZE, 256, 16, 16};
static shape out4pooldims = {BATCH_SIZE, 256, 8, 8};

static shape out4pdims = {BATCH_SIZE, 256, 10, 10};
static shape2D out4urdims = {BATCH_SIZE*256*3*3, 8*8};
static shape w5dims = {512, 256, 3, 3};
static shape out5dims = {BATCH_SIZE, 512, 8, 8};


static shape out5pdims = {BATCH_SIZE, 512, 10, 10};
static shape2D out5urdims = {BATCH_SIZE*512*3*3, 8*8};
static shape w6dims = {512, 512, 3, 3};
static shape out6dims = {BATCH_SIZE, 512, 8, 8};
static shape out6pooldims = {BATCH_SIZE, 512, 4, 4};

static shape w7dims = {8192, 1024, 1, 1};
static shape out7dims = {BATCH_SIZE, 1024, 1, 1};

static shape w8dims = {1024, 1024, 1, 1};
static shape out8dims = {BATCH_SIZE, 1024, 1, 1};

static shape w9dims = {1024, 10, 1, 1};
static shape out9dims = {BATCH_SIZE, 10, 1, 1};
//function declaration
#endif
