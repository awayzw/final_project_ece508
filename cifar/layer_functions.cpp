#include "layer_functions.hpp"
void edge_padding(float *in, float *out, int padSize, shape &dims){
  int bs = dims.num;
  int cout = dims.channels;
  int hout = dims.height + 2*padSize;
  int wout = dims.width + 2*padSize;
  for(int i = 0; i < dims.num; i ++){
    for(int j = 0; j < dims.channels; j++){
      //the first row is 0 and last row also;
      for(int w =0; w < wout; w++){
        out[ i * (cout * hout * wout) + j * hout * wout + 0*wout + w] = 0.0;
        out[ i * (cout * hout * wout) + j * hout * wout + (hout-1)*wout + w] = 0.0;
      }
      //the first and last colum is 0;
      for(int h = 1; h < dims.height+1; h++){ 
        out[ i * (cout * hout * wout) + j * hout * wout + h*wout + 0] = 0.0;
        for(int w = 1; w < dims.width+1; w++){
          out[ i * (cout * hout * wout) + j * hout * wout + h*wout + w] = in[i * (dims.channels * dims.height * dims.width) + j * dims.height *dims.width + (h-1) *dims.width + w-1];
        }
        out[ i * (cout * hout * wout) + j * hout * wout + h*wout + wout-1] = 0.0;
      }
    }
  }
} 

void batch_normalization(float* beta, float* gamma, float* mean, float* inv_std, float *x, shape &dims){
  int batch_num = dims.num;
  int channel = dims.channels;
  int onebatchsize = dims.length();
  int oneImgSize = dims.height * dims.width;
  for( int j = 0; j < batch_num; j++){
    for (int c = 0; c < dims.channels; c++){
      for(int p = 0; p < dims.height; p++){
        for (int q = 0; q< dims.width; q++){
          int xoffset = j * onebatchsize + c*oneImgSize + p *dims.width + q;
          x[xoffset] = (x[xoffset] - mean[c] ) * gamma[c] * inv_std[c] + beta[c];
        }
      }
    }
  }
}


//This is NOT the normal CONV-2D, the fliped the weights, in python weight_flip = weight[:, ::-1, ::-1, :];
void conv2D(const float *X, const shape &xdims, const float *W, const float * b, const shape &wdims, float *Y, const shape &ydims){
  for (int i = 0; i < ydims.num; i++) {
    for (int m = 0; m < ydims.channels; m++) {    // for each output feature map
      for (int h =0; h < ydims.height; h++){ // for each output element
        for (int w = 0; w < ydims.width; w++){
          //int m = 0; int h  =0; int w = 0;
          int yoffset = ((i * ydims.channels + m) * ydims.height + h) * ydims.width + w;
          float sum  = 0;
          for (int  c = 0; c < xdims.channels; c++){     // sum over all input feature maps
            for (int p = 0; p < wdims.height; p++){  // filter height
              for (int q = 0; q < wdims.width; q++){ // filter width
                int xoffset = ((((i * xdims.channels) + c) * xdims.height) + (h + p)) * xdims.width + (w + q);
                //int woffset_flip = ((((m * wdims.channels) + c) * wdims.height) + p) * wdims.width + q;
                int woffset_flip = ((((m * wdims.channels) + c) * wdims.height) + wdims.height-1-p) * wdims.width + wdims.width-1-q;
                sum += X[xoffset] * W[woffset_flip];
              }
            }
          }
          Y[yoffset] = sum + b[m];
        }
      }
    }
  }
}

//return 2* round(hard_sigmoid(x))-1
void tanh(float *x, int size){
  for(int i = 0; i < size; i++){
    float tmp = (x[i]+1)/2;
    //float hard_sigmoid_x= (tmp < 0)? 0:((tmp > 1)?1:tmp);
    float hard_sigmoid_round_x= (tmp <= 0.5)? 0:1;
    //float hard_sigmoid_round_x = (hard_sigmoid_x <= 0.5) ? 0: 1;

    x[i] = 2 * hard_sigmoid_round_x - 1;
  }
}

void maxPool(const float *X, float* Y,  const shape &xdims, const shape &ydims, const int pool_size) {
  for(int i = 0; i < ydims.num; i++){
    for(int m = 0; m < ydims.channels; m++){
      for(int h = 0; h < ydims.height; h++){
        for(int w = 0; w < ydims.width; w++){
          const auto yoffset = ((i * ydims.channels + m) * ydims.height + h) * ydims.width + w;
          float tmp = X[((((i * xdims.channels) + m) * xdims.height) + (pool_size * h)) * xdims.width + (pool_size * w )];
          for (int p =0; p < pool_size; p++){
            for (int q = 0; q < pool_size; q++){
              const auto xoffset =
                ((((i * xdims.channels) + m) * xdims.height) + (pool_size * h + p)) * xdims.width + (pool_size * w + q);
              tmp= tmp > X[xoffset] ? tmp: X[xoffset];
            }
          }
          Y[yoffset] = tmp;
        }
      }
    }
  }
}

void gemm_cpu(float* A, float* B, float *C, int A_row, int A_col, int B_col, float *b){
  for(int i = 0; i < A_row; i++){
    for(int j =0; j < B_col; j++){
      float acc = b[j];
      for(int k =0; k < A_col; k++){
        acc += A[i *A_col + k] * B[ k *B_col + j];
      }
      C[i * B_col + j] = acc;
    }
  }
}

//void xnor_gemm_cpu(unsigned int* A, unsigned int* B, float* C, int A_row, int A_col, int B_col, float* b){
void xnor_gemm_cpu(float* A, float* B, float* C, int A_row, int A_col, int B_col, float* b){
  for(int i = 0; i < A_row; i++){
    for(int j =0; j < B_col; j++){
      float acc = b[j];
      for(int k =0; k < A_col; k++){
        unsigned int A_tmp = (unsigned int)A[i *A_col + k];
        unsigned int B_tmp = (unsigned int)B[ k *B_col + j];
        acc += (float)__builtin_popcount(A_tmp ^ B_tmp);
        //acc += (A_tmp ^ B_tmp);
      }
      C[i * B_col + j] = -(2*acc-(float)32*A_col);
    }
  }
}

void conv2D_org(const float *X, const shape &xdims, const float *W, const float * b, const shape &wdims, float *Y, const shape &ydims){
  for (int i = 0; i < ydims.num; i++) {
    for (int m = 0; m < ydims.channels; m++) {    // for each output feature map
      for (int h =0; h < ydims.height; h++){ // for each output element
        for (int w = 0; w < ydims.width; w++){
          //int m = 0; int h  =0; int w = 0;
          int yoffset = ((i * ydims.channels + m) * ydims.height + h) * ydims.width + w;
          float sum  = 0;
          for (int  c = 0; c < xdims.channels; c++){     // sum over all input feature maps
            for (int p = 0; p < wdims.height; p++){  // filter height
              for (int q = 0; q < wdims.width; q++){ // filter width
                int xoffset = ((((i * xdims.channels) + c) * xdims.height) + (h + p)) * xdims.width + (w + q);
                int woffset = ((((m * wdims.channels) + c) * wdims.height) + p) * wdims.width + q;
                //int woffset_flip = ((((m * wdims.channels) + c) * wdims.height) + wdims.height-1-p) * wdims.width + wdims.width-1-q;
                //sum += X[xoffset] * W[woffset_flip];
                sum += X[xoffset] * W[woffset];
              }
            }
          }
          Y[yoffset] = sum + b[m];
        }
      }
    }
  }
}
