#include "main_cpu.hpp"
#include "layer_functions.hpp"
int main(){
  shape tdim = {1, 2, 4, 4};
  shape tpdim = {1, 2, 4+2, 4+2};
  shape tpooldim = {1, 2, 2, 2};

  float* t = allocate<float>(tdim);
  float* tpad = allocate<float>(tpdim);
  float* tpool = allocate<float>(tpooldim);
  int m =1;

  for(int i =0; i < 1; i++){
    for(int j = 0; j < 2; j++){
      for(int k  =0; k < 4; k++){
        for(int l = 0; l < 4; l++){
          t[i *tdim.channels * tdim.height *tdim.width + j *tdim.height *tdim.width + k *tdim.width + l] = m++;
        }
      }
    }
  }

  print_matrix(t, tdim);

  //edge_padding(t, tpad, 1, tdim);
  maxPool(t, tpool, tdim, tpooldim, 2);
  print_matrix(tpool, tpooldim);
  delete [] t;
  delete [] tpad;
  delete [] tpool;
  return 0;

}
