#include "util_function.hpp"
void transpose_reshape(float *src, float *dst, const int N, const int M) {
#pragma omp parallel for
  for(int n = 0; n<N*M; n++) {
    int i = n/N;
    int j = n%N;
    dst[n] = 2*src[M*j + i]-1;
  }
}

void transpose(float *src, float *dst, const int N, const int M) {
#pragma omp parallel for
  for(int n = 0; n<N*M; n++) {
    int i = n/N;
    int j = n%N;
    dst[n] = src[M*j + i];
  }
}

__device__ unsigned int concatenate_gpu(float* array)
{
    unsigned int rvalue=0;
    unsigned int sign;
    
    for (int i = 0; i < 32; i++)
    {
        sign = (array[i]>=0);
        rvalue = rvalue | (sign<<i);
    }
    
    return rvalue;
}

__global__ void concatenate_rows_kernel(float *a, unsigned int *b, int size)
{ 
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if(i<size) b[i] = concatenate_gpu(&a[i*32]);
}

__global__ void concatenate_cols_kernel(float *a, unsigned int *b, int m, int n)
{   

    int j = blockIdx.x * blockDim.x + threadIdx.x;
    
    if(j<n){
        float * array = new float[32];
        for(int i=0; i<m; i+=32){
            for(int k=0; k<32;k++) array[k] = a[j + n*(i+k)];
            b[j+n*i/32]=concatenate_gpu(array); 
        } 
        delete[] array;
    }
}

void concatenate_cols_gpu(float *a, unsigned int *b, int m, int n){
  int num_threads = n;
  int num_blocks = ((num_threads-1)/CUDA_MAX_NUM_THREADS)+1;
  concatenate_cols_kernel<<<num_blocks, CUDA_MAX_NUM_THREADS>>>(a, b, m, n);
}



__global__ void gemm_xnor_gpu_optimized_kernel(unsigned int *A, unsigned int *B, float *C,
                                      int numARows, int numAColumns,
                                      int numBRows, int numBColumns,
                                      int numCRows, int numCColumns,
                     float * b,float* beta, float* gamma, float* mean, float* inv_std) {

  //Shared Memory and registers
  __shared__ unsigned int ds_B[TILE_WIDTH_K][TILE_WIDTH_N];
  unsigned int  ds_A[TILE_WIDTH_K];
  float  ds_P[TILE_WIDTH_N] ={0};
  
  //tile indices
  int bx = blockIdx.x;
  int by = blockIdx.y;
  int ty = threadIdx.y;
  int dx = TILE_WIDTH_N;
  int dy = TILE_WIDTH_M;
  
  int ds_B_row = ty/TILE_WIDTH_N;
  int ds_B_column = ty%TILE_WIDTH_N;
  
  int Column  = bx*dx+ds_B_column;
  int Row     = by*dy+ty;

  for (int p = 0; p < (numBRows-1)/TILE_WIDTH_K+1; ++p){
    //Each thread loads one B elements to shared memory
    if(p*TILE_WIDTH_K+ds_B_row < numBRows && Column < numBColumns){
      ds_B[ds_B_row][ds_B_column] = B[Column + (p*TILE_WIDTH_K+ds_B_row)*numBColumns];
    } else {
      ds_B[ds_B_row][ds_B_column] = 0.0;
    }
    __syncthreads();
    //Each thread loads TILE_WIDTH_K elements from A
    for(int i = 0; i < TILE_WIDTH_K; i++){
      if(p*TILE_WIDTH_K+i < numARows && Row < numAColumns){
        ds_A[i] = A[(p*TILE_WIDTH_K+i)*numAColumns + Row];
      }  
    }
    //compute
    for(int n = 0; n < TILE_WIDTH_N; ++n){
      for(int i = 0; i < TILE_WIDTH_K; ++i)
        ds_P[n] += __popc(ds_A[i] ^ ds_B[i][n]);
    }
    __syncthreads();
  }
  //write out
  for(int n = 0; n < TILE_WIDTH_N; ++n){
    if(Row < numCRows && bx*dx+n < numCColumns){
      int offset = Row;
      //C[Row*numCColumns + bx*dx+n] = -(2*(float)ds_P[n]-(float)32*numARows) + b[Row];
      C[Row * numCColumns + bx*dx+n] = ((-(2*(float)ds_P[n] -(float)32*numARows)+ b[offset] - mean[offset]) * gamma[offset] * inv_std[offset] + beta[offset]) >= 0.0? 1.0:-1.0;
    }
  }
}


void gemm_gpu_xnor(unsigned int *A, unsigned int *B, float *C,
                                      int numARows, int numAColumns,
                                      int numBRows, int numBColumns,
                                      int numCRows, int numCColumns,
                     float * b, float* beta, float* gamma, float* mean, float* inv_std, bool l4) {
  //launch GEMM with Optimization
  dim3 DimGrid((numCColumns-1)/TILE_WIDTH_N+1, (numCRows-1)/TILE_WIDTH_M+1);
  dim3 DimBlock(1, TILE_WIDTH_M); 
  if(!l4){
	 gemm_xnor_gpu_optimized_kernel<<<DimGrid, DimBlock>>>(A, B, C, numARows,numAColumns, numBRows, 
      numBColumns, numCRows, numCColumns, b, beta, gamma, mean, inv_std);
}
  else{
	 gemm_xnor_gpu_optimized_kernel_l4<<<DimGrid, DimBlock>>>(A, B, C, numARows,numAColumns, numBRows, 
      numBColumns, numCRows, numCColumns, b, beta, gamma, mean, inv_std);
}
}


/*
// Doesn't support batch
void gemm_gpu(float *A, float *B, float *C,
                                      int numARows, int numAColumns,
                                      int numBRows, int numBColumns,
                                      int numCRows, int numCColumns,
                     float * b, float* beta, float* gamma, float* mean, float* inv_std)
 {

  dim3 DimGrid((numCColumns-1)/TILE_WIDTH_N+1, (numCRows-1)/TILE_WIDTH_M+1);
  dim3 DimBlock(1, TILE_WIDTH_M); 
  matrixMultiply_kernel<<<DimGrid, DimBlock>>>(A, B, C, numARows,numAColumns, numBRows, 
      numBColumns, numCRows, numCColumns, b, beta, gamma, mean, inv_std);

}

__global__ void matrixMultiply_kernel(float *A, float *B, float *C,
                                      int numARows, int numAColumns,
                                      int numBRows, int numBColumns,
                                      int numCRows, int numCColumns,
                     float * b,float* beta, float* gamma, float* mean, float* inv_std) {
  //@@ Insert code to implement matrix multiplication here
  //@@ You have to perform register tiling for this 
  
  //Shared Memory and registers
  __shared__ float ds_B[TILE_WIDTH_K][TILE_WIDTH_N];
  float  ds_A[TILE_WIDTH_K];
  float  ds_P[TILE_WIDTH_N] ={0};
  
  //tile indices
  int bx = blockIdx.x;
  int by = blockIdx.y;
  int ty = threadIdx.y;
  int dx = TILE_WIDTH_N;
  int dy = TILE_WIDTH_M;
  
  int ds_B_row = ty/TILE_WIDTH_N;
  int ds_B_column = ty%TILE_WIDTH_N;
  
  int Column  = bx*dx+ds_B_column;
  int Row     = by*dy+ty;

  for (int p = 0; p < (numBRows-1)/TILE_WIDTH_K+1; ++p){
    //Each thread loads one B elements to shared memory
    if(p*TILE_WIDTH_K+ds_B_row < numBRows && Column < numBColumns){
      ds_B[ds_B_row][ds_B_column] = B[Column + (p*TILE_WIDTH_K+ds_B_row)*numBColumns];
    } else {
      ds_B[ds_B_row][ds_B_column] = 0.0;
    }
    __syncthreads();
    //Each thread loads TILE_WIDTH_K elements from A
    for(int i = 0; i < TILE_WIDTH_K; i++){
      if(p*TILE_WIDTH_K+i < numARows && Row < numAColumns){
        ds_A[i] = A[(p*TILE_WIDTH_K+i)*numAColumns + Row];
      }
    }
    //compute
    for(int n = 0; n < TILE_WIDTH_N; ++n){
      for(int i = 0; i < TILE_WIDTH_K; ++i)
        ds_P[n] += ds_A[i] * ds_B[i][n];
    }
    __syncthreads();
  }
  //write out
  for(int n = 0; n < TILE_WIDTH_N; ++n){
    if(Row < numCRows && bx*dx+n < numCColumns){
      //C[Row*numCColumns + bx*dx+n] = ds_P[n] + b[Row];
      int offset = Row;
      //C[Row * numCColumns + bx*dx+n] = ((ds_P[n] + b[offset] - mean[offset]) * gamma[offset] * inv_std[offset] + beta[offset]) >= 0.0? 1.0:-1.0;
      //float value = ((ds_P[n] + b[offset] - mean[offset]) * gamma[offset] * inv_std[offset] + beta[offset]);
      //C[Row * numCColumns + bx*dx+n] = 2*(((value+1)/2 <= 0.5)? 0:1.0)-1.0;
      C[Row * numCColumns + bx*dx+n] = ds_P[n] + b[offset];
    }
  }
}
*/

__global__ void gemm_xnor_gpu_optimized_kernel_l4(unsigned int *A, unsigned int *B, float *C,
                                      int numARows, int numAColumns,
                                      int numBRows, int numBColumns,
                                      int numCRows, int numCColumns,
                     float * b,float* beta, float* gamma, float* mean, float* inv_std) {

  //Shared Memory and registers
  __shared__ unsigned int ds_B[TILE_WIDTH_K][TILE_WIDTH_N];
  unsigned int  ds_A[TILE_WIDTH_K];
  float  ds_P[TILE_WIDTH_N] ={0};
  
  //tile indices
  int bx = blockIdx.x;
  int by = blockIdx.y;
  int ty = threadIdx.y;
  int dx = TILE_WIDTH_N;
  int dy = TILE_WIDTH_M;
  
  int ds_B_row = ty/TILE_WIDTH_N;
  int ds_B_column = ty%TILE_WIDTH_N;
  
  int Column  = bx*dx+ds_B_column;
  int Row     = by*dy+ty;

  for (int p = 0; p < (numBRows-1)/TILE_WIDTH_K+1; ++p){
    //Each thread loads one B elements to shared memory
    if(p*TILE_WIDTH_K+ds_B_row < numBRows && Column < numBColumns){
      ds_B[ds_B_row][ds_B_column] = B[Column + (p*TILE_WIDTH_K+ds_B_row)*numBColumns];
    } else {
      ds_B[ds_B_row][ds_B_column] = 0.0;
    }
    __syncthreads();
    //Each thread loads TILE_WIDTH_K elements from A
    for(int i = 0; i < TILE_WIDTH_K; i++){
      if(p*TILE_WIDTH_K+i < numARows && Row < numAColumns){
        ds_A[i] = A[(p*TILE_WIDTH_K+i)*numAColumns + Row];
      }  
    }
    //compute
    for(int n = 0; n < TILE_WIDTH_N; ++n){
      for(int i = 0; i < TILE_WIDTH_K; ++i)
        ds_P[n] += __popc(ds_A[i] ^ ds_B[i][n]);
    }
    __syncthreads();
  }
  //write out
  for(int n = 0; n < TILE_WIDTH_N; ++n){
    if(Row < numCRows && bx*dx+n < numCColumns){
      int offset = Row;
      //C[Row*numCColumns + bx*dx+n] = -(2*(float)ds_P[n]-(float)32*numARows) + b[Row];
      C[Row * numCColumns + bx*dx+n] = ((-(2*(float)ds_P[n] -(float)32*numARows)+ b[offset] - mean[offset]) * gamma[offset] * inv_std[offset] + beta[offset]) ;
    }
  }
}


//each thread is correponding to KxK of a column, N is the number of batch
void unroll_X_gpu(int N, int C, int W, int H, int K, float *X_unroll, float *X){
  int H_out = H-K+1;
  int W_out = W-K+1;
  int num_threads = N*C*H_out*W_out;
  int num_blocks = ((num_threads-1)/CUDA_MAX_NUM_THREADS)+1;
  //std::cout << "num_threads is " << num_threads << ", num_blocks is " << num_blocks << std::endl;
  unroll_X_kernel<<<num_blocks, CUDA_MAX_NUM_THREADS>>>(N, C, W, H, K, X_unroll, X);
}

__global__ void unroll_X_kernel(int N, int C, int W, int H, int K, float *X_unroll, float *X){
  int c, s, h, w, w_unroll, h_base, p, q, h_unroll;
  int t = blockIdx.x * blockDim.x+threadIdx.x;
  int H_out = H-K +1;
  int W_out = W-K+1;
  int W_unroll = H_out * W_out;
  int n = t/(W_unroll * C);
  if(t < N*C * W_unroll){
    c = (t/W_unroll)%C; //the channel for each thread
    s = t%W_unroll;
    h = s/W_out;
    w = s%W_out;
    w_unroll = h * W_out + w;
    h_base = n *C*K*K + c * K *K;
    for(p = 0; p < K; p++){
      for(q = 0; q<K; q++){
        h_unroll = h_base + p*K + q;
        X_unroll[h_unroll * W_unroll + w_unroll] = X[n * C*H*W + c * H * W + (h+p) * W + w+q];
      }
    }
  }
}

void conv2D_gpu(float *X, shape &xdims, float *W, float * b, shape &wdims, float *Y, shape &ydims, float* beta, float* gamma, float* mean, float* inv_std, int padding)
{
  float *device_X_unroll;
  check_success(cudaMalloc((void **)&device_X_unroll, sizeof(float) * xdims.num * xdims.channels * wdims.height * wdims.width * ydims.width * ydims.height));
  unroll_X_gpu(ydims.num, xdims.channels, xdims.width, xdims.height, wdims.height, device_X_unroll, X);
//  gemm_gpu(W, device_X_unroll, Y, /*int numARows*/wdims.length(), /*int numAColumns*/wdims.num, /*int numBRows*/wdims.length(), /*int numBColumns*/ydims.width*ydims.height,
//                                      /*int numCRows*/wdims.num, /*int numCColumns*/ydims.width*ydims.height,
//                                      b, beta, gamma, mean, inv_std);

  gemm_gpu_nl(ydims.num, xdims.channels, xdims.width, xdims.height, wdims.width, W, device_X_unroll, Y, 
  /*int numARows*/wdims.length(), /*int numAColumns*/wdims.num, /*int numBRows*/wdims.length(), /*int numBColumns*/ydims.width*ydims.height,
                                      /*int numCRows*/wdims.num, /*int numCColumns*/ydims.width*ydims.height, b, beta, gamma, mean, inv_std, padding);
}

void conv2D_gpu_pooling(float *X, shape &xdims, float *W, float * b, shape &wdims, float *Y, shape &ydims, float *Y_pool, shape &ypooldims, float* beta, float* gamma, float* mean, float* inv_std, int pool_size)
{
  float *device_X_unroll;
  check_success(cudaMalloc((void **)&device_X_unroll, sizeof(float) * xdims.num * xdims.channels * wdims.height * wdims.width * ydims.width * ydims.height));
  unroll_X_gpu(ydims.num, xdims.channels, xdims.width, xdims.height, wdims.height, device_X_unroll, X);
  check_success(cudaDeviceSynchronize());
//  gemm_gpu(W, device_X_unroll, Y, /*int numARows*/wdims.length(), /*int numAColumns*/wdims.num, /*int numBRows*/wdims.length(), /*int numBColumns*/ydims.width*ydims.height,
//                                      /*int numCRows*/wdims.num, /*int numCColumns*/ydims.width*ydims.height,
//                                      b/*, beta, gamma, mean, inv_std*/);

  gemm_gpu(ydims.num, xdims.channels, xdims.width, xdims.height, wdims.width, W, device_X_unroll, Y, 
  /*int numARows*/wdims.length(), /*int numAColumns*/wdims.num, /*int numBRows*/wdims.length(), /*int numBColumns*/ydims.width*ydims.height,
                                      /*int numCRows*/wdims.num, /*int numCColumns*/ydims.width*ydims.height, b);
  //max_pool_gpu(Y, pool_size, Y_pool, ypooldims.num, ypooldims.channels, ypooldims.height, ypooldims.width); 
  max_pool_nl_gpu(Y, pool_size, Y_pool, ypooldims.num, ypooldims.channels, ypooldims.height, ypooldims.width, beta, gamma, mean, inv_std); 
}

void gemm_gpu(int N, int Cin, int W, int H, int K, float *A, float *B, float *C, int numARows,
    int numAColumns, int numBRows, int numBColumns,
    int numCRows, int numCColumns,
    float * b) {
  dim3 DimGrid((numCColumns-1)/TILE_WIDTH_N+1, (numCRows-1)/TILE_WIDTH_M+1, N);
  dim3 DimBlock(1, TILE_WIDTH_M);
  matrixMultiply_kernel<<<DimGrid, DimBlock>>>(N, Cin, W, H, K, A, B, C, numARows, numAColumns, numBRows,
      numBColumns, numCRows, numCColumns, b);

}

__global__ void matrixMultiply_kernel(int N, int Cin, int W, int H, int K, float *A, float *B, float *C,
    int numARows, int numAColumns,
    int numBRows, int numBColumns,
    int numCRows, int numCColumns, float* b) {
  //@@ Insert code to implement matrix multiplication here
  //@@ You have to perform register tiling for this

  //Shared Memory and registers
  __shared__ float ds_B[TILE_WIDTH_K][TILE_WIDTH_N];
  float  ds_A[TILE_WIDTH_K];
  float  ds_P[TILE_WIDTH_N] ={0};

  //tile indices
  int bx = blockIdx.x;
  int by = blockIdx.y;
  int bz = blockIdx.z; //batch number

  int ty = threadIdx.y;
  int dx = TILE_WIDTH_N;
  int dy = TILE_WIDTH_M;

  int ds_B_row = ty/TILE_WIDTH_N;
  int ds_B_column = ty%TILE_WIDTH_N;

  int Column  = bx*dx+ds_B_column;
  int Row     = by*dy+ty;

  for (int p = 0; p < (numBRows-1)/TILE_WIDTH_K+1; ++p){
    //Each thread loads one B elements to shared memory
    if(p*TILE_WIDTH_K+ds_B_row < numBRows && Column < numBColumns){
      ds_B[ds_B_row][ds_B_column] = B[Column + ((bz * Cin*K*K) + (p*TILE_WIDTH_K+ds_B_row))*numBColumns];
    } else {
      ds_B[ds_B_row][ds_B_column] = 0.0;
    }
    __syncthreads();
    //Each thread loads TILE_WIDTH_K elements from A
    for(int i = 0; i < TILE_WIDTH_K; i++){
      if(p*TILE_WIDTH_K+i < numARows && Row < numAColumns){
        ds_A[i] = A[(p*TILE_WIDTH_K+i)*numAColumns + Row];
      }
    }
    //compute
    for(int n = 0; n < TILE_WIDTH_N; ++n){
      for(int i = 0; i < TILE_WIDTH_K; ++i)
        ds_P[n] += ds_A[i] * ds_B[i][n];
    }
    __syncthreads();
  }
  //write out
  for(int n = 0; n < TILE_WIDTH_N; ++n){
    if(Row < numCRows && bx*dx+n < numCColumns){
      C[(bz* numCRows+ Row)*numCColumns + bx*dx+n] = ds_P[n] + b[Row];
    }
  }
}

void gemm_gpu_nl(int N, int Cin, int W, int H, int K, float *A, float *B, float *C, int numARows,
    int numAColumns, int numBRows, int numBColumns,
    int numCRows, int numCColumns,
    float * b, float* beta, float* gamma, float* mean, float* inv_std, int padding) {
  dim3 DimGrid((numCColumns-1)/TILE_WIDTH_N+1, (numCRows-1)/TILE_WIDTH_M+1, N);
  dim3 DimBlock(1, TILE_WIDTH_M);
  matrixMultiply_kernel_nl<<<DimGrid, DimBlock>>>(N, Cin, W, H, K, A, B, C, numARows,numAColumns, numBRows,
      numBColumns, numCRows, numCColumns, b, beta, gamma, mean, inv_std);

}

__global__ void matrixMultiply_kernel_nl(int N, int Cin, int W, int H, int K, float *A, float *B, float *C,
    int numARows, int numAColumns,
    int numBRows, int numBColumns,
    int numCRows, int numCColumns,
    float * b, float* beta, float* gamma, float* mean, float* inv_std
    ) {
  //@@ Insert code to implement matrix multiplication here
  //@@ You have to perform register tiling for this

  //Shared Memory and registers
  __shared__ float ds_B[TILE_WIDTH_K][TILE_WIDTH_N];
  float  ds_A[TILE_WIDTH_K];
  float  ds_P[TILE_WIDTH_N] ={0};

  //tile indices
  int bx = blockIdx.x;
  int by = blockIdx.y;
  int bz = blockIdx.z; //batch number

  int ty = threadIdx.y;
  int dx = TILE_WIDTH_N;
  int dy = TILE_WIDTH_M;

  int ds_B_row = ty/TILE_WIDTH_N;
  int ds_B_column = ty%TILE_WIDTH_N;

  int Column  = bx*dx+ds_B_column;
  int Row     = by*dy+ty;

  for (int p = 0; p < (numBRows-1)/TILE_WIDTH_K+1; ++p){
    //Each thread loads one B elements to shared memory
    if(p*TILE_WIDTH_K+ds_B_row < numBRows && Column < numBColumns){
      ds_B[ds_B_row][ds_B_column] = B[Column + ((bz * Cin*K*K) + (p*TILE_WIDTH_K+ds_B_row))*numBColumns];
    } else {
      ds_B[ds_B_row][ds_B_column] = 0.0;
    }
    __syncthreads();
    //Each thread loads TILE_WIDTH_K elements from A
    for(int i = 0; i < TILE_WIDTH_K; i++){
      if(p*TILE_WIDTH_K+i < numARows && Row < numAColumns){
        ds_A[i] = A[(p*TILE_WIDTH_K+i)*numAColumns + Row];
      }
    }
    //compute
    for(int n = 0; n < TILE_WIDTH_N; ++n){
      for(int i = 0; i < TILE_WIDTH_K; ++i)
        ds_P[n] += ds_A[i] * ds_B[i][n];
    }
    __syncthreads();
  }
  //write out
  for(int n = 0; n < TILE_WIDTH_N; ++n){
    if(Row < numCRows && bx*dx+n < numCColumns){
      //C[(bz* numCRows+ Row)*numCColumns + bx*dx+n] = (ds_P[n] < 0) ? 0 : ds_P[n];
      //C[(bz* numCRows+ Row)*numCColumns + bx*dx+n] = ds_P[n];
      int offset = Row;
      float value = ((ds_P[n] + b[offset] - mean[offset]) * gamma[offset] * inv_std[offset] + beta[offset]);
      C[(bz* numCRows+ Row)*numCColumns + bx*dx+n] = 2*(((value+1)/2 <= 0.5)? 0:1.0)-1.0;
    }
  }
}


//each thread is in charge of one output
void max_pool_gpu( float *X,  int pool_size, float *Y,  int B, int C, int H, int W){
  int num_threads = B * C * H * W;
  int num_blocks = ((num_threads-1)/CUDA_MAX_NUM_THREADS)+1;
  //printf("num blocks %d\n", num_blocks);
  //printf("num threads %d\n", num_threads);
  //printf("num scales %f\n", scale);
  //printf("y_length %d\n", B*C*H*W);
  max_pool_kernel<<<num_blocks, CUDA_MAX_NUM_THREADS>>>(X, pool_size, Y, B, C, H, W);
}

__global__ void max_pool_kernel(float* X, int pool_size, float *Y,  int B, int C, int H, int W){
  int t = blockIdx.x * blockDim.x+threadIdx.x;
  int y_length = B * C* H *W;
  if(t < y_length){
    int M = H * W;
    int N = M * C;
    int n = t/N; //ydims.num
    int n_res = t%N;
    int c = n_res/M; //depth
    int c_res = n_res%M;
    int h = c_res/W; //height
    int w = c_res%W; //depth
    int xH = H*pool_size;
    int xW = W*pool_size;
    const auto yoffset = t;
    float acc = X[((((n * C) + c) * xH) + (pool_size * h )) * xW + (pool_size * w )];
    for( int p =0; p < pool_size; p++){
      for( int q =0; q < pool_size; q++){
        const auto xoffset = ((((n * C) + c) * xH) + (pool_size * h + p)) * xW + (pool_size * w + q);
        auto tmp = X[xoffset];
        acc = tmp  > acc ? tmp : acc;
      }
    }
    Y[yoffset] = acc;
  }
}

// nl stands for non-linear. This contains batch normalization and tanh
//each thread is in charge of one output
void max_pool_nl_gpu( float *X,  int pool_size, float *Y,  int B, int C, int H, int W, float* beta, float* gamma, float* mean, float* inv_std){
  int num_threads = B * C * H * W;
  int num_blocks = ((num_threads-1)/CUDA_MAX_NUM_THREADS)+1;
  //printf("num blocks %d\n", num_blocks);
  //printf("num threads %d\n", num_threads);
  //printf("num scales %f\n", scale);
  //printf("y_length %d\n", B*C*H*W);
  max_pool_nl_kernel<<<num_blocks, CUDA_MAX_NUM_THREADS>>>(X, pool_size, Y, B, C, H, W, beta, gamma, mean, inv_std);
}

__global__ void max_pool_nl_kernel(float* X, int pool_size, float *Y,  int B, int C, int H, int W, float* beta, float* gamma, float* mean, float* inv_std){
  int t = blockIdx.x * blockDim.x+threadIdx.x;
  int y_length = B * C* H *W;
  if(t < y_length){
    int M = H * W;
    int N = M * C;
    int n = t/N; //ydims.num
    int n_res = t%N;
    int c = n_res/M; //depth
    int c_res = n_res%M;
    int h = c_res/W; //height
    int w = c_res%W; //depth
    int xH = H*pool_size;
    int xW = W*pool_size;
    const auto yoffset = t;
    float acc = X[((((n * C) + c) * xH) + (pool_size * h )) * xW + (pool_size * w )];
    for( int p =0; p < pool_size; p++){
      for( int q =0; q < pool_size; q++){
        const auto xoffset = ((((n * C) + c) * xH) + (pool_size * h + p)) * xW + (pool_size * w + q);
        auto tmp = X[xoffset];
        acc = tmp  > acc ? tmp : acc;
      }
    }
    float value = ((acc - mean[c]) * gamma[c] * inv_std[c] + beta[c]);
    Y[yoffset] = 2*(((value+1)/2 <= 0.5)? 0:1.0)-1.0;
  }
}

void gemm_gpu_org(float *A, float *B, float *C,
                                      int numARows, int numAColumns,
                                      int numBRows, int numBColumns,
                                      int numCRows, int numCColumns,
                     float * b, float* beta, float* gamma, float* mean, float* inv_std)
 {

  dim3 DimGrid((numCColumns-1)/TILE_WIDTH_N+1, (numCRows-1)/TILE_WIDTH_M+1);
  dim3 DimBlock(1, TILE_WIDTH_M); 
  matrixMultiply_org_kernel<<<DimGrid, DimBlock>>>(A, B, C, numARows,numAColumns, numBRows, 
      numBColumns, numCRows, numCColumns, b, beta, gamma, mean, inv_std);

}

__global__ void matrixMultiply_org_kernel(float *A, float *B, float *C,
                                      int numARows, int numAColumns,
                                      int numBRows, int numBColumns,
                                      int numCRows, int numCColumns,
                     float * b,float* beta, float* gamma, float* mean, float* inv_std) {
  //@@ Insert code to implement matrix multiplication here
  //@@ You have to perform register tiling for this 
  
  //Shared Memory and registers
  __shared__ float ds_B[TILE_WIDTH_K][TILE_WIDTH_N];
  float  ds_A[TILE_WIDTH_K];
  float  ds_P[TILE_WIDTH_N] ={0};
  
  //tile indices
  int bx = blockIdx.x;
  int by = blockIdx.y;
  int ty = threadIdx.y;
  int dx = TILE_WIDTH_N;
  int dy = TILE_WIDTH_M;
  
  int ds_B_row = ty/TILE_WIDTH_N;
  int ds_B_column = ty%TILE_WIDTH_N;
  
  int Column  = bx*dx+ds_B_column;
  int Row     = by*dy+ty;

  for (int p = 0; p < (numBRows-1)/TILE_WIDTH_K+1; ++p){
    //Each thread loads one B elements to shared memory
    if(p*TILE_WIDTH_K+ds_B_row < numBRows && Column < numBColumns){
      ds_B[ds_B_row][ds_B_column] = B[Column + (p*TILE_WIDTH_K+ds_B_row)*numBColumns];
    } else {
      ds_B[ds_B_row][ds_B_column] = 0.0;
    }
    __syncthreads();
    //Each thread loads TILE_WIDTH_K elements from A
    for(int i = 0; i < TILE_WIDTH_K; i++){
      if(p*TILE_WIDTH_K+i < numARows && Row < numAColumns){
        ds_A[i] = A[(p*TILE_WIDTH_K+i)*numAColumns + Row];
      }  
    }
    //compute
    for(int n = 0; n < TILE_WIDTH_N; ++n){
      for(int i = 0; i < TILE_WIDTH_K; ++i)
        ds_P[n] += ds_A[i] * ds_B[i][n];
    }
    __syncthreads();
  }
  //write out
  for(int n = 0; n < TILE_WIDTH_N; ++n){
    if(Row < numCRows && bx*dx+n < numCColumns){ 
	int Row_pad = Row + padding;
	int Col_pad = Col + padding;
      //C[Row*numCColumns + bx*dx+n] = ds_P[n] + b[Row];
      	int offset = Row;
      //C[Row * numCColumns + bx*dx+n] = ((ds_P[n] + b[offset] - mean[offset]) * gamma[offset] * inv_std[offset] + beta[offset]) >= 0.0? 1.0:-1.0;
	C[Row_pad * (numCColumns+padding) + Col_pad] = ((ds_P[n] + b[offset] - mean[offset]) * gamma[offset] * inv_std[offset] + beta[offset]) >= 0.0? 1.0:-1.0;
    }
	  if(Row == 0){
		if(bx*dx + n == 0){
			C[Row * (numCColumns + padding) ] = 0;
		}
		else if(bx * dx + n == numCColumns - 1){
			C[Row * (numCColumns + padding)  + bx*dx+n + 2*padding] = 0;
		}
		C[Row * (numCColumns + padding) + col_pad] = 0;
	
	  }
	  if(Row == numCRows -1 ){
		if(bx*dx + n == 0){
			C[(Row+2*padding)* (numCColumns + padding) ] = 0;
		}
		else if(bx * dx + n == numCColumns - 1){
			C[(Row+2*padding) * (numCColumns + padding)  + bx*dx+n + 2*padding] = 0;
		}
  }
}
