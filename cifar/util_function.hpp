#ifndef _UTIL_FUNC_H_
#define _UTIL_FUNC_H_
#include "main_cpu.hpp"
#include "assert.h"
#define CUDA_MAX_NUM_THREADS 1024
#ifndef TILE_WIDTH_N
 #define TILE_WIDTH_N 8//16
#endif

#ifndef TILE_WIDTH_K
 #define TILE_WIDTH_K 4//4
#endif 

#ifndef TILE_WIDTH_M
  #define TILE_WIDTH_M TILE_WIDTH_N*TILE_WIDTH_K
#endif 

#include <iostream>

void transpose(float *src, float *dst, const int N, const int M);
void transpose_reshape(float *src, float *dst, const int N, const int M);
__device__ unsigned int concatenate_gpu(float* array);
__global__ void concatenate_rows_kernel(float *a, unsigned int *b, int size);
__global__ void concatenate_cols_kernel(float *a, unsigned int *b, int m, int n);
void concatenate_cols_gpu(float *a, unsigned int *b, int m, int n);
void gemm_gpu_xnor(unsigned int *A, unsigned int *B, float *C,
                                      int numARows, int numAColumns,
                                      int numBRows, int numBColumns,
                                      int numCRows, int numCColumns,
                     float * b, float* beta, float* gamma, float* mean, float* inv_std, bool l4);

__global__ void gemm_xnor_gpu_optimized_kernel(unsigned int *A, unsigned int *B, float *C,
                                      int numARows, int numAColumns,
                                      int numBRows, int numBColumns,
                                      int numCRows, int numCColumns,
                     float * b,float* beta, float* gamma, float* mean, float* inv_std) ;

/*void gemm_gpu(float *A, float *B, float *C,
                                      int numARows, int numAColumns,
                                      int numBRows, int numBColumns,
                                      int numCRows, int numCColumns,
                     float *b, float* beta, float* gamma, float* mean, float* inv_std);
*/

template <typename T>
static bool check_success(const T &err);

template <>
bool check_success<cudaError_t>(const cudaError_t &err) {
  const auto res = err == cudaSuccess;
  if (res == true) {
    return res;
  }
  std::cout << "Failed in CUDA. Error = " << cudaGetErrorString(err) << std::endl;
  assert(res);
  return res;
}

/*__global__ void matrixMultiply_kernel(float *A, float *B, float *C,
                                      int numARows, int numAColumns,
                                      int numBRows, int numBColumns,
                                      int numCRows, int numCColumns,
                     float* b,float* beta, float* gamma, float* mean, float* inv_std);
*/
__global__ void gemm_xnor_gpu_optimized_kernel_l4(unsigned int *A, unsigned int *B, float *C,
                                      int numARows, int numAColumns,
                                      int numBRows, int numBColumns,
                                      int numCRows, int numCColumns,
                     float * b,float* beta, float* gamma, float* mean, float* inv_std) ;

//void conv2D_gpu(const float *X, const shape &xdims, const float *W, const float * b, const shape &wdims, float *Y, const shape &ydims, float* beta, float* gamma, float* mean, float* inv_std);
void conv2D_gpu(float *X, shape &xdims, float *W, float * b, shape &wdims, float *Y, shape &ydims, float* beta, float* gamma, float* mean, float* inv_std);
void unroll_X_gpu(int N, int C, int W, int H, int K, float *X_unroll, float *X);
__global__ void unroll_X_kernel(int N, int C, int W, int H, int K, float *X_unroll, float *X);

void gemm_gpu_nl(int N, int Cin, int W, int H, int K, float *A, float *B, float *C, int numARows,
    int numAColumns, int numBRows, int numBColumns,
    int numCRows, int numCColumns,
    float * b, float* beta, float* gamma, float* mean, float* inv_std);

__global__ void matrixMultiply_kernel_nl(int N, int Cin, int W, int H, int K, float *A, float *B, float *C,
    int numARows, int numAColumns,
    int numBRows, int numBColumns,
    int numCRows, int numCColumns,
    float * b, float* beta, float* gamma, float* mean, float* inv_std
    );

void gemm_gpu(int N, int Cin, int W, int H, int K, float *A, float *B, float *C, int numARows,
    int numAColumns, int numBRows, int numBColumns,
    int numCRows, int numCColumns,
    float * b);

__global__ void matrixMultiply_kernel(int N, int Cin, int W, int H, int K, float *A, float *B, float *C,
    int numARows, int numAColumns,
    int numBRows, int numBColumns,
    int numCRows, int numCColumns, float* b);

/*void gemm_gpu(float *A, float *B, float *C,
                                      int numARows, int numAColumns,
                                      int numBRows, int numBColumns,
                                      int numCRows, int numCColumns,
                     float * b, float* beta, float* gamma, float* mean, float* inv_std);

__global__ void matrixMultiply_kernel(float *A, float *B, float *C,
                                      int numARows, int numAColumns,
                                      int numBRows, int numBColumns,
                                      int numCRows, int numCColumns,
                     float * b,float* beta, float* gamma, float* mean, float* inv_std);*/

void conv2D_gpu_pooling(float *X, shape &xdims, float *W, float * b, shape &wdims, float *Y, shape &ydims, float *Y_pool, shape &ypooldims, float* beta, float* gamma, float* mean, float* inv_std, int pool_size);

void max_pool_gpu( float *X,  int pool_size, float *Y,  int B, int C, int H, int W);

__global__ void max_pool_kernel(float* X, int pool_size, float *Y,  int B, int C, int H, int W);

void max_pool_nl_gpu( float *X,  int pool_size, float *Y,  int B, int C, int H, int W, float* beta, float* gamma, float* mean, float* inv_std);
__global__ void max_pool_nl_kernel(float* X, int pool_size, float *Y,  int B, int C, int H, int W, float* beta, float* gamma, float* mean, float* inv_std);

void gemm_gpu_org(float *A, float *B, float *C,
                                      int numARows, int numAColumns,
                                      int numBRows, int numBColumns,
                                      int numCRows, int numCColumns,
                     float * b, float* beta, float* gamma, float* mean, float* inv_std);
__global__ void matrixMultiply_org_kernel(float *A, float *B, float *C,
                                      int numARows, int numAColumns,
                                      int numBRows, int numBColumns,
                                      int numCRows, int numCColumns,
                     float * b,float* beta, float* gamma, float* mean, float* inv_std);

#endif
