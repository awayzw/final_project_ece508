#ifndef _LAYER_FUNC_H_
#define _LAYER_FUNC_H_
#include "main_cpu.hpp"
void edge_padding(float *in, float *out, int padSize, shape &dims);
void batch_normalization(float* beta, float* gamma, float* mean, float* inv_std, float *x, shape &dims);
void conv2D(const float *X, const shape &xdims, const float *W, const float * b, const shape &wdims, float *Y, const shape &ydims);
void tanh(float *x, int size);
void maxPool(const float *X, float* Y,  const shape &xdims, const shape &ydims, const int pool_size) ;
void xnor_gemm_cpu(float* A, float* B, float* C, int A_row, int A_col, int B_col, float* b);
void gemm_cpu(float* A, float* B, float *C, int A_row, int A_col, int B_col, float *b);
void conv2D_org(const float *X, const shape &xdims, const float *W, const float * b, const shape &wdims, float *Y, const shape &ydims);
#endif
