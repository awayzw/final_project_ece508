#include "main_cpu.hpp"
#include "math.h"
#include <iostream>
#include <cuda_runtime.h>
#include "cublas_v2.h"

#define TILE_WIDTH_N 16
#define TILE_WIDTH_K 4
#define TILE_WIDTH_M TILE_WIDTH_N*TILE_WIDTH_K

void gemm_cpu(float* A, float* B, float *C, int A_row, int A_col, int B_col, float *b){
  for(int i = 0; i < A_row; i++){
    for(int j =0; j < B_col; j++){
      float acc = b[j];
      for(int k =0; k < A_col; k++){
      	acc += A[i *A_col + k] * B[ k *B_col + j];
      }
      C[i * B_col + j] = acc;
    }
  }
}

__global__ void gemm_gpu_kernel(float* A, float* B, float *C, int A_row, int A_col, int B_col, float *b){
  if(blockIdx.x == 0){
    for(int i = 0; i < A_row; i++){
    for(int j =0; j < B_col; j++){
      float acc = b[j];
      for(int k =0; k < A_col; k++){
      	acc += A[i *A_col + k] * B[ k *B_col + j];
      }
      C[i * B_col + j] = acc;
    }
  }
  }
}

__global__ void gemm_gpu_optimized_kernel(float* A, float* B, float *C, int A_row, int A_col, int B_col, float *b){
  A += blockIdx.y * TILE_WIDTH_M + threadIdx.y; //cal offset for A, B, C
  B += blockIdx.x * TILE_WIDTH_N; 
  C += (blockIdx.y * TILE_WIDTH_M + threadIdx.y) * numCColumns + blockIdx.x * TILE_WIDTH_N;
  
  __shared__ float Bds[TILE_WIDTH_K][TILE_WIDTH_N];
  float A_reg[TILE_WIDTH_K] = {0};
  float result[TILE_WIDTH_N] = {0};

  int Bds_idx_y = threadIdx.y / TILE_WIDTH_N;
  int Bds_idx_x = threadIdx.y % TILE_WIDTH_N;
  
  for (int m = 0; m < ((numAColumns+TILE_WIDTH_K-1) / TILE_WIDTH_K); m++) // one step calcuate K col
  {
    for (int i = 0; i < TILE_WIDTH_K; i++)
      A_reg[i] = ((m*TILE_WIDTH_K + i) < numAColumns) ? A[(m*TILE_WIDTH_K + i) * numARows] : 0; //load A to Reg, 因为A col-major, 所以A的index:(m*TILE_WIDTH_K + i) * numARows

    if ( ((blockIdx.x * TILE_WIDTH_N + Bds_idx_x ) < numBColumns) && ((m * TILE_WIDTH_K + Bds_idx_y) < numBRows) )
      Bds[Bds_idx_y][Bds_idx_x] = B[(m * TILE_WIDTH_K + Bds_idx_y)*numBColumns + Bds_idx_x];  //load B to shared MEM
    else
      Bds[Bds_idx_y][Bds_idx_x] = 0;
    __syncthreads();
    for (int i = 0; i < TILE_WIDTH_K; i++)
    {
      for (int j = 0; j < TILE_WIDTH_N; j++)
      {
        result[j] += A_reg[i] * Bds[i][j];
      }
    }
    __syncthreads();
  } //complete K steps
  for (int i = 0; i < TILE_WIDTH_N; i++)
  {
    if (((blockIdx.y * TILE_WIDTH_M + threadIdx.y) < numCRows ) &&
         ((blockIdx.x * TILE_WIDTH_N + i) < numCColumns) )
      C[i] = result[i];
  }
  
}

static void gemm_gpu(float* A, float* B, float *C, int A_row, int A_col, int B_col, float *b){
  //launch naive GEMM   
  gemm_gpu_kernel<<<1,1>>>(A, B, C, A_row, A_col, B_col, b);
  //launch GEMM with Optimization
  int block_size_x = 1;
  int block_size_y = TILE_WIDTH_M;
  int grid_size_x = (OUT1_SIZE + TILE_WIDTH_N - 1) / TILE_WIDTH_N;
  int grid_size_y = (BATCH_SIZE + TILE_WIDTH_M - 1) / TILE_WIDTH_M;
  dim3 dimBlock(block_size_x, block_size_y);
  dim3 dimGrid(grid_size_x, grid_size_y);
  gemm_gpu_optimized_kernel<<<dimGrid, dimBlock>>>(A, B, C, A_row, A_col, B_col, b);
  
}

//this should be merged into gemm_gpu
void reshape(float *x, int size){
  for(int i = 0; i < size ; i++){
    x[i] = 2 * x[i] -1;
  }
}


//following the definition of http://lasagne.readthedocs.io/en/latest/modules/layers/normalization.html
//the real code is https://github.com/Lasagne/Lasagne/blob/master/lasagne/layers/normalization.py#L120-L320, line 320
void batch_normalization(float* beta, float* gamma, float* mean, float* inv_std, float *x, int batch_num, int onebatchsize){
  for( int j = 0; j < batch_num; j++){
    for (int i = 0; i < onebatchsize; i++){ 
      x[j * onebatchsize + i] = (x[j * onebatchsize + i] - mean[i] ) * gamma[i] * inv_std[i] + beta[i];
    }
  }
}

//the function binary_ops.SignTheano(x), binarize the output x  = (x>= 0) ? 1.0 : -1.0;
void SignTheano(float* x, int size){
	for(int i = 0; i < size; i++){
		x[i] = (x[i] >=0.0) ? 1.0 : -1.0;

	}
}

int main(int argc, char **argv){
  //allocate the memory for input and output weights
  float *x = allocate<float>(xdims);
  float *w1 = allocate<float>(w1dims);
  float *b1 = allocate<float>(b1dims);
  float *beta1 = allocate<float>(b1dims); 
  float *gamma1 = allocate<float>(b1dims);
  float *mean1 = allocate<float>(b1dims);
  float *inv_std1 = allocate<float>(b1dims);

  float *device_x = allocate<float>(xdims);
  float *device_w1 = allocate<float>(w1dims);
  float *device_b1 = allocate<float>(b1dims);
  float *device_out1 = allocate<float>(out1dims);


  readdata("x.bin", xdims, x);
  readdata("w1.bin", w1dims, w1);
  readdata("b1.bin", b1dims, b1);
  readdata("beta1.bin", b1dims, beta1);
  readdata("gamma1.bin", b1dims, gamma1);
  readdata("inv_std1.bin", b1dims, inv_std1);
  readdata("mean1.bin", b1dims, mean1);

  //dense layer1, output is batchsize * 4096
  float *out1 = allocate<float>(out1dims);
  float *out1_GR = allocate<float>(out1dims);

  //reshpe to the range of [-1, 1]
  reshape(x, xdims.batched_length());

  std::chrono::time_point<std::chrono::high_resolution_clock>start, end;
  double elapsed1 = 0;
  //time  
  start = now();
  
  //Prepare for kernel launch
  cudaMalloc((void **)&device_x, sizeof(float) * xdims.batched_length());
  cudaMalloc((void **)&device_w1, sizeof(float) * w1dims.batched_length());
  cudaMalloc((void **)&device_b1, sizeof(float) * b1dims.batched_length());
  cudaMalloc((void **)&device_out1, sizeof(float) * out1dims.batched_length());
  cudaMemcpy(device_x, x, sizeof(float) * xdims.batched_length(), cudaMemcpyHostToDevice);
  cudaMemcpy(device_w1, w1, sizeof(float) * w1dims.batched_length(), cudaMemcpyHostToDevice);
  cudaMemcpy(device_b1, b1, sizeof(float) * b1dims.batched_length(), cudaMemcpyHostToDevice);
  //launch kernel
  gemm_gpu(device_x, device_w1, device_out1, BATCH_SIZE, IMAGE_SIZE, OUT1_SIZE, device_b1);
  cudaDeviceSynchronize(); 
  cudaMemcpy(out1, device_out1, sizeof(float) * out1dims.batched_length(), cudaMemcpyDeviceToHost); 
  
  //***use cuBlas***// 
  //C[BATCH_SIZE][OUT1_SIZE]=A[BATCH_SIZE][IMAGE_SIZE]*B[IMAGE_SIZE][OUT1_SIZE]
/*  cublasHandle_t handle;
  cublasCreate(&handle);
  const float alp = 1;
  const float bet = 0;
  const float *alpha = &alp;
  const float *beta = &bet;
  cublasSgemm(handle, CUBLAS_OP_T, CUBLAS_OP_T, (int)BATCH_SIZE, (int)OUT1_SIZE, (int)IMAGE_SIZE, alpha, device_x, IMAGE_SIZE, device_w1, OUT1_SIZE, beta, device_out1, (int)BATCH_SIZE);
  cudaMemcpy(out1, device_out1, sizeof(float) * out1dims.batched_length(), cudaMemcpyDeviceToHost); 
  cublasDestroy(handle);*/
  //***end cuBlas***//
  
  end = now();
  
  
  //call GEMM
  
  //float* A, float* B, float *C, int A_row(BATCH_SIZE), int A_col(IMAGE_SIZE), int B_col(OUT1_SIZE), float *b
  gemm_cpu(x, w1, out1_GR, BATCH_SIZE,  IMAGE_SIZE, OUT1_SIZE, b1);
  
  elapsed1 += std::chrono::duration<double, std::milli>(end - start).count(); 
  std::cout << "elapsed = " << elapsed1 << " milliseconds\n";

  //print_acc(x, xdims.batched_length());
  //print_acc(w1, w1dims.batched_length());  
  print_acc(out1, out1dims.batched_length());
    
  printf("GR: "); print_acc(out1_GR, out1dims.batched_length());
  //batchNormrLayer
  batch_normalization(beta1, gamma1, mean1, inv_std1, out1, out1dims.num, out1dims.length);

  SignTheano(out1, out1dims.batched_length());
  print_acc(out1, out1dims.batched_length());


  cudaFree(device_x);
  cudaFree(device_w1);
  cudaFree(device_out1);
  delete [] x;
  delete [] w1;
  delete [] b1;
  delete [] beta1;
  delete [] out1;
  delete [] out1_GR;
  
}
