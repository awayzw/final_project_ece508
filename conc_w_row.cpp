#include "main_cpu.hpp"

template<typename T>
static void writedata(std::string fileName, shape dims, T *input){
  FILE *fout;
  fout=fopen(fileName.c_str(), "wb");
  for(int i = 0; i < dims.num; i++){
    fwrite(&input[i * dims.length], sizeof(T), dims.length, fout);
  }
  fclose(fout);
}
unsigned int concatenate(float* array)
{
    unsigned int rvalue=0;
    unsigned int sign;
    
    for (int i = 0; i < 32; i++)
    {
        sign = (array[i]>=0);
        rvalue = rvalue | (sign<<i);
    }
    
    return rvalue;
}


void concatenate_rows_cpu_2d(float *a, unsigned int *b, int m, int n)
{
  for(int j = 0; j < m; j ++)
    for (int i = 0; i < n/32; i++)
        b[j*n/32 + i] = concatenate(&a[j*n + i*32]);
}

int main(){

  shape wc2rdims= {OUT1_SIZE, OUT1_SIZE/32};
  unsigned int* wc2r = allocate<unsigned int>(wc2rdims);
  float* w2 = allocate<float>(w2dims);

  shape wc3rdims= {OUT1_SIZE, OUT1_SIZE/32};
  unsigned int* wc3r = allocate<unsigned int>(wc3rdims);
  float* w3 = allocate<float>(w3dims);

  shape wc4rdims= {OUT1_SIZE, OUT4_SIZE/32};
  unsigned int* wc4r = allocate<unsigned int>(wc4rdims);
  float* w4 = allocate<float>(w4dims);

  readdata("params/w2.bin", w2dims, w2);
  concatenate_rows_cpu_2d(w2, wc2r, w2dims.num, w2dims.length);
  writedata("params/wc2r.bin", wc2rdims, wc2r);


  readdata("params/w3.bin", w3dims, w3);
  concatenate_rows_cpu_2d(w3, wc3r, w3dims.num, w3dims.length);
  writedata("params/wc3r.bin", wc3rdims, wc3r);

  readdata("params/w4.bin", w4dims, w4);
  concatenate_rows_cpu_2d(w4, wc4r, w4dims.num, w4dims.length);
  writedata("params/wc4r.bin", wc4rdims, wc4r);

  delete [] wc2r;
  delete [] w2;
  delete [] wc3r;
  delete [] w3;
  delete [] wc4r;
  delete [] w4;

  return 0;
}
