#ifndef _UTIL_FUNC_H_
#define _UTIL_FUNC_H_
#include "main_cpu.hpp"
#include "assert.h"
#define CUDA_MAX_NUM_THREADS 1024
#ifndef TILE_WIDTH_N
 #define TILE_WIDTH_N 8//16
#endif

#ifndef TILE_WIDTH_K
 #define TILE_WIDTH_K 4//4
#endif 

#ifndef TILE_WIDTH_M
  #define TILE_WIDTH_M TILE_WIDTH_N*TILE_WIDTH_K
#endif 

void transpose(float *src, float *dst, const int N, const int M);
void transpose_reshape(float *src, float *dst, const int N, const int M);
__device__ unsigned int concatenate_gpu(float* array);
__global__ void concatenate_rows_kernel(float *a, unsigned int *b, int size);
__global__ void concatenate_cols_kernel(float *a, unsigned int *b, int m, int n);
void concatenate_cols_gpu(float *a, unsigned int *b, int m, int n);
void gemm_gpu_xnor(unsigned int *A, unsigned int *B, float *C,
                                      int numARows, int numAColumns,
                                      int numBRows, int numBColumns,
                                      int numCRows, int numCColumns,
                     float * b, float* beta, float* gamma, float* mean, float* inv_std, bool l4);

__global__ void gemm_xnor_gpu_optimized_kernel(unsigned int *A, unsigned int *B, float *C,
                                      int numARows, int numAColumns,
                                      int numBRows, int numBColumns,
                                      int numCRows, int numCColumns,
                     float * b,float* beta, float* gamma, float* mean, float* inv_std) ;

void gemm_gpu(float *A, float *B, float *C,
                                      int numARows, int numAColumns,
                                      int numBRows, int numBColumns,
                                      int numCRows, int numCColumns,
                     float *b, float* beta, float* gamma, float* mean, float* inv_std);
template <typename T>
static bool check_success(const T &err);

template <>
bool check_success<cudaError_t>(const cudaError_t &err) {
  const auto res = err == cudaSuccess;
  if (res == true) {
    return res;
  }
  std::cout << "Failed in CUDA. Error = " << cudaGetErrorString(err) << std::endl;
  assert(res);
  return res;
}

__global__ void matrixMultiply_kernel(float *A, float *B, float *C,
                                      int numARows, int numAColumns,
                                      int numBRows, int numBColumns,
                                      int numCRows, int numCColumns,
                     float* b,float* beta, float* gamma, float* mean, float* inv_std);
__global__ void gemm_xnor_gpu_optimized_kernel_l4(unsigned int *A, unsigned int *B, float *C,
                                      int numARows, int numAColumns,
                                      int numBRows, int numBColumns,
                                      int numCRows, int numCColumns,
                     float * b,float* beta, float* gamma, float* mean, float* inv_std) ;
#endif
