#include "main_cpu_cp.hpp"
#include "util_function_cp.hpp"
#include <thread>
#include <algorithm>


int main(int argc, char **argv){

	float alpha = 1.0; 

	size_t num_batch = 1000;

	if (argc >= 3) {
		num_batch = atoi(argv[1]);
		alpha = atof(argv[2]);
	}
	unsigned int gpu_batch_size = (unsigned int)num_batch * alpha;
	unsigned int cpu_batch_size = num_batch - gpu_batch_size;
	printf("cpu_batch_size = %d\n", cpu_batch_size); 
	printf("gpu_batch_size = %d\n", gpu_batch_size); 

	const int GPU_PROXY = 0;
	const int CPU_PROXY = 1;

	static shape xdims = {num_batch, IMAGE_SIZE};
	static shape xdims_cpu = {cpu_batch_size, IMAGE_SIZE};
	static shape xdims_gpu = {gpu_batch_size, IMAGE_SIZE};
	static shape w1dims = {IMAGE_SIZE, OUT1_SIZE};
	static shape b1dims = {1, OUT1_SIZE};
	static shape out1dims = {num_batch, OUT1_SIZE};
	static shape out1dims_cpu = {cpu_batch_size, OUT1_SIZE};
	static shape out1dims_gpu = {gpu_batch_size, OUT1_SIZE};

	static shape w2dims = {OUT1_SIZE, OUT1_SIZE};
	static shape wc2dims = {OUT1_SIZE/32, OUT1_SIZE};
	static shape b2dims = {1, OUT1_SIZE};
	static shape out2dims = {num_batch, OUT1_SIZE};
	static shape out2dims_cpu = {cpu_batch_size, OUT1_SIZE};
	static shape out2dims_gpu = {gpu_batch_size, OUT1_SIZE};

	static shape w3dims = {OUT1_SIZE, OUT1_SIZE};
	static shape wc3dims = {OUT1_SIZE/32, OUT1_SIZE};
	static shape b3dims = {1, OUT1_SIZE};
	static shape out3dims = {num_batch, OUT1_SIZE};
	static shape out3dims_cpu = {cpu_batch_size, OUT1_SIZE};
	static shape out3dims_gpu = {gpu_batch_size, OUT1_SIZE};

	static shape w4dims = {OUT1_SIZE, OUT4_SIZE};
	static shape wc4dims = {OUT1_SIZE/32, OUT4_SIZE};
	static shape b4dims = {1, OUT4_SIZE};
	static shape out4dims = {num_batch, OUT4_SIZE};
	static shape out4dims_cpu = {cpu_batch_size, OUT4_SIZE};
	static shape out4dims_gpu = {gpu_batch_size, OUT4_SIZE};

	static shape wc2rdims = {OUT1_SIZE, OUT1_SIZE/32};
	static shape wc4rdims = {OUT1_SIZE, OUT4_SIZE/32};

	float *x_original = allocate<float>(xdims);
	float *w1 = allocate<float>(w1dims);
	float *b1 = allocate<float>(b1dims);
	float *beta1 = allocate<float>(b1dims); 
	float *gamma1 = allocate<float>(b1dims);
	float *mean1 = allocate<float>(b1dims);
	float *inv_std1 = allocate<float>(b1dims);


	readdata("params/x1000.bin", xdims, x_original);
	readdata("params/w1.bin", w1dims, w1);
	readdata("params/b1.bin", b1dims, b1);
	readdata("params/beta1.bin", b1dims, beta1);
	readdata("params/gamma1.bin", b1dims, gamma1);
	readdata("params/inv_std1.bin", b1dims, inv_std1);
	readdata("params/mean1.bin", b1dims, mean1);


	// layer 2
	unsigned int *wc2 = allocate<unsigned int>(wc2dims);
	float *b2 = allocate<float>(b2dims);
	float *beta2 = allocate<float>(b2dims); 
	float *gamma2 = allocate<float>(b2dims);
	float *mean2 = allocate<float>(b2dims);
	float *inv_std2 = allocate<float>(b2dims);

	readdata("params/wc2.bin", wc2dims, wc2);
	readdata("params/b2.bin", b2dims, b2);
	readdata("params/beta2.bin", b2dims, beta2);
	readdata("params/gamma2.bin", b2dims, gamma2);
	readdata("params/inv_std2.bin", b2dims, inv_std2);
	readdata("params/mean2.bin", b2dims, mean2);


	//layer 3, reuse b, beta gamma, inv_std and mean from layer2
	unsigned int *wc3 = allocate<unsigned int>(wc3dims);
	float *b3 = allocate<float>(b3dims);
	float *beta3 = allocate<float>(b3dims); 
	float *gamma3 = allocate<float>(b3dims);
	float *mean3 = allocate<float>(b3dims);
	float *inv_std3 = allocate<float>(b3dims);

	readdata("params/wc3.bin", wc3dims, wc3);
	readdata("params/b3.bin", b3dims, b3);
	readdata("params/beta3.bin", b3dims, beta3);
	readdata("params/gamma3.bin", b3dims, gamma3);
	readdata("params/inv_std3.bin", b3dims, inv_std3);
	readdata("params/mean3.bin", b3dims, mean3);

	unsigned int *wc4 = allocate<unsigned int>(wc4dims);
	float *b4 = allocate<float>(b4dims);
	float *beta4 = allocate<float>(b4dims); 
	float *gamma4 = allocate<float>(b4dims);
	float *mean4 = allocate<float>(b4dims);
	float *inv_std4 = allocate<float>(b4dims);

	//layer 4, reuse memory space
	readdata("params/wc4.bin", wc4dims, wc4);
	readdata("params/b4.bin", b4dims, b4);
	readdata("params/beta4.bin", b4dims, beta4);
	readdata("params/gamma4.bin", b4dims, gamma4);
	readdata("params/inv_std4.bin", b4dims, inv_std4);
	readdata("params/mean4.bin", b4dims, mean4);

	float *out1 = allocate<float>(out1dims); //dense layer1, output is batchsize * 4096
	float *out2 = allocate<float>(out2dims);
	float *out3 = allocate<float>(out3dims);
	float *out4 = allocate<float>(out4dims);

	std::chrono::time_point<std::chrono::high_resolution_clock>start, end;
	double elapsed1 = 0;
	start = now();

	std::vector<std::thread> proxy_threads;
	for (int proxy_type = 0; proxy_type < 2; proxy_type++) {
		proxy_threads.push_back(std::thread([&, proxy_type]() {
			if (proxy_type == GPU_PROXY) {

				float *x = x_original; 

				float *device_x;
				float *device_w1;
				float *device_b1;
				float *device_out1;
				float *device_beta1; 
				float *device_gamma1;
				float *device_mean1;
				float *device_inv_std1;

				float* xT = x;
				xT = allocate<float>(xdims_gpu);
				transpose_reshape(x, xT, xdims_gpu.num, xdims_gpu.length);	

				check_success(cudaMalloc((void **)&device_x, sizeof(float) * xdims_gpu.batched_length()));
				check_success(cudaMalloc((void **)&device_w1, sizeof(float) * w1dims.batched_length()));
				check_success(cudaMalloc((void **)&device_b1, sizeof(float) * b1dims.batched_length()));
				check_success(cudaMalloc((void **)&device_out1, sizeof(float) * out1dims_gpu.batched_length())); 
				check_success(cudaMalloc((void **)&device_beta1, sizeof(float) * b1dims.batched_length()));
				check_success(cudaMalloc((void **)&device_gamma1, sizeof(float) * b1dims.batched_length()));
				check_success(cudaMalloc((void **)&device_mean1, sizeof(float) * b1dims.batched_length()));
				check_success(cudaMalloc((void **)&device_inv_std1, sizeof(float) * b1dims.batched_length()));


				check_success(cudaMemcpy(device_x, xT, sizeof(float) * xdims_gpu.batched_length(), cudaMemcpyHostToDevice));
				check_success(cudaMemcpy(device_w1, w1, sizeof(float) * w1dims.batched_length(), cudaMemcpyHostToDevice));
				check_success(cudaMemcpy(device_b1, b1, sizeof(float) * b1dims.batched_length(), cudaMemcpyHostToDevice));
				check_success(cudaMemcpy(device_beta1, beta1, sizeof(float) * b1dims.batched_length(), cudaMemcpyHostToDevice));
				check_success(cudaMemcpy(device_gamma1, gamma1, sizeof(float) * b1dims.batched_length(), cudaMemcpyHostToDevice));
				check_success(cudaMemcpy(device_mean1, mean1, sizeof(float) * b1dims.batched_length(), cudaMemcpyHostToDevice));
				check_success(cudaMemcpy(device_inv_std1, inv_std1, sizeof(float) * b1dims.batched_length(), cudaMemcpyHostToDevice));


				//launch kernel
				gemm_gpu(device_w1, device_x, device_out1,
				w1dims.num, w1dims.length, 
				xdims_gpu.length, xdims_gpu.num, 
				w1dims.length, xdims_gpu.num,
				device_b1, device_beta1, device_gamma1, device_mean1, device_inv_std1);
				check_success(cudaDeviceSynchronize());

				check_success(cudaFree(device_x));
				check_success(cudaFree(device_w1));
				check_success(cudaFree(device_b1));
				check_success(cudaFree(device_beta1));
				check_success(cudaFree(device_gamma1));
				check_success(cudaFree(device_mean1));
				check_success(cudaFree(device_inv_std1));

				unsigned int *device_wc2;
				float *device_b2;
				float *device_beta2;
				float *device_gamma2;
				float *device_mean2;
				float *device_inv_std2;

				//FIXME: THIS could be in place
				unsigned int *device_out1_conc;
				check_success(cudaMalloc((void **)&device_wc2, sizeof(unsigned int) * wc2dims.batched_length()));
				check_success(cudaMalloc((void **)&device_b2, sizeof(float) * b2dims.batched_length()));
				check_success(cudaMalloc((void **)&device_beta2, sizeof(float) * b2dims.batched_length()));
				check_success(cudaMalloc((void **)&device_gamma2, sizeof(float) * b2dims.batched_length()));
				check_success(cudaMalloc((void **)&device_mean2, sizeof(float) * b2dims.batched_length()));
				check_success(cudaMalloc((void **)&device_inv_std2, sizeof(float) * b2dims.batched_length())); 
				check_success(cudaMalloc((void **)&device_out1_conc, sizeof(unsigned int) *out1dims_gpu.batched_length()/32));

				check_success(cudaMemcpy(device_wc2, wc2, sizeof(unsigned int) * wc2dims.batched_length(), cudaMemcpyHostToDevice));
				check_success(cudaMemcpy(device_b2, b2, sizeof(float) * b2dims.batched_length(), cudaMemcpyHostToDevice));
				check_success(cudaMemcpy(device_beta2, beta2, sizeof(float) * b2dims.batched_length(), cudaMemcpyHostToDevice));
				check_success(cudaMemcpy(device_gamma2, gamma2, sizeof(float) * b2dims.batched_length(), cudaMemcpyHostToDevice));
				check_success(cudaMemcpy(device_mean2, mean2, sizeof(float) * b2dims.batched_length(), cudaMemcpyHostToDevice));
				check_success(cudaMemcpy(device_inv_std2, inv_std2, sizeof(float) * b2dims.batched_length(), cudaMemcpyHostToDevice));


				concatenate_cols_gpu(device_out1, device_out1_conc, out1dims_gpu.length, gpu_batch_size);
				check_success(cudaDeviceSynchronize());

				//reuse the space of device_out1 for device_out2
				gemm_gpu_xnor(device_wc2, device_out1_conc, device_out1,
				wc2dims.num, wc2dims.length,
				out1dims_gpu.length/32, out1dims_gpu.num,
				wc2dims.length, out1dims_gpu.num, 
				device_b2, device_beta2, device_gamma2, device_mean2, device_inv_std2, false);
				check_success(cudaDeviceSynchronize());



				check_success(cudaMemcpy(device_wc2, wc2, sizeof(unsigned int) * wc2dims.batched_length(), cudaMemcpyHostToDevice));
				check_success(cudaMemcpy(device_b2, b2, sizeof(float) * b2dims.batched_length(), cudaMemcpyHostToDevice));
				check_success(cudaMemcpy(device_beta2, beta2, sizeof(float) * b2dims.batched_length(), cudaMemcpyHostToDevice));
				check_success(cudaMemcpy(device_gamma2, gamma2, sizeof(float) * b2dims.batched_length(), cudaMemcpyHostToDevice));
				check_success(cudaMemcpy(device_mean2, mean2, sizeof(float) * b2dims.batched_length(), cudaMemcpyHostToDevice));
				check_success(cudaMemcpy(device_inv_std2, inv_std2, sizeof(float) * b2dims.batched_length(), cudaMemcpyHostToDevice));

				concatenate_cols_gpu(device_out1, device_out1_conc, out1dims_gpu.length, gpu_batch_size);
				check_success(cudaDeviceSynchronize());

				//reuse the space of device_out1 for device_out2
				gemm_gpu_xnor(device_wc2, device_out1_conc, device_out1,
				wc2dims.num, wc2dims.length,
				out1dims_gpu.length/32, out1dims_gpu.num,
				wc2dims.length, out1dims_gpu.num, 
				device_b2, device_beta2, device_gamma2, device_mean2, device_inv_std2, false);
				check_success(cudaDeviceSynchronize());


				check_success(cudaMemcpy(device_wc2, wc2, sizeof(unsigned int) * wc4dims.batched_length(), cudaMemcpyHostToDevice));
				check_success(cudaMemcpy(device_b2, b2, sizeof(float) * b4dims.batched_length(), cudaMemcpyHostToDevice));
				check_success(cudaMemcpy(device_beta2, beta2, sizeof(float) * b4dims.batched_length(), cudaMemcpyHostToDevice));
				check_success(cudaMemcpy(device_gamma2, gamma2, sizeof(float) * b4dims.batched_length(), cudaMemcpyHostToDevice));
				check_success(cudaMemcpy(device_mean2, mean2, sizeof(float) * b4dims.batched_length(), cudaMemcpyHostToDevice));
				check_success(cudaMemcpy(device_inv_std2, inv_std2, sizeof(float) * b4dims.batched_length(), cudaMemcpyHostToDevice));


				concatenate_cols_gpu(device_out1, device_out1_conc, out1dims_gpu.length, gpu_batch_size);
				check_success(cudaDeviceSynchronize());

				gemm_gpu_xnor(device_wc2, device_out1_conc, device_out1,
				wc4dims.num, wc4dims.length,
				out1dims_gpu.length/32, out1dims_gpu.num,
				wc4dims.length, out4dims_gpu.num, 
				device_b2, device_beta2, device_gamma2, device_mean2, device_inv_std2, true);
				check_success(cudaDeviceSynchronize());

				//
				check_success(cudaMemcpy(out4, device_out1, sizeof(float) * out4dims_gpu.batched_length(), cudaMemcpyDeviceToHost));
				//print_acc(out4, out4dims.batched_length());

				check_success(cudaFree(device_out1_conc));
				check_success(cudaFree(device_out1));
				check_success(cudaFree(device_b2));
				check_success(cudaFree(device_beta2));
				check_success(cudaFree(device_gamma2));
				check_success(cudaFree(device_mean2));
				check_success(cudaFree(device_inv_std2));
				check_success(cudaFree(device_wc2));

			}
			else if (proxy_type == CPU_PROXY) {

				float *x = x_original + gpu_batch_size * xdims.length; 

				reshape(x, xdims_cpu.batched_length());

				gemm_cpu(x, w1, out1, cpu_batch_size,  IMAGE_SIZE, OUT1_SIZE, b1);
				end = now();
				elapsed1 += std::chrono::duration<double, std::milli>(end - start).count(); 
				std::cout << "elapsed = " << elapsed1 << " milliseconds\n";

				// The input layer - batchNormrLayer
				batch_normalization(beta1, gamma1, mean1, inv_std1, out1, out1dims_cpu.num, out1dims_cpu.length);

				// The input layer - NonlinearityLayer
				SignTheano(out1, out1dims_cpu.batched_length());

				// The first hidden layer
				unsigned int *Ac = new unsigned int[cpu_batch_size*OUT1_SIZE/32]; 
				unsigned int *Bc = new unsigned int[OUT1_SIZE*OUT1_SIZE/32]; 

				concatenate_rows_cpu(out1, Ac, cpu_batch_size*OUT1_SIZE/32);

				xnor_gemm_cpu(Ac, wc2, out2, cpu_batch_size, OUT1_SIZE/32, OUT1_SIZE, b2);

				batch_normalization(beta2, gamma2, mean2, inv_std2, out2, out2dims_cpu.num, out2dims_cpu.length);

				SignTheano(out2, out2dims_cpu.batched_length());

				// The second hidden layer
				concatenate_rows_cpu(out2, Ac, cpu_batch_size*OUT1_SIZE/32);

				xnor_gemm_cpu(Ac, wc3, out3, cpu_batch_size, OUT1_SIZE/32, OUT1_SIZE, b3);

				batch_normalization(beta3, gamma3, mean3, inv_std3, out3, out3dims_cpu.num, out3dims_cpu.length);

				SignTheano(out3, out3dims_cpu.batched_length());

				// The output layer
				concatenate_rows_cpu(out3, Ac, cpu_batch_size*OUT1_SIZE/32);

				xnor_gemm_cpu(Ac, wc4, out4, cpu_batch_size, OUT1_SIZE/32, OUT4_SIZE, b4);

				batch_normalization(beta4, gamma4, mean4, inv_std4, out4, out4dims_cpu.num, out4dims_cpu.length);
			}
		}));
	}

	std::for_each(proxy_threads.begin(), proxy_threads.end(), [](std::thread &t) { t.join(); });


  end = now();
  elapsed1 += std::chrono::duration<double, std::milli>(end - start).count(); 
  std::cout << "Total GPU runtime elapsed = " << elapsed1 << " milliseconds\n";

  // ignore argmax for now
/*
  float* out4T = allocate<float>(out4dims);

  transpose(out4, out4T, out4dims.length, out4dims.num);
  unsigned int max_idx = 0;

  for(int j = 0; j < out4dims.num; j++){
 	for (int i = 0; i < out4dims.length; i++)
 	 {
 	   float max_value = 0;
 	   if (max_value < out4T[j * out4dims.length + i])
 	   {
 	     max_idx = j * out4dims.length + i; 
 	     max_value = out4T[j*out4dims.length + i]; 
 	   }
 	 }
  	//print_acc(out1, out4dims.batched_length());
  	std::cout << "Category: " << max_idx%(out4dims.length )<< std::endl;
}
*/

	delete [] x_original;
	delete [] w1;
	delete [] b1;
	delete [] beta1;
	delete [] gamma1;
	delete [] mean1;
	delete [] inv_std1;

	delete [] b2;
	delete [] beta2;
	delete [] gamma2;
	delete [] mean2;
	delete [] inv_std2;
	delete [] wc2;
	delete [] out4;
//  delete [] out4T;
}
