all:
	g++ -std=c++11 main_cpu.cpp -o bnn_cpu
	nvcc -std=c++11 main_gpu_fusion.cu -lcublas -o bnn_gpu_fusion
	nvcc -std=c++11 main_gpu_cp.cu -lcublas -o bnn_gpu_cp
cpu:
	g++ -std=c++11 main_cpu.cpp -o bnn_cpu
gpu:
	nvcc -std=c++11 main_gpu_fusion.cu -lcublas -o bnn_gpu_fusion
gpucomp:
	nvcc -std=c++11 main_gpu_compress.cu -lcublas -o bnn_gpu_compress
cp:
	nvcc -std=c++11 main_gpu_cp.cu util_function_cp.cu -lcublas -o bnn_gpu_cp
clean:
	rm bnn_cpu bnn_gpu_fusion bnn_gpu_cp

