
#include "main_cpu.hpp"

template<typename T>
static void writedata(std::string fileName, shape dims, T *input){
  FILE *fout;
  fout=fopen(fileName.c_str(), "wb");
  for(int i = 0; i < dims.num; i++){
    fwrite(&input[i * dims.length], sizeof(T), dims.length, fout);
  }
  fclose(fout);
}
unsigned int concatenate(float* array)
{
    unsigned int rvalue=0;
    unsigned int sign;
    
    for (int i = 0; i < 32; i++)
    {
        sign = (array[i]>=0);
        rvalue = rvalue | (sign<<i);
    }
    
    return rvalue;
}

void concatenate_rows_cpu(float *a, unsigned int *b, int size)
{
    for (int i = 0; i < size; i++)
        b[i] = concatenate(&a[i*32]);
}

void concatenate_rows_cpu_2d(float *a, unsigned int *b, int m, int n)
{
  for(int j = 0; j < m; j ++)
    for (int i = 0; i < n/32; i++)
        b[j*n/32 + i] = concatenate(&a[j*n + i*32]);
}

int main(){

  shape wc2rdims= {OUT1_SIZE, OUT1_SIZE/32};
  unsigned int* wc2r = allocate<unsigned int>(wc2rdims);
  //float* w2 = allocate<float>(w2dims);

  readdata("params/w2.bin", w2dims, w2);
  //printf("blabla\n");
  gemm
  //concatenate_rows_cpu_2d(w2, wc2r, w2dims.num, w2dims.length);
  //concatenate_rows_cpu(w2, wc2r, wc2rdims.batched_length());
  //for(int i  =wc2rdims.batched_length() - 100; i < wc2rdims.batched_length(); i++)
   // printf("%dth: %d\n", i, wc2r[i]);
  //writedata("params/wc2r.bin", wc2rdims, wc2r);

  delete [] wc2r;
  delete [] w2;

  return 0;
}
