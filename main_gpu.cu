#include "main_cpu.hpp"
#include "math.h"
#include <iostream>
//#include <cuda_runtime.h>
#include "cublas_v2.h"
#include <cassert>

#define TILE_WIDTH_N 8//16
#define TILE_WIDTH_K 1//4
#define TILE_WIDTH_M TILE_WIDTH_N*TILE_WIDTH_K

void gemm_cpu(float* A, float* B, float *C, int A_row, int A_col, int B_col, float *b){
  for(int i = 0; i < A_row; i++){
    for(int j =0; j < B_col; j++){
      float acc = b[j];
      for(int k =0; k < A_col; k++){
      	acc += A[i *A_col + k] * B[ k *B_col + j];
      }
      C[i * B_col + j] = acc;
    }
  }
}

__global__ void gemm_gpu_kernel(float* A, float* B, float *C, int A_row, int A_col, int B_col, float *b){
  if(blockIdx.x == 0){
    for(int i = 0; i < A_row; i++){
    for(int j =0; j < B_col; j++){
      float acc = b[j];
      for(int k =0; k < A_col; k++){
      	acc += A[i *A_col + k] * B[ k *B_col + j];
      }
      C[i * B_col + j] = acc;
    }
  }
  }
}

//C[BATCH_SIZE][OUT1_SIZE]=A[BATCH_SIZE][IMAGE_SIZE]*B[IMAGE_SIZE][OUT1_SIZE]
__global__ void gemm_gpu_optimized_kernel(float* A, float* B, float *C, int numARows, int numAColumns, int numBColumns, float *b,
                                          float* beta, float* gamma, float* mean, float* inv_std)
{
  int numBRows = IMAGE_SIZE;
  int numCRows = numARows;
  int numCColumns = numBColumns; 
  A += blockIdx.y * TILE_WIDTH_M + threadIdx.y; //cal offset for A, B, C
  B += blockIdx.x * TILE_WIDTH_N; 
  C += (blockIdx.y * TILE_WIDTH_M + threadIdx.y) * numCColumns + blockIdx.x * TILE_WIDTH_N;
  
  __shared__ float Bds[TILE_WIDTH_K][TILE_WIDTH_N];
  float A_reg[TILE_WIDTH_K] = {0};
  float result[TILE_WIDTH_N] = {0};

  int Bds_idx_y = threadIdx.y / TILE_WIDTH_N;
  int Bds_idx_x = threadIdx.y % TILE_WIDTH_N;
  
  for (int m = 0; m < ((numAColumns+TILE_WIDTH_K-1) / TILE_WIDTH_K); m++) // one step calcuate K col
  {
    for (int i = 0; i < TILE_WIDTH_K; i++)
      A_reg[i] = ((m*TILE_WIDTH_K + i) < numAColumns) ? A[(m*TILE_WIDTH_K + i) * numARows] : 0; 

    if ( ((blockIdx.x * TILE_WIDTH_N + Bds_idx_x ) < numBColumns) && ((m * TILE_WIDTH_K + Bds_idx_y) < numBRows) )
      Bds[Bds_idx_y][Bds_idx_x] = B[(m * TILE_WIDTH_K + Bds_idx_y)*numBColumns + Bds_idx_x];  //load B to shared MEM
    else
      Bds[Bds_idx_y][Bds_idx_x] = 0;
    __syncthreads();
    for (int i = 0; i < TILE_WIDTH_K; i++)
    {
      for (int j = 0; j < TILE_WIDTH_N; j++)
      {
        result[j] += A_reg[i] * Bds[i][j];
      }
    }
    __syncthreads();
  } //complete K steps
  for (int i = 0; i < TILE_WIDTH_N; i++)
  {
    if (((blockIdx.y * TILE_WIDTH_M + threadIdx.y) < numCRows ) &&
         ((blockIdx.x * TILE_WIDTH_N + i) < numCColumns) )
      //Normalization + binnarization
      C[i] = ((result[i] + b[blockIdx.x * TILE_WIDTH_N + i] - mean[blockIdx.x * TILE_WIDTH_N + i]) * gamma[blockIdx.x * TILE_WIDTH_N + i] * inv_std[blockIdx.x * TILE_WIDTH_N + i] + beta[blockIdx.x * TILE_WIDTH_N + i]) >= 0.0? 1.0:-1.0;
  }
}


__global__ void gemm_gpu_xnor_optimized_kernel(unsigned int* A, unsigned int* B, float *C, int numARows, int numAColumns, int numBColumns, float *b,
                                          float* beta, float* gamma, float* mean, float* inv_std)
{
  int numBRows = IMAGE_SIZE;
  int numCRows = numARows;
  int numCColumns = numBColumns;
  A += blockIdx.y * TILE_WIDTH_M + threadIdx.y; //cal offset for A, B, C
  B += blockIdx.x * TILE_WIDTH_N; 
  C += (blockIdx.y * TILE_WIDTH_M + threadIdx.y) * numCColumns + blockIdx.x * TILE_WIDTH_N;
  
  __shared__ unsigned int Bds[TILE_WIDTH_K][TILE_WIDTH_N];
  unsigned int A_reg[TILE_WIDTH_K] = {0};
  float result[TILE_WIDTH_N] = {0};

  int Bds_idx_y = threadIdx.y / TILE_WIDTH_N;
  int Bds_idx_x = threadIdx.y % TILE_WIDTH_N;
  
  for (int m = 0; m < ((numAColumns+TILE_WIDTH_K-1) / TILE_WIDTH_K); m++) // one step calcuate K col
  {
    for (int i = 0; i < TILE_WIDTH_K; i++)
      A_reg[i] = ((m*TILE_WIDTH_K + i) < numAColumns) ? A[(m*TILE_WIDTH_K + i) * numARows] : 0; 

    if ( ((blockIdx.x * TILE_WIDTH_N + Bds_idx_x ) < numBColumns) && ((m * TILE_WIDTH_K + Bds_idx_y) < numBRows) )
      Bds[Bds_idx_y][Bds_idx_x] = B[(m * TILE_WIDTH_K + Bds_idx_y)*numBColumns + Bds_idx_x];  //load B to shared MEM
    else
      Bds[Bds_idx_y][Bds_idx_x] = 0;
    __syncthreads();
    for (int i = 0; i < TILE_WIDTH_K; i++)
    {
      for (int j = 0; j < TILE_WIDTH_N; j++)
      {
        unsigned int A_tmp = (unsigned int)A_reg[i];
        unsigned int B_tmp = (unsigned int)Bds[i][j];
        result[j] += __popc(A_tmp ^ B_tmp);
      }
    }
    __syncthreads();
  } //complete K steps
  for (int i = 0; i < TILE_WIDTH_N; i++)
  {
    if (((blockIdx.y * TILE_WIDTH_M + threadIdx.y) < numCRows ) &&
         ((blockIdx.x * TILE_WIDTH_N + i) < numCColumns) )
      C[i] = (( -(2*(float)result[i]-(float)32*numAColumns)  - mean[blockIdx.x * TILE_WIDTH_N + i]) * gamma[blockIdx.x * TILE_WIDTH_N + i] * inv_std[blockIdx.x * TILE_WIDTH_N + i] + beta[blockIdx.x * TILE_WIDTH_N + i]) >= 0.0? 1.0:-1.0;
      //Normalization + binnarization
  }
}

__global__ void gemm_gpu_xnor_l4_optimized_kernel(unsigned int* A, unsigned int* B, float *C, int numARows, int numAColumns, int numBColumns, float *b,
                                          float* beta, float* gamma, float* mean, float* inv_std)
{
  int numBRows = IMAGE_SIZE;
  int numCRows = numARows;
  int numCColumns = numBColumns;
  A += blockIdx.y * TILE_WIDTH_M + threadIdx.y; //cal offset for A, B, C
  B += blockIdx.x * TILE_WIDTH_N; 
  C += (blockIdx.y * TILE_WIDTH_M + threadIdx.y) * numCColumns + blockIdx.x * TILE_WIDTH_N;
  
  __shared__ unsigned int Bds[TILE_WIDTH_K][TILE_WIDTH_N];
  unsigned int A_reg[TILE_WIDTH_K] = {0};
  float result[TILE_WIDTH_N] = {0};

  int Bds_idx_y = threadIdx.y / TILE_WIDTH_N;
  int Bds_idx_x = threadIdx.y % TILE_WIDTH_N;
  
  for (int m = 0; m < ((numAColumns+TILE_WIDTH_K-1) / TILE_WIDTH_K); m++) // one step calcuate K col
  {
    for (int i = 0; i < TILE_WIDTH_K; i++)
      A_reg[i] = ((m*TILE_WIDTH_K + i) < numAColumns) ? A[(m*TILE_WIDTH_K + i) * numARows] : 0; 

    if ( ((blockIdx.x * TILE_WIDTH_N + Bds_idx_x ) < numBColumns) && ((m * TILE_WIDTH_K + Bds_idx_y) < numBRows) )
      Bds[Bds_idx_y][Bds_idx_x] = B[(m * TILE_WIDTH_K + Bds_idx_y)*numBColumns + Bds_idx_x];  //load B to shared MEM
    else
      Bds[Bds_idx_y][Bds_idx_x] = 0;
    __syncthreads();
    for (int i = 0; i < TILE_WIDTH_K; i++)
    {
      for (int j = 0; j < TILE_WIDTH_N; j++)
      {
        unsigned int A_tmp = (unsigned int)A_reg[i];
        unsigned int B_tmp = (unsigned int)Bds[i][j];
        result[j] += __popc(A_tmp ^ B_tmp);
      }
    }
    __syncthreads();
  } //complete K steps
  for (int i = 0; i < TILE_WIDTH_N; i++)
  {
    if (((blockIdx.y * TILE_WIDTH_M + threadIdx.y) < numCRows ) &&
         ((blockIdx.x * TILE_WIDTH_N + i) < numCColumns) )
      C[i] = ( -(2*(float)result[i]-(float)32*numAColumns)  - mean[blockIdx.x * TILE_WIDTH_N + i]) * gamma[blockIdx.x * TILE_WIDTH_N + i] * inv_std[blockIdx.x * TILE_WIDTH_N + i] + beta[blockIdx.x * TILE_WIDTH_N + i];
      //Normalization
  }
}

static void gemm_gpu(float* A, float* B, float *C, int A_row, int A_col, int B_col, float *b,
                     float* beta, float* gamma, float* mean, float* inv_std)
{
  //launch naive GEMM   
//  gemm_gpu_kernel<<<1,1>>>(A, B, C, A_row, A_col, B_col, b);
  //launch GEMM with Optimization
  int block_size_x = 1;
  int block_size_y = TILE_WIDTH_M;
  int grid_size_x = (OUT1_SIZE + TILE_WIDTH_N - 1) / TILE_WIDTH_N;
  int grid_size_y = (BATCH_SIZE + TILE_WIDTH_M - 1) / TILE_WIDTH_M;
  dim3 dimBlock(block_size_x, block_size_y);
  dim3 dimGrid(grid_size_x, grid_size_y);
  gemm_gpu_optimized_kernel<<<dimGrid, dimBlock>>>(A, B, C, A_row, A_col, B_col, b,
                                                   beta, gamma, mean, inv_std);
  
}

static void gemm_gpu_xnor(unsigned int * A, unsigned int* B, float *C, int A_row, int A_col, int B_col, float *b,
                     float* beta, float* gamma, float* mean, float* inv_std)
{
  //launch GEMM with Optimization
  int block_size_x = 1;
  int block_size_y = TILE_WIDTH_M;
  int grid_size_x = (OUT1_SIZE + TILE_WIDTH_N - 1) / TILE_WIDTH_N;
  int grid_size_y = (BATCH_SIZE + TILE_WIDTH_M - 1) / TILE_WIDTH_M;
  dim3 dimBlock(block_size_x, block_size_y);
  dim3 dimGrid(grid_size_x, grid_size_y);
  gemm_gpu_xnor_optimized_kernel<<<dimGrid, dimBlock>>>(A, B, C, A_row, A_col, B_col, b,
                                                   beta, gamma, mean, inv_std); 
}

static void gemm_gpu_xnor_l4(unsigned int * A, unsigned int* B, float *C, int A_row, int A_col, int B_col, float *b,
                     float* beta, float* gamma, float* mean, float* inv_std)
{
  //launch GEMM with Optimization
  int block_size_x = 1;
  int block_size_y = TILE_WIDTH_M;
  int grid_size_x = (OUT1_SIZE + TILE_WIDTH_N - 1) / TILE_WIDTH_N;
  int grid_size_y = (BATCH_SIZE + TILE_WIDTH_M - 1) / TILE_WIDTH_M;
  dim3 dimBlock(block_size_x, block_size_y);
  dim3 dimGrid(grid_size_x, grid_size_y);
  gemm_gpu_xnor_l4_optimized_kernel<<<dimGrid, dimBlock>>>(A, B, C, A_row, A_col, B_col, b,
                                                   beta, gamma, mean, inv_std);
}

unsigned int concatenate(float* array)
{
    unsigned int rvalue=0;
    unsigned int sign;
    
    for (int i = 0; i < 32; i++)
    {
        sign = (array[i]>=0);
        rvalue = rvalue | (sign<<i);
    }
    
    return rvalue;
}

void concatenate_rows_cpu(float *a, unsigned int *b, int size)
{
    for (int i = 0; i < size; i++)
        b[i] = concatenate(&a[i*32]);
}

void concatenate_cols_cpu(float *a, unsigned int *b, int m, int n)
{
    for (int j = 0; j < n; j++)
    {
        float * array = new float[32];
        for(int i=0; i<m; i+=32){
            for(int k=0; k<32;k++) array[k] = a[j + n*(i+k)];
            b[j+n*i/32]=concatenate(array); 
        } 
        delete[] array;
    }
}


//this should be merged into gemm_gpu
void reshape(float *x, int size){
  for(int i = 0; i < size ; i++){
    x[i] = 2 * x[i] -1;
  }
}


//following the definition of http://lasagne.readthedocs.io/en/latest/modules/layers/normalization.html
//the real code is https://github.com/Lasagne/Lasagne/blob/master/lasagne/layers/normalization.py#L120-L320, line 320
void batch_normalization(float* beta, float* gamma, float* mean, float* inv_std, float *x, int batch_num, int onebatchsize){
  for( int j = 0; j < batch_num; j++){
    for (int i = 0; i < onebatchsize; i++){ 
      x[j * onebatchsize + i] = (x[j * onebatchsize + i] - mean[i] ) * gamma[i] * inv_std[i] + beta[i];
    }
  }
}

//the function binary_ops.SignTheano(x), binarize the output x  = (x>= 0) ? 1.0 : -1.0;
void SignTheano(float* x, int size){
	for(int i = 0; i < size; i++){
		x[i] = (x[i] >=0.0) ? 1.0 : -1.0;

	}
}

int main(int argc, char **argv){
  //allocate the memory for input and output weights
  //for input layer
  float *x = allocate<float>(xdims);
  float *w1 = allocate<float>(w1dims);
  float *b1 = allocate<float>(b1dims);
  float *beta1 = allocate<float>(b1dims); 
  float *gamma1 = allocate<float>(b1dims);
  float *mean1 = allocate<float>(b1dims);
  float *inv_std1 = allocate<float>(b1dims);
  
  float *device_x = allocate<float>(xdims);
  float *device_w1 = allocate<float>(w1dims);
  float *device_b1 = allocate<float>(b1dims);
  float *device_out1 = allocate<float>(out1dims);
  float *device_beta1 = allocate<float>(b1dims); 
  float *device_gamma1 = allocate<float>(b1dims);
  float *device_mean1 = allocate<float>(b1dims);
  float *device_inv_std1 = allocate<float>(b1dims);

  //for 2nd layer
  float *w2 = allocate<float>(w2dims);
  float *b2 = allocate<float>(b2dims);
  float *beta2 = allocate<float>(b2dims); 
  float *gamma2 = allocate<float>(b2dims);
  float *mean2 = allocate<float>(b2dims);
  float *inv_std2 = allocate<float>(b2dims);
  
  float *device_w2 = allocate<float>(w2dims);
  float *device_b2 = allocate<float>(b2dims);
  float *device_out2 = allocate<float>(out2dims);
  float *device_beta2 = allocate<float>(b2dims); 
  float *device_gamma2 = allocate<float>(b2dims);
  float *device_mean2 = allocate<float>(b2dims);
  float *device_inv_std2 = allocate<float>(b2dims);
  
  //for 3rd layer
  float *w3 = allocate<float>(w3dims);
  float *b3 = allocate<float>(b3dims);
  float *beta3 = allocate<float>(b3dims); 
  float *gamma3 = allocate<float>(b3dims);
  float *mean3 = allocate<float>(b3dims);
  float *inv_std3 = allocate<float>(b3dims);
    
  float *device_w3 = allocate<float>(w3dims);
  float *device_b3 = allocate<float>(b3dims);
  float *device_out3 = allocate<float>(out3dims);
  float *device_beta3 = allocate<float>(b3dims); 
  float *device_gamma3 = allocate<float>(b3dims);
  float *device_mean3 = allocate<float>(b3dims);
  float *device_inv_std3 = allocate<float>(b3dims);
  
  //for 4th layer
  float *w4 = allocate<float>(w4dims);
  float *b4 = allocate<float>(b4dims);
  float *beta4 = allocate<float>(b4dims); 
  float *gamma4 = allocate<float>(b4dims);
  float *mean4 = allocate<float>(b4dims);
  float *inv_std4 = allocate<float>(b4dims);
  
  float *device_w4 = allocate<float>(w4dims);
  float *device_b4 = allocate<float>(b4dims);
  float *device_out4 = allocate<float>(out4dims);
  float *device_beta4 = allocate<float>(b4dims); 
  float *device_gamma4 = allocate<float>(b4dims);
  float *device_mean4 = allocate<float>(b4dims);
  float *device_inv_std4 = allocate<float>(b4dims);
  
  readdata("params/x.bin", xdims, x);
  readdata("params/w1.bin", w1dims, w1);
  readdata("params/b1.bin", b1dims, b1);
  readdata("params/beta1.bin", b1dims, beta1);
  readdata("params/gamma1.bin", b1dims, gamma1);
  readdata("params/inv_std1.bin", b1dims, inv_std1);
  readdata("params/mean1.bin", b1dims, mean1);
  
  readdata("params/w2.bin", w2dims, w2);
  readdata("params/b2.bin", b2dims, b2);
  readdata("params/beta2.bin", b2dims, beta2);
  readdata("params/gamma2.bin", b2dims, gamma2);
  readdata("params/inv_std2.bin", b2dims, inv_std2);
  readdata("params/mean2.bin", b2dims, mean2);

  readdata("params/w3.bin", w3dims, w3);
  readdata("params/b3.bin", b3dims, b3);
  readdata("params/beta3.bin", b3dims, beta3);
  readdata("params/gamma3.bin", b3dims, gamma3);
  readdata("params/inv_std3.bin", b3dims, inv_std3);
  readdata("params/mean3.bin", b3dims, mean3);

  readdata("params/w4.bin", w4dims, w4);
  readdata("params/b4.bin", b4dims, b4);
  readdata("params/beta4.bin", b4dims, beta4);
  readdata("params/gamma4.bin", b4dims, gamma4);
  readdata("params/inv_std4.bin", b4dims, inv_std4);
  readdata("params/mean4.bin", b4dims, mean4);

  //dense layer1, output is batchsize * 4096
  float *out1 = allocate<float>(out1dims);
  float *out2 = allocate<float>(out2dims);  
  float *out3 = allocate<float>(out3dims);
  float *out4 = allocate<float>(out4dims);
  float *out1_GR = allocate<float>(out1dims);

  //reshpe to the range of [-1, 1]
  reshape(x, xdims.batched_length());

  std::chrono::time_point<std::chrono::high_resolution_clock>start, end;
  double elapsed1 = 0;
  //time  
  start = now();
  
  //Prepare for layer1 kernel launch
  cudaMalloc((void **)&device_x, sizeof(float) * xdims.batched_length());
  cudaMalloc((void **)&device_w1, sizeof(float) * w1dims.batched_length());
  cudaMalloc((void **)&device_b1, sizeof(float) * b1dims.batched_length());
  cudaMalloc((void **)&device_out1, sizeof(float) * out1dims.batched_length()); 
  cudaMalloc((void **)&device_beta1, sizeof(float) * b1dims.batched_length());
  cudaMalloc((void **)&device_gamma1, sizeof(float) * b1dims.batched_length());
  cudaMalloc((void **)&device_mean1, sizeof(float) * b1dims.batched_length());
  cudaMalloc((void **)&device_inv_std1, sizeof(float) * b1dims.batched_length());
  
  cudaMemcpy(device_x, x, sizeof(float) * xdims.batched_length(), cudaMemcpyHostToDevice);
  cudaMemcpy(device_w1, w1, sizeof(float) * w1dims.batched_length(), cudaMemcpyHostToDevice);
  cudaMemcpy(device_b1, b1, sizeof(float) * b1dims.batched_length(), cudaMemcpyHostToDevice);
  cudaMemcpy(device_beta1, beta1, sizeof(float) * b1dims.batched_length(), cudaMemcpyHostToDevice);
  cudaMemcpy(device_gamma1, gamma1, sizeof(float) * b1dims.batched_length(), cudaMemcpyHostToDevice);
  cudaMemcpy(device_mean1, mean1, sizeof(float) * b1dims.batched_length(), cudaMemcpyHostToDevice);
  cudaMemcpy(device_inv_std1, inv_std1, sizeof(float) * b1dims.batched_length(), cudaMemcpyHostToDevice);
  //launch kernel
  gemm_gpu(device_x, device_w1, device_out1, BATCH_SIZE, IMAGE_SIZE, OUT1_SIZE, device_b1,
           device_beta1, device_gamma1, device_mean1, device_inv_std1);
  cudaDeviceSynchronize(); 
  cudaMemcpy(out1, device_out1, sizeof(float) * out1dims.batched_length(), cudaMemcpyDeviceToHost); 
  
  unsigned int *Ac = new unsigned int[BATCH_SIZE*OUT1_SIZE/8]; 
  unsigned int *Bc = new unsigned int[OUT1_SIZE*OUT1_SIZE/8]; 
  unsigned int *device_Ac = new unsigned int[BATCH_SIZE*OUT1_SIZE/8]; 
  unsigned int *device_Bc = new unsigned int[OUT1_SIZE*OUT1_SIZE/8]; 
  
  concatenate_rows_cpu(out1, Ac, BATCH_SIZE*OUT1_SIZE/32);
  concatenate_cols_cpu(w2, Bc, OUT1_SIZE, OUT1_SIZE);
//  xnor_gemm_cpu(Ac, Bc, out2, BATCH_SIZE, OUT1_SIZE/32, OUT1_SIZE, b2);
  
  //Prepare for layer2 kernel launch
  cudaMalloc((void **)&device_Ac, sizeof(unsigned int) * BATCH_SIZE*OUT1_SIZE/8);
  cudaMalloc((void **)&device_Bc, sizeof(unsigned int) * OUT1_SIZE*OUT1_SIZE/8);
  cudaMalloc((void **)&device_b2, sizeof(float) * b2dims.batched_length());
  cudaMalloc((void **)&device_out2, sizeof(float) * out2dims.batched_length()); 
  cudaMalloc((void **)&device_beta2, sizeof(float) * b2dims.batched_length());
  cudaMalloc((void **)&device_gamma2, sizeof(float) * b2dims.batched_length());
  cudaMalloc((void **)&device_mean2, sizeof(float) * b2dims.batched_length());
  cudaMalloc((void **)&device_inv_std2, sizeof(float) * b2dims.batched_length());
  
  cudaMemcpy(device_Ac, Ac, sizeof(unsigned int) * BATCH_SIZE*OUT1_SIZE/8, cudaMemcpyHostToDevice);
  cudaMemcpy(device_Bc, Bc, sizeof(unsigned int) * OUT1_SIZE*OUT1_SIZE/8, cudaMemcpyHostToDevice);
  cudaMemcpy(device_b2, b2, sizeof(float) * b2dims.batched_length(), cudaMemcpyHostToDevice);
  cudaMemcpy(device_beta2, beta2, sizeof(float) * b2dims.batched_length(), cudaMemcpyHostToDevice);
  cudaMemcpy(device_gamma2, gamma2, sizeof(float) * b2dims.batched_length(), cudaMemcpyHostToDevice);
  cudaMemcpy(device_mean2, mean2, sizeof(float) * b2dims.batched_length(), cudaMemcpyHostToDevice);
  cudaMemcpy(device_inv_std2, inv_std2, sizeof(float) * b2dims.batched_length(), cudaMemcpyHostToDevice);
  
  //launch kernel
  gemm_gpu_xnor(device_Ac, device_Bc, device_out2, BATCH_SIZE, OUT1_SIZE/32, OUT1_SIZE, device_b2,
           device_beta2, device_gamma2, device_mean2, device_inv_std2);
  cudaDeviceSynchronize(); 
  cudaMemcpy(out2, device_out2, sizeof(float) * out2dims.batched_length(), cudaMemcpyDeviceToHost); 
   
  concatenate_rows_cpu(out2, Ac, BATCH_SIZE*OUT1_SIZE/32);
  concatenate_cols_cpu(w3, Bc, OUT1_SIZE, OUT1_SIZE);
  
  //Prepare for layer3 kernel launch
  cudaMalloc((void **)&device_b3, sizeof(float) * b3dims.batched_length());
  cudaMalloc((void **)&device_out3, sizeof(float) * out3dims.batched_length()); 
  cudaMalloc((void **)&device_beta3, sizeof(float) * b3dims.batched_length());
  cudaMalloc((void **)&device_gamma3, sizeof(float) * b3dims.batched_length());
  cudaMalloc((void **)&device_mean3, sizeof(float) * b3dims.batched_length());
  cudaMalloc((void **)&device_inv_std3, sizeof(float) * b3dims.batched_length());
  
  cudaMemcpy(device_Ac, Ac, sizeof(unsigned int) * BATCH_SIZE*OUT1_SIZE/8, cudaMemcpyHostToDevice);
  cudaMemcpy(device_Bc, Bc, sizeof(unsigned int) * OUT1_SIZE*OUT1_SIZE/8, cudaMemcpyHostToDevice);
  cudaMemcpy(device_b3, b3, sizeof(float) * b3dims.batched_length(), cudaMemcpyHostToDevice);
  cudaMemcpy(device_beta3, beta3, sizeof(float) * b3dims.batched_length(), cudaMemcpyHostToDevice);
  cudaMemcpy(device_gamma3, gamma3, sizeof(float) * b3dims.batched_length(), cudaMemcpyHostToDevice);
  cudaMemcpy(device_mean3, mean3, sizeof(float) * b3dims.batched_length(), cudaMemcpyHostToDevice);
  cudaMemcpy(device_inv_std3, inv_std3, sizeof(float) * b3dims.batched_length(), cudaMemcpyHostToDevice);
 
  //launch kernel
  gemm_gpu_xnor(device_Ac, device_Bc, device_out3, BATCH_SIZE, OUT1_SIZE/32, OUT1_SIZE, device_b3,
           device_beta3, device_gamma3, device_mean3, device_inv_std3);
  cudaDeviceSynchronize(); 
  cudaMemcpy(out3, device_out3, sizeof(float) * out3dims.batched_length(), cudaMemcpyDeviceToHost); 
  
   
  concatenate_rows_cpu(out3, Ac, BATCH_SIZE*OUT1_SIZE/32);
  concatenate_cols_cpu(w4, Bc, OUT1_SIZE, OUT4_SIZE);
  
  //Prepare for layer4 kernel launch
  cudaMalloc((void **)&device_out4, sizeof(float) * out4dims.batched_length()); 
  cudaMalloc((void **)&device_beta4, sizeof(float) * b4dims.batched_length());
  cudaMalloc((void **)&device_gamma4, sizeof(float) * b4dims.batched_length());
  cudaMalloc((void **)&device_mean4, sizeof(float) * b4dims.batched_length());
  cudaMalloc((void **)&device_inv_std4, sizeof(float) * b4dims.batched_length());
  
  cudaMemcpy(device_Ac, Ac, sizeof(unsigned int) * BATCH_SIZE*OUT1_SIZE/8, cudaMemcpyHostToDevice);
  cudaMemcpy(device_Bc, Bc, sizeof(unsigned int) * OUT1_SIZE*OUT1_SIZE/8, cudaMemcpyHostToDevice);
  cudaMemcpy(device_beta4, beta4, sizeof(float) * b4dims.batched_length(), cudaMemcpyHostToDevice);
  cudaMemcpy(device_gamma4, gamma4, sizeof(float) * b4dims.batched_length(), cudaMemcpyHostToDevice);
  cudaMemcpy(device_mean4, mean4, sizeof(float) * b4dims.batched_length(), cudaMemcpyHostToDevice);
  cudaMemcpy(device_inv_std4, inv_std4, sizeof(float) * b4dims.batched_length(), cudaMemcpyHostToDevice);
  
  //launch kernel
  gemm_gpu_xnor_l4(device_Ac, device_Bc, device_out4, BATCH_SIZE, OUT1_SIZE/32, OUT4_SIZE, device_b4,
           device_beta4, device_gamma4, device_mean4, device_inv_std4);
  cudaDeviceSynchronize(); 
  cudaMemcpy(out4, device_out4, sizeof(float) * out4dims.batched_length(), cudaMemcpyDeviceToHost);
  
  float max_idx = 0;
  for (int i = 0; i < out4dims.batched_length(); i++)
  {
    float max_value = 0;
    if (max_value < out4[i])
    {
      max_idx = i; 
      max_value = out4[i]; 
    }
  }
  std::cout << "Category: " << max_idx << std::endl;
  
  //***use cuBlas***// 
  //C[BATCH_SIZE][OUT1_SIZE]=A[BATCH_SIZE][IMAGE_SIZE]*B[IMAGE_SIZE][OUT1_SIZE]
/*  cublasHandle_t handle;
  cublasCreate(&handle);
  const float alp = 1;
  const float bet = 0;
  const float *alpha = &alp;
  const float *beta = &bet;
  cublasSgemm(handle, CUBLAS_OP_T, CUBLAS_OP_T, (int)BATCH_SIZE, (int)OUT1_SIZE, (int)IMAGE_SIZE, alpha, device_x, IMAGE_SIZE, device_w1, OUT1_SIZE, beta, device_out1, (int)BATCH_SIZE);
  cudaMemcpy(out1, device_out1, sizeof(float) * out1dims.batched_length(), cudaMemcpyDeviceToHost); 
  cublasDestroy(handle);*/
  //***end cuBlas***//
  
  end = now();
  
  
  //call GEMM
  
  //float* A, float* B, float *C, int A_row(BATCH_SIZE), int A_col(IMAGE_SIZE), int B_col(OUT1_SIZE), float *b
  gemm_cpu(x, w1, out1_GR, BATCH_SIZE,  IMAGE_SIZE, OUT1_SIZE, b1);
  
  elapsed1 += std::chrono::duration<double, std::milli>(end - start).count(); 
  std::cout << "elapsed = " << elapsed1 << " milliseconds\n";

  print_acc(out4, out4dims.batched_length());//from GPU kernel
    
  //batchNormrLayer
  batch_normalization(beta1, gamma1, mean1, inv_std1, out1_GR, out1dims.num, out1dims.length);
  SignTheano(out1_GR, out1dims.batched_length());
  //printf("GR: "); print_acc(out1_GR, out1dims.batched_length());

  cudaFree(device_x);
  cudaFree(device_w1);
  cudaFree(device_b1);
  cudaFree(device_out1);
  cudaFree(device_beta1);
  cudaFree(device_gamma1);
  cudaFree(device_mean1);
  cudaFree(device_inv_std1);

  cudaFree(device_b2);
  cudaFree(device_out2);
  cudaFree(device_beta2);
  cudaFree(device_gamma2);
  cudaFree(device_mean2);
  cudaFree(device_inv_std2);
  
  cudaFree(device_b3);
  cudaFree(device_out3);
  cudaFree(device_beta3);
  cudaFree(device_gamma3);
  cudaFree(device_mean3);
  cudaFree(device_inv_std3);
  
  delete [] x;
  delete [] w1;
  delete [] b1;
  delete [] beta1;
  delete [] out1_GR;
  
  delete [] w2;
  delete [] b2;
  delete [] beta2;
  delete [] gamma2;
  delete [] mean2;
  delete [] inv_std2;

  delete [] w3;
  delete [] b3;
  delete [] beta3;
  delete [] gamma3;
  delete [] mean3;
  delete [] inv_std3;

  delete [] w4;
  delete [] b4;
  delete [] beta4;
  delete [] gamma4;
  delete [] mean4;
  delete [] inv_std4;

  delete [] out1;
  delete [] out2;
  delete [] out3;
  delete [] out4;
  
}
