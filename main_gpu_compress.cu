#include "main_cpu.hpp"
#include "util_function.hpp"
void gemm_cpu(float* A, float* B, float *C, int A_row, int A_col, int B_col, float *b){
    for(int i = 0; i < A_row; i++){
      for(int j =0; j < B_col; j++){
        float acc = b[j];
        for(int k =0; k < A_col; k++){
          acc += A[i *A_col + k] * B[ k *B_col + j];
        }
        C[i * B_col + j] = acc;
      }
    }
  }
int main(int argc, char **argv){

	float *x = allocate<float>(xdims);
        float *w1 = allocate<float>(w1dims);
        float *b1 = allocate<float>(b1dims);
        float *beta1 = allocate<float>(b1dims); 
        float *gamma1 = allocate<float>(b1dims);
        float *mean1 = allocate<float>(b1dims);
        float *inv_std1 = allocate<float>(b1dims);
	
       float *device_x;
        float *device_w1;
        float *device_b1;
        float *device_out1;
        float *device_beta1; 
       float *device_gamma1;
        float *device_mean1;
        float *device_inv_std1;
        
       readdata("params/x1000.bin", xdims, x);
        readdata("params/w1.bin", w1dims, w1);
        readdata("params/b1.bin", b1dims, b1);
        readdata("params/beta1.bin", b1dims, beta1);
        readdata("params/gamma1.bin", b1dims, gamma1);
        readdata("params/inv_std1.bin", b1dims, inv_std1);
        readdata("params/mean1.bin", b1dims, mean1);

	float* xT = x;
	xT = allocate<float>(xdims);
	transpose_reshape(x, xT, xdims.num, xdims.length);	
	delete [] x;
  std::chrono::time_point<std::chrono::high_resolution_clock>start, end;
  double elapsed1 = 0;
  start = now();
        
       check_success(cudaMalloc((void **)&device_x, sizeof(float) * xdims.batched_length()));
        check_success(cudaMalloc((void **)&device_w1, sizeof(float) * w1dims.batched_length()));
        check_success(cudaMalloc((void **)&device_b1, sizeof(float) * b1dims.batched_length()));
        check_success(cudaMalloc((void **)&device_out1, sizeof(float) * out1dims.batched_length())); 
       check_success(cudaMalloc((void **)&device_beta1, sizeof(float) * b1dims.batched_length()));
        check_success(cudaMalloc((void **)&device_gamma1, sizeof(float) * b1dims.batched_length()));
        check_success(cudaMalloc((void **)&device_mean1, sizeof(float) * b1dims.batched_length()));
        check_success(cudaMalloc((void **)&device_inv_std1, sizeof(float) * b1dims.batched_length()));
        

        check_success(cudaMemcpy(device_x, xT, sizeof(float) * xdims.batched_length(), cudaMemcpyHostToDevice));
        check_success(cudaMemcpy(device_w1, w1, sizeof(float) * w1dims.batched_length(), cudaMemcpyHostToDevice));
        check_success(cudaMemcpy(device_b1, b1, sizeof(float) * b1dims.batched_length(), cudaMemcpyHostToDevice));
        check_success(cudaMemcpy(device_beta1, beta1, sizeof(float) * b1dims.batched_length(), cudaMemcpyHostToDevice));
        check_success(cudaMemcpy(device_gamma1, gamma1, sizeof(float) * b1dims.batched_length(), cudaMemcpyHostToDevice));
        check_success(cudaMemcpy(device_mean1, mean1, sizeof(float) * b1dims.batched_length(), cudaMemcpyHostToDevice));
        check_success(cudaMemcpy(device_inv_std1, inv_std1, sizeof(float) * b1dims.batched_length(), cudaMemcpyHostToDevice));


        //launch kernel
        gemm_gpu(device_w1, device_x, device_out1,
		 w1dims.num, w1dims.length, 
		xdims.length, xdims.num, 
		w1dims.length, xdims.num,
		 device_b1, device_beta1, device_gamma1, device_mean1, device_inv_std1);
        check_success(cudaDeviceSynchronize());

  	check_success(cudaFree(device_x));
  	check_success(cudaFree(device_w1));
  	check_success(cudaFree(device_b1));
  	check_success(cudaFree(device_beta1));
  	check_success(cudaFree(device_gamma1));
  	check_success(cudaFree(device_mean1));
  	check_success(cudaFree(device_inv_std1));

  	delete [] w1;
  	delete [] b1;
  	delete [] beta1;
  	delete [] gamma1;
  	delete [] mean1;
  	delete [] inv_std1;

	// layer 2
  	unsigned int *wc2 = allocate<unsigned int>(wc2dims);
  	float *b2 = allocate<float>(b2dims);
  	float *beta2 = allocate<float>(b2dims); 
  	float *gamma2 = allocate<float>(b2dims);
  	float *mean2 = allocate<float>(b2dims);
  	float *inv_std2 = allocate<float>(b2dims);

  	readdata("params/wc2.bin", wc2dims, wc2);
  	readdata("params/b2.bin", b2dims, b2);
  	readdata("params/beta2.bin", b2dims, beta2);
  	readdata("params/gamma2.bin", b2dims, gamma2);
  	readdata("params/inv_std2.bin", b2dims, inv_std2);
  	readdata("params/mean2.bin", b2dims, mean2);

	unsigned int *device_wc2;
	float *device_b2;
	float *device_beta2;
	float *device_gamma2;
	float *device_mean2;
	float *device_inv_std2;

//FIXME: THIS could be in place
	unsigned int *device_out1_conc;
  check_success(cudaMalloc((void **)&device_wc2, sizeof(unsigned int) * wc2dims.batched_length()));
  check_success(cudaMalloc((void **)&device_b2, sizeof(float) * b2dims.batched_length()));
  check_success(cudaMalloc((void **)&device_beta2, sizeof(float) * b2dims.batched_length()));
  check_success(cudaMalloc((void **)&device_gamma2, sizeof(float) * b2dims.batched_length()));
  check_success(cudaMalloc((void **)&device_mean2, sizeof(float) * b2dims.batched_length()));
  check_success(cudaMalloc((void **)&device_inv_std2, sizeof(float) * b2dims.batched_length())); 
  check_success(cudaMalloc((void **)&device_out1_conc, sizeof(unsigned int) *out1dims.batched_length()/32));

  check_success(cudaMemcpy(device_wc2, wc2, sizeof(unsigned int) * wc2dims.batched_length(), cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_b2, b2, sizeof(float) * b2dims.batched_length(), cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_beta2, beta2, sizeof(float) * b2dims.batched_length(), cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_gamma2, gamma2, sizeof(float) * b2dims.batched_length(), cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_mean2, mean2, sizeof(float) * b2dims.batched_length(), cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_inv_std2, inv_std2, sizeof(float) * b2dims.batched_length(), cudaMemcpyHostToDevice));
  

  concatenate_cols_gpu(device_out1, device_out1_conc, out1dims.length, BATCH_SIZE);
 check_success(cudaDeviceSynchronize());

//reuse the space of device_out1 for device_out2
        gemm_gpu_xnor(device_wc2, device_out1_conc, device_out1,
		wc2dims.num, wc2dims.length,
		out1dims.length/32, out1dims.num,
		wc2dims.length, out1dims.num, 
           device_b2, device_beta2, device_gamma2, device_mean2, device_inv_std2, false);
	 check_success(cudaDeviceSynchronize());


	//layer 3, reuse b, beta gamma, inv_std and mean from layer2
  	readdata("params/wc3.bin", wc2dims, wc2);
	readdata("params/b3.bin", b2dims, b2);
	readdata("params/beta3.bin", b2dims, beta2);
	readdata("params/gamma3.bin", b2dims, gamma2);
	readdata("params/inv_std3.bin", b2dims, inv_std2);
	readdata("params/mean3.bin", b2dims, mean2);

  check_success(cudaMemcpy(device_wc2, wc2, sizeof(unsigned int) * wc2dims.batched_length(), cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_b2, b2, sizeof(float) * b2dims.batched_length(), cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_beta2, beta2, sizeof(float) * b2dims.batched_length(), cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_gamma2, gamma2, sizeof(float) * b2dims.batched_length(), cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_mean2, mean2, sizeof(float) * b2dims.batched_length(), cudaMemcpyHostToDevice));
  check_success(cudaMemcpy(device_inv_std2, inv_std2, sizeof(float) * b2dims.batched_length(), cudaMemcpyHostToDevice));

  concatenate_cols_gpu(device_out1, device_out1_conc, out1dims.length, BATCH_SIZE);
 check_success(cudaDeviceSynchronize());

//reuse the space of device_out1 for device_out2
        gemm_gpu_xnor(device_wc2, device_out1_conc, device_out1,
		wc2dims.num, wc2dims.length,
		out1dims.length/32, out1dims.num,
		wc2dims.length, out1dims.num, 
           device_b2, device_beta2, device_gamma2, device_mean2, device_inv_std2, false);
	 check_success(cudaDeviceSynchronize());
        
//layer 4, reuse memory space
        readdata("params/wc4.bin", wc4dims, wc2);
        readdata("params/b4.bin", b4dims, b2);
        readdata("params/beta4.bin", b4dims, beta2);
        readdata("params/gamma4.bin", b4dims, gamma2);
        readdata("params/inv_std4.bin", b4dims, inv_std2);
        readdata("params/mean4.bin", b4dims, mean2);
        
  	check_success(cudaMemcpy(device_wc2, wc2, sizeof(unsigned int) * wc4dims.batched_length(), cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_b2, b2, sizeof(float) * b4dims.batched_length(), cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_beta2, beta2, sizeof(float) * b4dims.batched_length(), cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_gamma2, gamma2, sizeof(float) * b4dims.batched_length(), cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_mean2, mean2, sizeof(float) * b4dims.batched_length(), cudaMemcpyHostToDevice));
  	check_success(cudaMemcpy(device_inv_std2, inv_std2, sizeof(float) * b4dims.batched_length(), cudaMemcpyHostToDevice));
        

  	concatenate_cols_gpu(device_out1, device_out1_conc, out1dims.length, BATCH_SIZE);
  	check_success(cudaDeviceSynchronize());

        gemm_gpu_xnor(device_wc2, device_out1_conc, device_out1,
		wc4dims.num, wc4dims.length,
		out1dims.length/32, out1dims.num,
		wc4dims.length, out4dims.num, 
           device_b2, device_beta2, device_gamma2, device_mean2, device_inv_std2, true);
	 check_success(cudaDeviceSynchronize());

  float* out4 = allocate<float>(out4dims);
  check_success(cudaMemcpy(out4, device_out1, sizeof(float) * out4dims.batched_length(), cudaMemcpyDeviceToHost));
	//print_acc(out4, out4dims.batched_length());

  check_success(cudaFree(device_out1_conc));
  check_success(cudaFree(device_out1));
  check_success(cudaFree(device_b2));
  check_success(cudaFree(device_beta2));
  check_success(cudaFree(device_gamma2));
  check_success(cudaFree(device_mean2));
  check_success(cudaFree(device_inv_std2));
  check_success(cudaFree(device_wc2));

  end = now();
  elapsed1 += std::chrono::duration<double, std::milli>(end - start).count(); 
  std::cout << "Total GPU runtime elapsed = " << elapsed1 << " milliseconds\n";

  float* out4T = allocate<float>(out4dims);

  transpose(out4, out4T, out4dims.length, out4dims.num);
  unsigned int max_idx = 0;

  for(int j = 0; j < out4dims.num; j++){
 	for (int i = 0; i < out4dims.length; i++)
 	 {
 	   float max_value = 0;
 	   if (max_value < out4T[j * out4dims.length + i])
 	   {
 	     max_idx = j * out4dims.length + i; 
 	     max_value = out4T[j*out4dims.length + i]; 
 	   }
 	 }
  	//print_acc(out1, out4dims.batched_length());
  	std::cout << "Category: " << max_idx%(out4dims.length )<< std::endl;
}

  delete [] b2;
  delete [] beta2;
  delete [] gamma2;
  delete [] mean2;
  delete [] inv_std2;
  delete [] wc2;
  delete [] out4;
  delete [] out4T;
}
